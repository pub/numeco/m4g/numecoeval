# NumEcoEval ChangeLog

Tous les changements de ce projet seront documentés dans ce document.

## [Non livré]

- Api-event-calcul : correction du backoff par défaut à 30 secondes : [Issue11](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/numeco/m4g/numecoeval/-/issues/11)
- Librairie de calculs : Ajout du champ DureeDeVie dans l'objet ImpactEquipementPhysique
- Correction : Trace du calcul de l'utilisation d'un équipement physique : suppression de '/ 365'
- Nettoyage : Suppression complète du calcul de l'impact du réseau à partir d'un équipement physique de type réseau (donc suppression des champs goTelecharge et nbJourUtiliseAn dans l'inventaire des équipements physiques). Désormais, l'impact du réseau se calcule grâce aux opérations non IT -> [Issue10](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/numeco/m4g/numecoeval/-/issues/10)
- Customisation des référentiels par organisation : les référentiels TypeItem, FacteurCaracterisation, CorrespondanceRefEquipement et Hypotheses peuvent être personnalisés par une organisation -> [Issue9](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/numeco/m4g/numecoeval/-/issues/9)

## [2.1.0] - 2024-08-02

- Colonnes goTelecharge et nbJourUtiliseAn plus obligatoires dans les fichiers d'inventaire des équipements physiques
- Correction bug dans la table ind_indicateur_impact_operation_non_it, ajout de l'attribut date_lot_discrimminator
- Nettoyage du code : Enlever tous les appels aux anciennes API (impactReseau, impactEquipement, mixelec et typeEquipement), les API restent fonctionnelles mais ne sont plus appelées par les autres composants
- Nettoyage : Supression des tables, Entities et Repositories plus utilisés (pour MixElectrique, ImpactEquipement, ImpactReseau & TypeEquipement) 
- Lancer les calculs sur uniquement quelques critères et/ou étapes -> [Issue6](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/numeco/m4g/numecoeval/-/issues/6)

## [2.0.1] - 2024-07-10

- Correction bug lors du lancement d'un calcul pour un équipement physique dont le modèle n'est pas renseigné -> [Issue8](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/numeco/m4g/numecoeval/-/issues/8)

## [2.0.0] - 2024-06-14

Adaptations pour être conforme avec le référentiel méthodologique d'évaluation environnementale des Systèmes d'Information [(RCP SI)](https://librairie.ademe.fr/consommer-autrement/6649-referentiel-methodologique-d-evaluation-environnementale-des-systemes-d-information-si.html):
- Pouvoir charger et lancer un calcul sur les opérations non IT -> [Issue3](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/numeco/m4g/numecoeval/-/issues/3)
- Intégrer la durée d'usage amont et aval des équipements physiques & ajout d'un paramètre pour choisir la méthode de calcul de la durée d'usage d'un équipement physique -> [Issue5](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/numeco/m4g/numecoeval/-/issues/5)
- Pouvoir inclure dans l'évaluation d'impact des informations sur la qualité des données -> [Issue4](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/numeco/m4g/numecoeval/-/issues/4)


## [1.3.0] - 2024-03-05

- Le paramètre "nomOrganisation" de l'API de chargement des inventaires devient un champ obligatoire -> [Issue2](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/numeco/m4g/numecoeval/-/issues/2)
- Intégrer des nouveaux impacts au référentiel -> [Issue1](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/numeco/m4g/numecoeval/-/issues/1)
- Defect: Mauvaise description de l'API de chargement des données d'entrées
- Defect : Erreur sur le calcul comparant la date de retrait et la date d'achat pour les équipements physiques
- Mise à jour de Java 17 vers 21 et des dépendances
- Defect: Libellé erreur erroné quand donnée de référence manquante
- Ajout du mode d'utilisation et taux d'utilisation
- Ajout d'une API /version pour récupérer la version courante de NumEcoEval dans le composant référentiel

## [1.2.2] - 2024-01-11

### Ajoutés
- Ajout de multi-partition pour le topic de donnée principal pour permettre la lecture en parallèle
- Amélioration des logs liés à Kafka et aux temps de traitement des équipements physiques

## [1.2.1] - 2024-01-04

### Corrigés
- Augmentation de la taille maximum des fichiers en entrée de POST /import/csv
- Remise en place des champs _discriminator
- Déduplication des avertissements en sortie d'API /import/csv

## [1.2.0] - 2023-12-11

### Ajoutés
- Contrôles sur les API d'entrées
- API Referentiel : affichage des mix electriques pour un pays

## [1.1.0] - 2023-11-21

### Ajoutés

### Corrigés
- [Code non cohérent avec la spécification](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/numeco/m4g/api-event-enrichissement/-/issues/2)
- [Aucun index lors de la récupération des sous-éléments - problème de performance ](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/numeco/m4g/api-event-donneesentrees/-/issues/1)

### Supprimés
- Composants api-event-enrichissement, api-event-indicateurs, api-rest-calculs (regroupés dans api-event-calculs)

## [1.0.0] - 2023-06-01

### Ajoutés
- Initialisation des modules
