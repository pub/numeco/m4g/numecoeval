<!-- Adapted from https://github.com/othneildrew/Best-README-Template/ -->
<a name="readme-top"></a>

<!-- PROJECT LOGO -->
<div align="center">

[![MIT License][license-shield]][license-url]
[![Java][Java]][Java-url]
[![Forge MTE][MTE]][MTE-url]

  <h3 align="center">NumEcoEval</h3>

  <p align="center">
    Calcul de l’empreinte environnementale d'un système d'information
    <br />
  </p>
</div>


<!-- TABLE OF CONTENTS -->
<details>
  <summary>Sommaire</summary>
  <ol>
    <li>
      <a href="#le-projet">Le projet</a>
    </li>
    <li>
      <a href="#demarrage-rapide">Démarrage rapide</a>
    </li>
    <li><a href="#feuille-de-route">Feuille de route</a></li>
    <li><a href="#contributions">Contributions</a></li>
    <li><a href="#licence">Licence</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->

## Le projet

**NumEcoEval** est une solution permettant de calculer l’empreinte environnementale d'un système d'information.
Le système d'information est défini comme l'ensemble des équipements physiques, des machines virtuelles et des applications gérés par une organisation.

Il suit les principes décrit dans la RCP Service Numérique et préfigure l'outillage qui pourrait être associé à une RCP "Système d'Information" qui est en cours d'élaboration par l'écosystème.

Cet outil a été construit sur la base des travaux réalisés par Le Ministère de la transition écologique, Sopra Steria, l’ADEME, l’INR et BOAVIZTA.

Ce projet est construit en modules présent dans ce même repository Gitlab ou dans d'autres du même groupe.

<details><summary>Liste des modules de NumEcoEval</summary>
<ul>
  <li>[docs](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/numeco/m4g/numecoeval/-/tree/develop/docs)</li>
  <li>[core](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/numeco/m4g/numecoeval/-/tree/develop/services/core)</li>
  <li>[common](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/numeco/m4g/numecoeval/-/tree/develop/services/common)</li>
  <li>[calculs](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/numeco/m4g/numecoeval/-/tree/develop/services/calculs)</li>
  <li>[api-expositiondonneesentrees](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/numeco/m4g/numecoeval/-/tree/develop/services/api-expositiondonneesentrees)</li>
  <li>[api-referentiel](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/numeco/m4g/numecoeval/-/tree/develop/services/api-referentiel)</li>
  <li>[api-event-donneesentrees](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/numeco/m4g/numecoeval/-/tree/develop/api-event-donneesentrees)</li>
  <li>[api-event-calculs](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/numeco/m4g/numecoeval/-/tree/develop/api-event-calculs)</li>
</ul>
</details>

<details><summary>Liste des modules hors de ce repository</summary>
<ul>
  <li>[ci-library](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/numeco/m4g/ci-library)</li>
</ul>
</details>


> Pour tout renseignement complémentaire, contacter numerique-ecologie@developpement-durable.gouv.fr 


<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- GETTING STARTED -->

## Démarrage rapide

### Installation

Le démarrage le plus rapide se fait via Docker-compose avec le [fichier mis à disposition](assets/docker-compose/docker-compose.yml).

La version par défaut est la version de développement `develop`.

Pour utiliser une version différente, il faut modifier le TAG présent dans le fichier [.env](assets/docker-compose/.env).

La commande la plus simple pour démarrer le moteur : 
````shell
# Aller dans le dossier assets/docker-compose
cd assets/docker-compose

# Faire cette commande si vous voulez les ports en 808x
# Par défaut, les ports sont en 1808x
sed -i "s/PORT_PREFIX=.*/PORT_PREFIX=/g" .env

# Mode Live - Uniquement durant l'exécution de la commande
docker-compose up

# Mode Deamon - Exécution en tâche de fond
docker-compose up -d
````

7 conteneurs doivent être démarrés :
* Zookeeper
* Kafka
* Postgres
* API REST Référentiels
* API REST Expositions des données d'entrées
* API Event données d'entrées
* API Event calculs

Pour arrêter l'application :
````shell
cd assets/docker-compose
docker-compose down
````
_Il suffit de couper le processus en mode Live._

Exemple de démarrage avec une version production:
````shell
cd assets/docker-compose
docker-compose -e TAG=1.2.0 up -d
````

*En cas d'initialisation, il peut arriver que le moteur ait besoin d'un redémarrage avant d'être utilisé.
Si une des API REST ne répond pas sur son URL ou un ou plusieurs containers sont indisponibles: il faut procéder à un redémarrage en arrêtant puis en démarrant le moteur.*


### Usage

Une fois l'application démarrée, les API REST sont alors disponibles sur les URLs suivantes (contrats d'interface):
- [API REST Référentiels](http://localhost:8080/swagger-ui/index.html)
- [API REST Expositions des données d'entrées](http://localhost:8081/swagger-ui/index.html)
- [API Event Calculs](http://localhost:8085/swagger-ui/index.html)

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- ROADMAP -->

## Feuille de route

:construction: _En cours de rédaction_ :construction:

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- CONTRIBUTING -->

## Contributions

Les contributions sont ce qui fait de la communauté open source un endroit incroyable pour
apprendre, s'inspirer et de créer. Toutes les contributions que vous faites sont **grandement
appréciées**.

À ce jour, veuillez nous contacter au préalable à numerique-ecologie@developpement-durable.gouv.fr pour avoir accès en contribution à ce projet.

Vous pouivez ensuite forker le dépôt et
créer une pull request. Vous pouvez aussi simplement ouvrir une issue avec le tag "
enhancement".
N'oubliez pas de donner une étoile au projet ! Merci encore !

1. Forkez le Project
2. Créez votre branche de feature (`git checkout -b feature/AmazingFeature`)
3. Commitez votre codes (`git commit -m 'Add some AmazingFeature'`)
4. Pushez sur la branche (`git push origin feature/AmazingFeature`)
5. Ouvrez Pull Request

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- LICENSE -->

## Licence

Distribué sous la licence Apache 2.0. Voir `LICENSE` pour plus d'informations.

<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- CONTACT -->

## Contact

Programme Numérique et Écologie - numerique-ecologie@developpement-durable.gouv.fr

<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->

[license-shield]: https://img.shields.io/gitlab/license/20519?gitlab_url=https%3A%2F%2Fgitlab-forge.din.developpement-durable.gouv.fr&style=for-the-badge

[license-url]: https://gitlab-forge.din.developpement-durable.gouv.fr/pub/numeco/m4g/numecoeval/-/blob/develop/LICENSE

[Java]: https://img.shields.io/badge/OpenJDK-000000?style=for-the-badge&logo=openjdk&logoColor=white&labelColor=C13D3C

[Java-url]: https://openjdk.org/

[MTE]: https://img.shields.io/badge/forge%20MTE-0000?color=00008f&style=for-the-badge&logo=gitlab

[MTE-url]: https://gitlab-forge.din.developpement-durable.gouv.fr/pub/numeco/m4g/numecoeval

