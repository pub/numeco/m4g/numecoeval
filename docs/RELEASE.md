# Création de release

Exemple avec version actuelle 1.4.0-SNAPSHOT pour faire une release 1.4.0, puis préparer la develop sur 1.5.0-SNAPSHOT

- Créer une MR develop -> main

- STOP PIPELINES (car non utiles ici)

- Sur branche main: 
	- CHANGELOG: nouvelle release
    - Maj version pom.xml <version>1.4.0</version> (un replace all permet d'aller plus vite)
    - Commit + push
    - STOP PIPELINES (car non utiles ici)

- Gitlab: Code > Tags
    - Créer le tag : ça va builder tout, laisser les pipelines tourner

- Sur la branche develop, maj du pom -> 1.5.0-SNAPSHOT
    - Commit + push
    - Laisser les pipelines tourner
