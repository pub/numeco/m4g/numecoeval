#!/bin/bash

# CONTANTS
REFERENTIELS="criteres etapes hypotheses correspondanceRefEquipement facteursCaracterisation typeItem"

E2E_LOCAL_PATH=.

. ./.env
. ./utils.sh

log "Send referentiel data"
for ref in $REFERENTIELS; do
    log_n
    curl -s -XPOST $REFERENTIEL_URL/referentiel/$ref/csv?mode=FULL --form file=@${E2E_LOCAL_PATH}/input_ref/$ref.csv
    echo ""
done
