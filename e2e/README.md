# E2E et génération de JDD

Dossiers :
- input_ref : référentiel
- input_template :
    - DataCenter.csv : fichier classique
    - EquipementPhysique_hors_serveur.csv : fichier avec 100 lignes d'équipements hors Server
    - EquipementPhysique_serveur.csv : fichier avec 100 lignes d'équipements Server
    - EquipementVirtuel.csv : 1 ligne variabilisé avec VCPU et TYPE_EQV
    - Application.csv:  1 ligne variabilisé avec TYPE_ENV
- expected: 
    - export des tables indicateurs en mode csv, triés
 
Le test E2E (non-reg) génère 100 équipements physiques hors serveur, 100 serveurs, 100 VM, 100 applications.

Lancement du test E2E:
- generic: `sh performance-test.sh E2E $NomOrg $NotLotPrefix`
- exemple: `sh performance-test.sh E2E org perf`

## Générer un JDD

Le script 2_generate_dataset.sh permet de générer un dataset à partir des fichiers présents dans `input_template`.

Il prend 4 paramètres:
- NB_EQ_PH_HORS_SRV
- NB_EQ_PH_SRV
- NB_VM_PAR_SRV
- NB_APP_PAR_VM

Exemple d'appel : `sh 2_generate_dataset.sh 100 100 1 1`
- Cela génère dans le dossier data :
  - 100 EqPh hors server
  - 100 EqPh server
  - 100 EqPh * 1 VM         = 100 VM
  - 100 EqPh * 1 VM * 1 APP = 100 APP


