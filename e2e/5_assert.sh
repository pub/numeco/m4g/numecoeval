#!/bin/bash

ORGANISATION=${1:-org1}
NOM_LOT=${2:-lot1}

E2E_LOCAL_PATH=.

# load INDICATEUR_URL
. ./.env

export_table() {
    curl -s "$INDICATEUR_URL/indicateur/${1}Csv?nomLot=${NOM_LOT}&nomOrganisation=${ORGANISATION}&fields=${2}" >actual/${1}.csv
}

if [ -d actual ]; then rm -rf actual; fi
mkdir actual

export_table equipementPhysique conso_elec_moyenne,critere,etapeacv,impact_unitaire,nom_entite,nom_entite_discriminator,nom_equipement,nom_source_donnee,nom_source_donnee_discriminator,quantite,source,statut_equipement_physique,statut_indicateur,trace,type_equipement,unite,version_calcul,qualite
export_table equipementVirtuel cluster,conso_elec_moyenne,critere,etapeacv,impact_unitaire,nom_entite,nom_entite_discriminator,nom_equipement,nom_equipement_virtuel,nom_source_donnee,nom_source_donnee_discriminator,source,statut_indicateur,trace,unite,version_calcul,qualite
export_table application conso_elec_moyenne,critere,domaine,etapeacv,impact_unitaire,nom_application,nom_entite,nom_entite_discriminator,nom_equipement_physique,nom_equipement_virtuel,nom_source_donnee,nom_source_donnee_discriminator,source,sous_domaine,statut_indicateur,trace,type_environnement,unite,version_calcul,qualite
export_table operationNonIT conso_elec_moyenne,critere,etapeacv,impact_unitaire,nom_entite,nom_entite_discriminator,nom_item_non_it,nom_source_donnee,nom_source_donnee_discriminator,quantite,source,statut_indicateur,trace,type_item,unite,version_calcul,qualite

ALL_OK=true
for file in $(ls actual/); do
    echo -n "Check file $file : "
    res=$(diff -qs actual/$file ${E2E_LOCAL_PATH}/expected/$file)
    if [ $? -eq 1 ]; then
        echo "KO"
        echo "*** REGRESSION : file $file is different from expected, see file: reports/diff_$file"
        diff actual/$file ${E2E_LOCAL_PATH}/expected/$file > reports/diff_$file
        ALL_OK=false
    else
        echo "OK"
    fi
done

if [ "${ALL_OK}" = "false" ]; then
    exit 1
fi
