#!/bin/bash

ORGANISATION=${1:-org1}
NOM_LOT=$2
SCENARIO=${3:-E2E}
MODE=${4:-ASYNC}
DUREE_USAGE=${5:-FIXE}

DATE_LOT=$(date +'%Y-%m-%d')

. ./.env
. ./utils.sh

rm -f progress.log

log_n "$ORGANISATION - $NOM_LOT - $DATE_LOT - Load Input - " | tee -a progress.log
curl -s -XPOST "$ENTREE_URL/entrees/csv?nomLot=${NOM_LOT}&dateLot=${DATE_LOT}&nomOrganisation=${ORGANISATION}" \
  --form csvDataCenter=@data/DataCenter.csv \
  --form csvEquipementPhysique=@data/EquipementPhysique.csv \
  --form csvEquipementVirtuel=@data/EquipementVirtuel.csv \
  --form csvApplication=@data/Application.csv \
  --form csvOperationNonIT=@data/OperationNonIT.csv | tee -a progress.log
 echo "" | tee -a progress.log

sleep 2

log_n "$ORGANISATION - $NOM_LOT - $DATE_LOT - Soumission with mode=${MODE} & duree usage=${DUREE_USAGE}" | tee -a progress.log

curl -s -XPOST "$ENTREE_URL/entrees/calculs/soumission?dureeUsage=$DUREE_USAGE&mode=$MODE" -d"{\"nomLot\":\"${NOM_LOT}\"}" -H "Content-Type: application/json" | tee -a progress.log
echo "" | tee -a progress.log
