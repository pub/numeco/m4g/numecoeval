function log() {
    echo "$(date +'%Y-%m-%d %H:%M:%S.%3N') - $@"
}

function log_n() {
    echo -n "$(date +'%Y-%m-%d %H:%M:%S.%3N') - $@"
}