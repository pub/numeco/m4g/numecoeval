#!/bin/bash

ORGANISATION=${1:-org1}
NOM_LOT=$2

. ./.env
. ./utils.sh

while true; do
    log_n "$ORGANISATION - $NOM_LOT - Statut - " | tee -a progress.log
    response=$(curl -s "$ENTREE_URL/entrees/calculs/statut?nomLot=${NOM_LOT}&nomOrganisation=${ORGANISATION}")
    if echo $response | grep -q "TERMINE"; then
        echo $response | tee -a progress.log
        echo "" | tee -a progress.log
        break
    fi

    echo $response | tee -a progress.log
    echo "" | tee -a progress.log
    sleep 10
done
