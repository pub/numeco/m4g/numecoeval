#!/bin/bash
# Call: sh 2_generate_dataset.sh
NB_EQ_PH_HORS_SRV=${1:-100}
NB_EQ_PH_SRV=${2:-100}
NB_VM_PAR_SRV=${3:-4}
NB_APP_PAR_VM=${4:-2}

. ./.env
. ./utils.sh

# CONSTANTS
NB_CSV_HORS_SRV=100
NB_CSV_SRV=100

set -f
ARR_VM_TYPE_EQV=(${VM_TYPE_EQV//,/ })
ARR_APP_TYPE_ENV=(${APP_TYPE_ENV//,/ })

if [ -d data ]; then
    rm -rf data
fi
mkdir data

# *** GENERATE DATASET ***
log "Generate input data in the 'data' local folder"
log "NB_EQ_PH_HORS_SRV=$NB_EQ_PH_HORS_SRV, NB_EQ_PH_SRV=$NB_EQ_PH_SRV, NB_VM_PAR_SRV=$NB_VM_PAR_SRV, NB_APP_PAR_VM=$NB_APP_PAR_VM"

# *** DataCenter.csv ***
log "Generate input data : DataCenter.csv"
cp -f input_template/DataCenter.csv data/

# *** EquipementPhysique_hors_serveur.csv ***
log "Generate input data : EquipementPhysique_hors_serveur.csv"
head -n1 input_template/EquipementPhysique_hors_serveur.csv >data/EquipementPhysique.csv

for ((i = 1; i <= $(($NB_EQ_PH_HORS_SRV / $NB_CSV_HORS_SRV)); i++)); do
    tail -n $NB_CSV_HORS_SRV input_template/EquipementPhysique_hors_serveur.csv | sed s/physical-eq-/physical-eq-${i}/g >>data/EquipementPhysique.csv
done

# *** EquipementPhysique_serveur.csv ***
log "Generate input data : EquipementPhysique_serveur.csv"

for ((i = 1; i <= $(($NB_EQ_PH_SRV / $NB_CSV_SRV)); i++)); do
    tail -n $NB_CSV_SRV input_template/EquipementPhysique_serveur.csv | sed s/physical-eq-srv-/physical-eq-srv-${i}/g >>data/EquipementPhysique.csv
done

# *** EquipementVirtuel.csv ***
log "Generate input data : EquipementVirtuel.csv"
head -n1 input_template/EquipementVirtuel.csv >data/EquipementVirtuel.csv

VM_LINE=$(tail -n1 input_template/EquipementVirtuel.csv)

n=0
for ((i = 1; i <= $(($NB_EQ_PH_SRV / $NB_CSV_SRV)); i++)); do
    for id in {001..100}; do
        for ((vm = 1; vm <= $NB_VM_PAR_SRV; vm++)); do
            type_eqv=${ARR_VM_TYPE_EQV[$((n % ${#ARR_VM_TYPE_EQV[@]}))]}
            VM1=${VM_LINE//virtual-eq-/virtual-eq-$i$id-$vm}
            VM2=${VM1//physical-eq-srv-/physical-eq-srv-$i$id}
            VM3=${VM2//##VCPU##/$VM_VCPU}
            echo ${VM3//##TYPE_EQV##/${type_eqv}}
            n=$((n + 1))
        done
    done >>data/EquipementVirtuel.csv
done

# *** OperationNonIT.csv ***
log "Generate input data : OperationNonIT.csv"
cp -f input_template/OperationNonIT.csv data/

# *** Application.csv ***
log "Generate input data : Application.csv"
head -n1 input_template/Application.csv >data/Application.csv

APP_LINE=$(tail -n1 input_template/Application.csv)

n=0
for ((i = 1; i <= $(($NB_EQ_PH_SRV / $NB_CSV_SRV)); i++)); do
    for id in {001..100}; do
        for ((vm = 1; vm <= $NB_VM_PAR_SRV; vm++)); do
            for ((app = 1; app <= $NB_APP_PAR_VM; app++)); do
                type_env=${ARR_APP_TYPE_ENV[$((n % ${#ARR_APP_TYPE_ENV[@]}))]}
                APP1=${APP_LINE//application-/application-$i$id-$vm-$app}
                APP2=${APP1//virtual-eq-/virtual-eq-$i$id-$vm}
                APP3=${APP2//physical-eq-srv-/physical-eq-srv-$i$id}
                echo ${APP3//##TYPE_ENV##/${type_env}}
                n=$((n + 1))
            done
        done
    done >>data/Application.csv
done

log "Generate input data : End"

echo "File,Size(MB),lines" >data/sizes.csv
total=0
lines=0
for file in $(ls data/ | grep -v sizes); do
    size=$(ls -l data/$file | cut -d' ' -f5)
    size=$(awk "BEGIN {printf \"%.3f\",$size/(1024 * 1024)}")
    line=$(wc -l data/$file | cut -d' ' -f1)
    echo "$file,$size,$line" >>data/sizes.csv
    total=$(awk "BEGIN {printf \"%.3f\",$total + $size}")
    lines=$((lines + $line))
done

echo "TOTAL,$total,$lines" >>data/sizes.csv

log "Report sizes generated in data/sizes.csv"
