package org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.adapter;

import org.instancio.Instancio;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.Entite;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.entity.EntiteEntity;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.repository.EntiteRepository;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.mapper.EntreeEntityMapper;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.mapper.EntreeEntityMapperImpl;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
class EntiteJpaAdapterTest {
    @InjectMocks
    private EntiteJpaAdapter jpaAdapter;

    @Mock
    EntiteRepository repository;

    EntreeEntityMapper entreeEntityMapper = new EntreeEntityMapperImpl();

    @BeforeEach
    void setup(){
        jpaAdapter = new EntiteJpaAdapter(repository, entreeEntityMapper);
    }

    @Test
    void saveShouldConvertAndCallSave() {
        Entite domain = Instancio.of(Entite.class).create();
        ArgumentCaptor<EntiteEntity> valueCapture = ArgumentCaptor.forClass(EntiteEntity.class);

        jpaAdapter.save(domain);

        Mockito.verify(repository, times(1)).save(valueCapture.capture());
        assertNotNull(valueCapture.getValue());
    }

    @Test
    void saveAllShouldConvertAndCallSaveAll() {
        Entite domain1 = Instancio.of(Entite.class).create();
        Entite domain2 = Instancio.of(Entite.class).create();
        ArgumentCaptor<List<EntiteEntity>> valueCapture = ArgumentCaptor.forClass(List.class);
        List<Entite> entrees = Arrays.asList(
                domain1,
                domain2
        );

        jpaAdapter.saveAll(entrees);

        Mockito.verify(repository, times(1)).saveAll(valueCapture.capture());
        assertNotNull(valueCapture.getValue());
        assertEquals(2, valueCapture.getValue().size());
    }
}
