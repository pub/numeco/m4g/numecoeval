package org.mte.numecoeval.expositiondonneesentrees.test;

import org.apache.commons.lang3.StringUtils;

import java.time.LocalDate;
import java.util.Map;

/**
 * Classe utilitaire pour les DataTable et la récupération de champ dans des types particuliers à partir de String.
 */
public class DataTableUtils {

    public static String safeString(Map<String, String> rowDatatable, String key) {
        if(StringUtils.isBlank(rowDatatable.get(key))) {
            return null;
        }
        return rowDatatable.get(key);
    }

    public static LocalDate safeLocalDate(Map<String, String> rowDatatable, String key) {
        if(StringUtils.isBlank(rowDatatable.get(key))) {
            return null;
        }
        return LocalDate.parse(rowDatatable.get(key));
    }

    public static Double safeDouble(Map<String, String> rowDatatable, String key) {
        if(StringUtils.isBlank(rowDatatable.get(key))) {
            return null;
        }
        return Double.parseDouble(rowDatatable.get(key));
    }

    public static Float safeFloat(Map<String, String> rowDatatable, String key) {
        if(StringUtils.isBlank(rowDatatable.get(key))) {
            return null;
        }
        return Float.parseFloat(rowDatatable.get(key));
    }

    public static Integer safeInteger(Map<String, String> rowDatatable, String key) {
        if(StringUtils.isBlank(rowDatatable.get(key))) {
            return null;
        }
        return Integer.parseInt(rowDatatable.get(key));
    }

    public static Boolean safeBoolean(Map<String, String> rowDatatable, String key) {
        if(StringUtils.isBlank(rowDatatable.get(key))) {
            return false;
        }
        return Boolean.parseBoolean(rowDatatable.get(key));
    }

    private DataTableUtils() {
        // Private constructor
    }
}
