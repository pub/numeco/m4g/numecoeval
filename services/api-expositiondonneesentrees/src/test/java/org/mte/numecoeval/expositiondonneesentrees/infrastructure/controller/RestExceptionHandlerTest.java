package org.mte.numecoeval.expositiondonneesentrees.infrastructure.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mte.numecoeval.expositiondonneesentrees.domain.exception.ValidationException;
import org.springframework.web.context.request.WebRequest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

class RestExceptionHandlerTest {

    RestExceptionHandler restExceptionHandler = new RestExceptionHandler();

    @Mock
    WebRequest webRequest;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void onException_shouldReturnGenericMessage() {
        Exception erreur = new Exception();
        when(webRequest.getContextPath()).thenReturn("/test");

        var response = restExceptionHandler.exception(erreur, webRequest);
        assertNotNull(response.getBody(), "Le corps de la réponse en erreur ne peut être null");
        assertEquals("500", response.getBody().getCode());
        assertEquals(500, response.getBody().getStatus());
        assertEquals("Erreur interne de traitement lors du traitement de la requête", response.getBody().getMessage());
    }


    @Test
    void onRuntimeException_shouldReturnGenericMessage() {
        var erreur = new NullPointerException("test");
        when(webRequest.getContextPath()).thenReturn("/test");

        var response = restExceptionHandler.runtimeException(erreur, webRequest);
        assertNotNull(response.getBody(), "Le corps de la réponse en erreur ne peut être null");
        assertEquals("500", response.getBody().getCode());
        assertEquals(500, response.getBody().getStatus());
        assertEquals("Erreur interne de traitement lors du traitement de la requête", response.getBody().getMessage());
    }


    @Test
    void onValidationException_shouldReturnGenericMessage() {
        var erreur = new ValidationException("Erreur lors de la validation des paramètres");
        when(webRequest.getContextPath()).thenReturn("/test");

        var response = restExceptionHandler.handleValidationException(erreur, webRequest);
        assertNotNull(response.getBody(), "Le corps de la réponse en erreur ne peut être null");
        assertEquals("400", response.getBody().getCode());
        assertEquals(400, response.getBody().getStatus());
        assertEquals(erreur.getErreur(), response.getBody().getMessage());
    }

}
