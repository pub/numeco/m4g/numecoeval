package org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.adapter;

import org.instancio.Instancio;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.OperationNonIT;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.entity.OperationNonITEntity;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.repository.OperationNonITRepository;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.mapper.EntreeEntityMapper;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.mapper.EntreeEntityMapperImpl;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
class OperationNonITJpaAdapterTest {
    @InjectMocks
    private OperationNonITJpaAdapter jpaAdapter;
    @Mock
    OperationNonITRepository repository;

    EntreeEntityMapper entreeEntityMapper = new EntreeEntityMapperImpl();

    @BeforeEach
    void setup() {
        jpaAdapter = new OperationNonITJpaAdapter(repository, entreeEntityMapper);
    }

    @Test
    void saveShouldConvertAndCallSave() {
        OperationNonIT domain = Instancio.of(OperationNonIT.class).create();
        ArgumentCaptor<OperationNonITEntity> valueCapture = ArgumentCaptor.forClass(OperationNonITEntity.class);

        jpaAdapter.save(domain);

        Mockito.verify(repository, times(1)).save(valueCapture.capture());
        assertNotNull(valueCapture.getValue());
    }

    @Test
    void saveAllShouldConvertAndCallSaveAll() {
        OperationNonIT domain1 = Instancio.of(OperationNonIT.class).create();
        OperationNonIT domain2 = Instancio.of(OperationNonIT.class).create();
        ArgumentCaptor<List<OperationNonITEntity>> valueCapture = ArgumentCaptor.forClass(List.class);
        List<OperationNonIT> entrees = Arrays.asList(
                domain1,
                domain2
        );

        jpaAdapter.saveAll(entrees);

        Mockito.verify(repository, times(1)).saveAll(valueCapture.capture());
        assertNotNull(valueCapture.getValue());
        assertEquals(2, valueCapture.getValue().size());
    }
}
