package org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.adapter;

import org.instancio.Instancio;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.DonneesEntree;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.entity.DonneesEntreesEntity;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.repository.DonneesEntreesRepository;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.mapper.EntreeEntityMapper;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.mapper.EntreeEntityMapperImpl;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
class DonneesEntreesJpaAdapterTest {
    @InjectMocks
    private DonneesEntreesJpaAdapter jpaAdapter;

    @Mock
    DonneesEntreesRepository repository;

    EntreeEntityMapper entreeEntityMapper = new EntreeEntityMapperImpl();

    @BeforeEach
    void setup(){
        jpaAdapter = new DonneesEntreesJpaAdapter(repository, entreeEntityMapper);
    }

    @Test
    void saveShouldConvertAndCallSave() {
        DonneesEntree domain = Instancio.of(DonneesEntree.class).create();
        ArgumentCaptor<DonneesEntreesEntity> valueCapture = ArgumentCaptor.forClass(DonneesEntreesEntity.class);

        jpaAdapter.save(domain);

        Mockito.verify(repository, times(1)).save(valueCapture.capture());
        assertNotNull(valueCapture.getValue());
    }

    @Test
    void saveAllShouldConvertAndCallSaveForEachObject() {
        DonneesEntree domain1 = Instancio.of(DonneesEntree.class).create();
        DonneesEntree domain2 = Instancio.of(DonneesEntree.class).create();
        List<DonneesEntree> entrees = Arrays.asList(
                domain1,
                domain2
        );

        jpaAdapter.saveAll(entrees);

        Mockito.verify(repository, times(entrees.size())).save(Mockito.any());
    }
}
