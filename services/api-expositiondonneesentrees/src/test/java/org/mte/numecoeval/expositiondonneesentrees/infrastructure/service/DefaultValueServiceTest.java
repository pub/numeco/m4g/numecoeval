package org.mte.numecoeval.expositiondonneesentrees.infrastructure.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.EquipementPhysique;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@ExtendWith(MockitoExtension.class)
public class DefaultValueServiceTest {
    @InjectMocks
    DefaultValueService defaultValueService;
    private String mode = "COPE";

    @BeforeEach
    public void init() {
        List<String> modeUtilisationList = List.of(mode);
        defaultValueService = new DefaultValueService(modeUtilisationList);
    }

    public EquipementPhysique equipementPhysique() {
        return EquipementPhysique.builder().build();
    }

    @Test
    void importEqPhysique_with_TauxUtilisationBiggerThan1_shouldBeReplacedByNull() {
        EquipementPhysique eq1 = equipementPhysique();
        eq1.setTauxUtilisation(21.0);
        defaultValueService.setEquipementValeurDefaut(eq1);
        assertNull(eq1.getTauxUtilisation());
    }

    @Test
    void importEqPhysique_with_TauxUtilisationOK_shouldReturnSameTaux() {
        EquipementPhysique eq1 = equipementPhysique();
        double tauxOK = 0.74;
        eq1.setTauxUtilisation(tauxOK);
        defaultValueService.setEquipementValeurDefaut(eq1);
        assertEquals(tauxOK, eq1.getTauxUtilisation(), 0.01);
    }

    @Test
    void importEqPhysique_with_ModeUtilisationKO_shouldBeReplacedByNull() {
        EquipementPhysique eq1 = equipementPhysique();
        eq1.setModeUtilisation("MODE_INCONNU");
        defaultValueService.setEquipementValeurDefaut(eq1);
        assertNull(eq1.getModeUtilisation());
    }

    @Test
    void importEqPhysique_with_ModeUtilisationOK_shouldReturnSameMode() {
        EquipementPhysique eq1 = equipementPhysique();
        eq1.setModeUtilisation(mode);
        defaultValueService.setEquipementValeurDefaut(eq1);
        assertEquals(mode, eq1.getModeUtilisation());
    }
}
