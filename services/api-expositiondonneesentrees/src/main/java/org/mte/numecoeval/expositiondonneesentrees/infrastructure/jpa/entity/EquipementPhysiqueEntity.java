package org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "EN_EQUIPEMENT_PHYSIQUE")
@Entity
public class EquipementPhysiqueEntity extends AbstractEntreeEntity {

    @Id
    @GeneratedValue(generator = "SEQ_EN_EQUIPEMENT_PHYSIQUE", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "SEQ_EN_EQUIPEMENT_PHYSIQUE", sequenceName = "SEQ_EN_EQUIPEMENT_PHYSIQUE", allocationSize = 1000)
    @Column(nullable = false)
    private Long id;

    private String nomEquipementPhysique;
    private String type;
    private String modele;
    private String statut;

    @Column(name = "pays_utilisation")
    private String paysDUtilisation;
    private String utilisateur;
    private LocalDate dateAchat;
    private LocalDate dateRetrait;
    private Double dureeUsageInterne;
    private Double dureeUsageAmont;
    private Double dureeUsageAval;
    private String nbCoeur;
    private String nomCourtDatacenter;
    private Double consoElecAnnuelle;
    private boolean serveur;
    private Double dureeVieDefaut;
    // référence d'équipement par défaut, propre au traitement
    private String refEquipementParDefaut;
    // référence d'équipement retenu via Correspondance dans le référentiel, propre au traitement
    String refEquipementRetenu;
    private String nomEntite;
    private Double quantite;
    private String modeUtilisation;
    private Double tauxUtilisation;
    private String qualite;

}
