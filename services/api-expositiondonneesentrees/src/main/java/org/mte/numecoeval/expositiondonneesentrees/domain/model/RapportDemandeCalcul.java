package org.mte.numecoeval.expositiondonneesentrees.domain.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@SuperBuilder
@Accessors(chain = true)
public class RapportDemandeCalcul {
    String nomLot;
    String dateLot;
    String nomOrganisation;
    Integer nbrDataCenter;
    Integer nbrEquipementPhysique;
    Integer nbrEquipementVirtuel;
    Integer nbrApplication;
    Integer nbrOperationNonIT;
    Integer nbrMessagerie;
}
