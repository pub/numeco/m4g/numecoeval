package org.mte.numecoeval.expositiondonneesentrees.infrastructure.service;

import lombok.AllArgsConstructor;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.config.MessageProperties;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.repository.ApplicationRepository;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.repository.EquipementPhysiqueRepository;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.repository.EquipementVirtuelRepository;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.repository.OperationNonITRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class ErrorManagementPostSaveService {

    final MessageProperties messageProperties;
    final EquipementPhysiqueRepository equipementPhysiqueRepository;
    final EquipementVirtuelRepository equipementVirtuelRepository;
    final ApplicationRepository applicationRepository;
    final OperationNonITRepository operationNonITRepository;


    public List<String> checkEquipementPhysiques(String nomLot) {
        // CA 3.2
        return equipementPhysiqueRepository.getEquipementPhysiqueSansDatacenter(nomLot).stream()
                .map(equipementPhysique -> messageProperties.getMessages().get("EQUIPEMENT_DATACENTER_INCONNU").formatted(equipementPhysique))
                .toList();
    }

    public List<String> checkEquipementVirtuels(String nomLot) {
        // CA 3.3
        return equipementVirtuelRepository.getEquipementVirtuelSansEquipementPhysique(nomLot).stream()
                .map(equipementVirtuel -> messageProperties.getMessages().get("EQUIPEMENT_VIRTUEL_INCONNU").formatted(equipementVirtuel))
                .toList();
    }

    public List<String> checkApplications(String nomLot) {
        var result = new ArrayList<String>();

        // CA 3.4.1
        result.addAll(applicationRepository.getApplicationSansEquipementPhysique(nomLot).stream()
                .map(equipementVirtuel -> messageProperties.getMessages().get("APPLICATION_AVEC_EQUIPEMENT_PHYSIQUE_INCONNU").formatted(equipementVirtuel))
                .toList());

        // CA 3.4.2
        result.addAll(applicationRepository.getApplicationSansEquipementPhysique(nomLot).stream()
                .map(equipementVirtuel -> messageProperties.getMessages().get("APPLICATION_AVEC_EQUIPEMENT_VIRTUEL_INCONNU").formatted(equipementVirtuel))
                .toList());

        return result;
    }

    public List<String> checkOperationsNonIT(String nomLot) {
        return operationNonITRepository.getOperationNonITAvecMauvaisNomCourtDatacenter(nomLot).stream()
                .map(itemNonIT -> messageProperties.getMessages().get("ITEM_NOM_DATACENTER_INCONNU").formatted(itemNonIT))
                .toList();
    }
}
