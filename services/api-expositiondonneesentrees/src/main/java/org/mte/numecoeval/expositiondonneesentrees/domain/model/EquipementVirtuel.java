package org.mte.numecoeval.expositiondonneesentrees.domain.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@SuperBuilder
@Accessors(chain = true)
public class EquipementVirtuel extends AbstractEntree {
    String nomEquipementVirtuel;
    String nomEquipementPhysique;
    String nomSourceDonneeEquipementPhysique;
    Integer vCPU;
    String cluster;
    String nomEntite;
    Double consoElecAnnuelle;
    String typeEqv;
    Double capaciteStockage;
    Double cleRepartition;
    String qualite;
}
