package org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.repository;

import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.entity.MessagerieEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessagerieRepository extends JpaRepository<MessagerieEntity, Long> {
    @Query("select id from #{#entityName} where nomLot = ?1 and statutTraitement = ?2")
    List<Long> getIdsByNomLotAndStatutTraitement(String nomLot, String statutTraitement);
}
