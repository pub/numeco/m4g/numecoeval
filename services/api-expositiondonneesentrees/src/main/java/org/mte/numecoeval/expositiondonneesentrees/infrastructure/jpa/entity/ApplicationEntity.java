package org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.entity;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.SuperBuilder;

/**
 * Entité représentant une application dans les données d'entrées.
 */
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Table(name = "EN_APPLICATION")
@Entity
public class ApplicationEntity extends AbstractEntreeEntity {
    @Id
    @GeneratedValue(generator = "SEQ_EN_APPLICATION", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "SEQ_EN_APPLICATION", sequenceName = "SEQ_EN_APPLICATION", allocationSize = 1000)
    @Column(nullable = false)
    private Long id;

    /**
     * Nom de l'application
     */
    private String nomApplication;

    /**
     * Type d'environnement de l'instance de l'application
     */
    private String typeEnvironnement;

    /**
     * Référence de l'équipement virtuel rattaché
     */
    private String nomEquipementVirtuel;

    /**
     * Référence de l'équipement physique rattaché
     */
    private String nomEquipementPhysique;

    /**
     * Nom de la source de données pour l'équipement physique rattaché
     */
    private String nomSourceDonneeEquipementVirtuel;

    /**
     * Domaine ou catégorie principale de l'application
     */
    private String domaine;

    /**
     * Domaine ou catégorie secondaire de l'application
     */
    private String sousDomaine;

    /**
     * Nom de l'entité rattachée à l'application
     */
    private String nomEntite;
    
    /**
     * Qualité de la donnée collectée
     */
    private String qualite;

}
