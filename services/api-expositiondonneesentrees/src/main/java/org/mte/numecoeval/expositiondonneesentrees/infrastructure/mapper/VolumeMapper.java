package org.mte.numecoeval.expositiondonneesentrees.infrastructure.mapper;

import org.mapstruct.Mapper;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.Volume;
import org.mte.numecoeval.expositiondonneesentrees.generated.api.model.VolumeRest;

@Mapper(componentModel = "spring")
public interface VolumeMapper {

    VolumeRest toRest(Volume volume);
}
