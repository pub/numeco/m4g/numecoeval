package org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.adapter;

import lombok.AllArgsConstructor;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.Application;
import org.mte.numecoeval.expositiondonneesentrees.domain.ports.output.EntreePersistencePort;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.repository.ApplicationRepository;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.mapper.EntreeEntityMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class ApplicationJpaAdapter implements EntreePersistencePort<Application> {
    private ApplicationRepository repository;
    EntreeEntityMapper entreeEntityMapper;

    @Override
    public void save(Application entree) {
        repository.save(entreeEntityMapper.toEntity(entree));
    }

    @Override
    public void saveAll(List<Application> entrees) {
        repository.saveAll(entreeEntityMapper.toEntityListApplication(entrees));
    }
}