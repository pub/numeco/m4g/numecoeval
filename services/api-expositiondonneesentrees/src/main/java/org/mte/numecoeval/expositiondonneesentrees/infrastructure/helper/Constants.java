package org.mte.numecoeval.expositiondonneesentrees.infrastructure.helper;

public class Constants {

    public static final String TABLE_DONNEES_ENTREES = "en_donnees_entrees";
    public static final String TABLE_DATA_CENTER = "en_data_center";
    public static final String TABLE_EQUIPEMENT_PHYSIQUE = "en_equipement_physique";
    public static final String TABLE_EQUIPEMENT_VIRTUEL = "en_equipement_virtuel";
    public static final String TABLE_APPLICATION = "en_application";
    public static final String TABLE_OPERATION_NON_IT = "en_operation_non_it";
    public static final String TABLE_MESSAGERIE = "en_messagerie";
}
