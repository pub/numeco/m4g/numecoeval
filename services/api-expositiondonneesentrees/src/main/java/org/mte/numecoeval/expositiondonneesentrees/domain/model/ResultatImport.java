package org.mte.numecoeval.expositiondonneesentrees.domain.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
@SuperBuilder
@NoArgsConstructor
public class ResultatImport {

    List<RapportImport> rapports = new ArrayList<>();

    DonneesEntree donneesEntree = new DonneesEntree();
}
