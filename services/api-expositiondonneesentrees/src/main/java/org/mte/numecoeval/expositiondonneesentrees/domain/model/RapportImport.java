package org.mte.numecoeval.expositiondonneesentrees.domain.model;

import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
@SuperBuilder
@NoArgsConstructor
public class RapportImport {

    String fichier;

    String type;

    List<String> erreurs = new ArrayList<>();

    List<String> avertissements = new ArrayList<>();

    long nbrLignesImportees = 0;
}
