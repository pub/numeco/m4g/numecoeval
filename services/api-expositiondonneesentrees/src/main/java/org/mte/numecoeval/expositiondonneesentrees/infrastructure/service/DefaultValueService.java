package org.mte.numecoeval.expositiondonneesentrees.infrastructure.service;

import lombok.AllArgsConstructor;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.EquipementPhysique;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class DefaultValueService {

    @Value("#{'${constraints.mode-utilisation}'.split(',')}")
    private List<String> modeUtilisationList;

    /**
     * fonction à effet de bord qui permet de réaffecter certaines variables à leur valeur par défaut lorsque leur valeur initiale est incorrecte (créé un avertissement)
     *
     * @param equipementPhysique un equipementPhysique à enregistrer en base
     */
    public void setEquipementValeurDefaut(EquipementPhysique equipementPhysique) {
        if (equipementPhysique.getModeUtilisation() != null && !modeUtilisationList.contains(equipementPhysique.getModeUtilisation())) {
            equipementPhysique.setModeUtilisation(null);
        }
        Double taux = equipementPhysique.getTauxUtilisation();
        if (taux != null && (taux < 0 || taux > 1)) {
            equipementPhysique.setTauxUtilisation(null);
        }
    }
}
