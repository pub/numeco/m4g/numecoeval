package org.mte.numecoeval.expositiondonneesentrees.infrastructure.config;

import org.apache.commons.lang3.ArrayUtils;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenApiConfig {

    private static final String[] IMPORT_PATHS = {
            "/entrees/json",
            "/entrees/csv"
    };
    private static final String[] CALCUL_PATHS = {
            "/entrees/calculs/*"
    };

    @Bean
    public GroupedOpenApi importGroupApi() {
        return GroupedOpenApi.builder()
                .group("Imports")
                .displayName("NumEcoEval Imports")
                .pathsToMatch(IMPORT_PATHS)
                .build();
    }

    @Bean
    public GroupedOpenApi calculsGroupApi() {
        return GroupedOpenApi.builder()
                .group("Calculs")
                .displayName("NumEcoEval Calculs")
                .pathsToMatch(CALCUL_PATHS)
                .build();
    }

    @Bean
    public GroupedOpenApi allGroupApi() {
        return GroupedOpenApi.builder()
                .group("All")
                .displayName("NumEcoEval Toutes les APIs")
                .pathsToMatch(ArrayUtils.addAll(IMPORT_PATHS, CALCUL_PATHS))
                .build();
    }

}

