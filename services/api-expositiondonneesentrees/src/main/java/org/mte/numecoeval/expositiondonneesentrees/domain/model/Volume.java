package org.mte.numecoeval.expositiondonneesentrees.domain.model;

public record Volume(long nbEnCours, long nbTraite) {
}
