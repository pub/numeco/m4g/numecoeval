package org.mte.numecoeval.expositiondonneesentrees.infrastructure.adapters;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.expositiondonneesentrees.domain.ports.output.ReferentielServicePort;
import org.mte.numecoeval.expositiondonneesentrees.referentiels.generated.api.client.InterneNumEcoEvalApi;
import org.mte.numecoeval.expositiondonneesentrees.referentiels.generated.api.model.*;
import org.mte.numecoeval.expositiondonneesentrees.utils.Constants;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatusCode;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import java.util.List;

@Service
@Slf4j
@AllArgsConstructor
public class ReferentielRestClient implements ReferentielServicePort {

    private InterneNumEcoEvalApi interneNumEcoEvalApi;

    @Cacheable("hasMixElec")
    @Override
    public boolean hasMixElec(String pays, String nomOrganisation) {
        for (String nomOrg : List.of(nomOrganisation, "")) {
            try {
                var mixelec = interneNumEcoEvalApi.getFacteurCaracterisation(null, null, null, pays, Constants.ELECTRICITY_MIX_CATEGORY, nomOrg).blockFirst();
                if (mixelec != null) {
                    return true;
                }
            } catch (WebClientResponseException e) {
                if (e.getStatusCode() != HttpStatusCode.valueOf(404)) {
                    log.error("Une erreur est survenue lors de l'appel au référentiel facteurcaracterisation", e);
                }
            }
        }
        return false;
    }

    @Override
    public List<TypeItemDTO> getAllTypesItem() {
        var result = interneNumEcoEvalApi.getAllTypeItemWithHttpInfo().block();
        return result.getBody();
    }

    @Cacheable("CorrespondanceRefEquipement")
    @Override
    public CorrespondanceRefEquipementDTO getCorrespondance(String modele, String nomOrganisation) {
        for (String nomOrg : List.of(nomOrganisation, "")) {
            try {
                var corr = interneNumEcoEvalApi.getCorrespondanceRefEquipement(modele, nomOrg).block();
                if (corr != null) {
                    return corr;
                }
            } catch (WebClientResponseException e) {
                if (e.getStatusCode() != HttpStatusCode.valueOf(404)) {
                    log.error("Une erreur est survenue lors de l'appel au référentiel correspondanceRefEquipement", e);
                }
            }
        }
        return null;
    }

    @Cacheable("FacteurCaracterisation")
    @Override
    public FacteurCaracterisationDTO getFacteurCaracterisation(String critere, String etape, String nom, String nomOrganisation) {
        for (String nomOrg : List.of(nomOrganisation, "")) {
            try {
                var fc = interneNumEcoEvalApi.getFacteurCaracterisation(critere, etape, nom, null, null, nomOrg).blockFirst();
                if (fc != null) {
                    return fc;
                }
            } catch (WebClientResponseException e) {
                if (e.getStatusCode() != HttpStatusCode.valueOf(404)) {
                    log.error("Une erreur est survenue lors de l'appel au référentiel facteurcaracterisation", e);
                }
            }
        }
        return null;
    }

    @Cacheable("Etapes")
    @Override
    public List<EtapeDTO> getAllEtapes() {
        return interneNumEcoEvalApi.getAllEtapesWithHttpInfo().block().getBody();
    }

    @Cacheable("Criteres")
    @Override
    public List<CritereDTO> getAllCriteres() {
        return interneNumEcoEvalApi.getAllCriteresWithHttpInfo().block().getBody();
    }
}
