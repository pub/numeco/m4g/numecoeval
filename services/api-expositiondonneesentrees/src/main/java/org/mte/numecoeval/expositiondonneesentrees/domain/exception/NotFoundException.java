package org.mte.numecoeval.expositiondonneesentrees.domain.exception;

public class NotFoundException extends RuntimeException {

    /**
     * Constructor with no arg
     */
    public NotFoundException() {
        super();
    }

}

