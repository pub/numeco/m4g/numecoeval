package org.mte.numecoeval.expositiondonneesentrees.domain.ports.input.impl;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.DemandeCalcul;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.RapportDemandeCalcul;
import org.mte.numecoeval.expositiondonneesentrees.domain.ports.input.SoumissionCalculSyncPort;
import org.mte.numecoeval.expositiondonneesentrees.generated.api.model.StatutTraitement;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.adapters.CalculsRestClient;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.repository.DataCenterRepository;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.repository.EquipementPhysiqueRepository;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.repository.MessagerieRepository;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.repository.OperationNonITRepository;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.mapper.CalculRestMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@AllArgsConstructor
public class SoumissionCalculSyncPortImpl implements SoumissionCalculSyncPort {

    EquipementPhysiqueRepository equipementPhysiqueRepository;
    OperationNonITRepository operationNonITRepository;
    MessagerieRepository messagerieRepository;

    DataCenterRepository dataCenterRepository;
    CalculsRestClient calculsRestClient;

    CalculRestMapper calculRestMapper;

    @Override
    public RapportDemandeCalcul soumissionCalcul(DemandeCalcul demandeCalcul) {
        validate(demandeCalcul);

        // find equipements physiques à partir de nomLot et nomOrganisation
        List<Long> equipementPhysiqueIds = equipementPhysiqueRepository.getIdsByNomLotAndStatutTraitement(demandeCalcul.getNomLot(), StatutTraitement.EN_ATTENTE.getValue());
        List<Long> operatioNonITIds = operationNonITRepository.getIdsByNomLotAndStatutTraitement(demandeCalcul.getNomLot(), StatutTraitement.EN_ATTENTE.getValue());
        List<Long> messagerieEntityIds = messagerieRepository.getIdsByNomLotAndStatutTraitement(demandeCalcul.getNomLot(), StatutTraitement.EN_ATTENTE.getValue());

        // map to Rest
        var reponseCalculRest = calculsRestClient.postSyncCalcul(
                equipementPhysiqueIds,
                operatioNonITIds,
                messagerieEntityIds
        );

        var rapport = calculRestMapper.toRest(reponseCalculRest);
        rapport.setNomLot(demandeCalcul.getNomLot());
        rapport.setDateLot(demandeCalcul.getDateLot());
        rapport.setNomOrganisation(demandeCalcul.getNomOrganisation());
        return rapport;
    }

}
