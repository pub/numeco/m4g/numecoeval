package org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.repository;

import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.entity.OperationNonITEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OperationNonITRepository extends JpaRepository<OperationNonITEntity, Long> {
    @Query(value = """
            WITH datacenters AS (
                SELECT nom_court_datacenter FROM en_data_center WHERE nom_lot = ?1
            )
            SELECT nom_item_non_it FROM en_operation_non_it eoni
            WHERE nom_lot = ?1
            AND nom_court_datacenter is NOT NULL
            AND nom_court_datacenter NOT IN (SELECT nom_court_datacenter FROM datacenters)
            ORDER BY nom_item_non_it
            """, nativeQuery = true)
    List<String> getOperationNonITAvecMauvaisNomCourtDatacenter(String nomLot);

    @Query("select id from #{#entityName} where nomLot = ?1 and statutTraitement = ?2")
    List<Long> getIdsByNomLotAndStatutTraitement(String nomLot, String statutTraitement);

}
