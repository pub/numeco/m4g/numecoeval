package org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.repository;

import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.entity.ApplicationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ApplicationRepository extends JpaRepository<ApplicationEntity, Long> {
    @Query(value = """
            SELECT nom_application  FROM en_application ea
            LEFT join en_equipement_physique eep ON ea.nom_equipement_physique  = eep.nom_equipement_physique
            WHERE ea.nom_lot = ?1
            AND (ea.nom_equipement_physique is null
            OR eep.nom_equipement_physique is null)
            ORDER BY nom_application
            """, nativeQuery = true)
    List<String> getApplicationSansEquipementPhysique(String nomLot);

    @Query(value = """
            SELECT nom_application  FROM en_application ea
            LEFT join en_equipement_virtuel eev ON ea.nom_equipement_virtuel = eev.nom_equipement_virtuel
            WHERE ea.nom_lot = ?1
            AND (ea.nom_equipement_virtuel is null
            OR eev.nom_equipement_virtuel is null)
            ORDER BY nom_application
            """, nativeQuery = true)
    List<String> getApplicationSansEquipementVirtuel(String nomLot);
}
