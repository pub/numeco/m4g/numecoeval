package org.mte.numecoeval.expositiondonneesentrees.infrastructure.config;

import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.expositiondonneesentrees.referentiels.generated.api.client.InterneNumEcoEvalApi;
import org.mte.numecoeval.expositiondonneesentrees.sync.calculs.generated.api.client.CalculsEquipementPhysiqueEtOperationNonItEtMessagerieApi;
import org.mte.numecoeval.expositiondonneesentrees.sync.calculs.generated.api.invoker.ApiClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
@Slf4j
public class CommonIntegrationConfig {

    @Value("${numecoeval.calculs.server.url}")
    String calculUrl;

    @Value("${numecoeval.referentiel.server.url}")
    String referentielUrl;

    @Bean
    public CalculsEquipementPhysiqueEtOperationNonItEtMessagerieApi clientAPISyncCalculs() {
        CalculsEquipementPhysiqueEtOperationNonItEtMessagerieApi calculsApi = new CalculsEquipementPhysiqueEtOperationNonItEtMessagerieApi();
        var apiClient = new ApiClient(WebClient.builder()
                .baseUrl(calculUrl)
                .build());
        apiClient.setBasePath(calculUrl);
        calculsApi.setApiClient(apiClient);

        log.info("Création du client d'API Sync Calcul sur l'URL {}", calculUrl);

        return calculsApi;
    }

    @Bean
    public InterneNumEcoEvalApi clientAPIReferentiel() {
        var interneNumEcoEvalApi = new InterneNumEcoEvalApi();
        var apiClient = new org.mte.numecoeval.expositiondonneesentrees.referentiels.generated.api.invoker.ApiClient(WebClient.builder()
                .baseUrl(referentielUrl)
                .build());
        apiClient.setBasePath(referentielUrl);
        interneNumEcoEvalApi.setApiClient(apiClient);

        log.info("Création du client d'API Referentiel sur l'URL {}", referentielUrl);

        return interneNumEcoEvalApi;
    }

}
