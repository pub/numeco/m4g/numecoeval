package org.mte.numecoeval.expositiondonneesentrees.domain.ports.input;

import org.mte.numecoeval.expositiondonneesentrees.domain.exception.ValidationException;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.DemandeCalcul;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.RapportDemandeCalcul;

import java.util.Objects;

public interface SoumissionCalculSyncPort {

    /**
     * Envoie la demande de calcul pour traitement.
     * Met à jour le statut de tous les objets d'entrées correspondant et en attente (statut = "EN_ATTENTE") au statut "A_INGERER".
     *
     * @param demandeCalcul {@link DemandeCalcul} à traiter
     * @return {@link RapportDemandeCalcul} traçant les modifications effectuées
     */
    RapportDemandeCalcul soumissionCalcul(DemandeCalcul demandeCalcul);

    /**
     * Validation de la demande de calcul
     *
     * @param demandeCalcul {@link DemandeCalcul} à traiter
     * @throws ValidationException en cas d'erreur de validation de l'objet {@link DemandeCalcul}
     */
    default void validate(DemandeCalcul demandeCalcul) throws ValidationException {
        if (Objects.isNull(demandeCalcul)) {
            throw new ValidationException("Corps de la demande obligatoire");
        }

        if (Objects.isNull(demandeCalcul.getNomLot())) {
            throw new ValidationException("Nom de lot obligatoire");
        }
    }
}
