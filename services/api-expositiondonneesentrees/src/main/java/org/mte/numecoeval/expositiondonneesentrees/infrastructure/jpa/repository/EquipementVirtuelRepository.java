package org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.repository;

import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.entity.EquipementVirtuelEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EquipementVirtuelRepository extends JpaRepository<EquipementVirtuelEntity, Long> {

    @Query(value = """
            SELECT nom_equipement_virtuel FROM en_equipement_virtuel eev
            LEFT join en_equipement_physique eep ON eev.nom_equipement_physique  = eep.nom_equipement_physique
            WHERE eev.nom_lot = ?1
            AND (eev.nom_equipement_physique is null
            OR eep.nom_equipement_physique is null)
            ORDER BY nom_equipement_virtuel
            """, nativeQuery = true)
    List<String> getEquipementVirtuelSansEquipementPhysique(String nomLot);
}
