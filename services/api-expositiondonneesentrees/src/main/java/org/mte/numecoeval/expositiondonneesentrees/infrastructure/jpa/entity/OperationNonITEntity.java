package org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "EN_OPERATION_NON_IT")
@Entity
public class OperationNonITEntity extends AbstractEntreeEntity {

    @Id
    @GeneratedValue(generator = "SEQ_EN_OPERATION_NON_IT", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "SEQ_EN_OPERATION_NON_IT", sequenceName = "SEQ_EN_OPERATION_NON_IT", allocationSize = 1000)
    @Column(nullable = false)
    private Long id;
    @Column(name = "nom_item_non_it")
    private String nomItemNonIT;
    private Double quantite;
    private String type;
    private Double dureeDeVie;
    private String localisation;
    private String nomEntite;
    private String nomSourceDonnee;
    private String nomCourtDatacenter;
    private String description;
    private Double consoElecAnnuelle;
    private String qualite;

}
