package org.mte.numecoeval.expositiondonneesentrees.infrastructure.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.*;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.entity.*;

import java.util.List;

@Mapper(componentModel = "spring")
public interface EntreeEntityMapper {

    DonneesEntreesEntity toEntity(DonneesEntree domain);

    DataCenterEntity toEntity(DataCenter domain);

    EquipementPhysiqueEntity toEntity(EquipementPhysique domain);

    @Mapping(target = "vCPU", source = "VCPU")
    EquipementVirtuelEntity toEntity(EquipementVirtuel domain);

    ApplicationEntity toEntity(Application domain);

    MessagerieEntity toEntity(Messagerie domain);

    EntiteEntity toEntity(Entite domain);

    OperationNonITEntity toEntity(OperationNonIT domain);

    List<DataCenterEntity> toEntityListDataCenter(List<DataCenter> domains);

    List<EquipementPhysiqueEntity> toEntityListEquipementPhysique(List<EquipementPhysique> domains);

    List<EquipementVirtuelEntity> toEntityListEquipementVirtuel(List<EquipementVirtuel> domains);

    List<ApplicationEntity> toEntityListApplication(List<Application> domains);

    List<MessagerieEntity> toEntityListMessagerie(List<Messagerie> domains);

    List<EntiteEntity> toEntityListEntite(List<Entite> domains);

    List<OperationNonITEntity> toEntityListOperationNonIT(List<OperationNonIT> domains);

    DonneesEntree toDomain(DonneesEntreesEntity entity);

    DataCenter toDomain(DataCenterEntity entity);

    EquipementPhysique toDomain(EquipementPhysiqueEntity entity);

    EquipementVirtuel toDomain(EquipementVirtuelEntity entity);

    Application toDomain(ApplicationEntity entity);

    Messagerie toDomain(MessagerieEntity entity);

    Entite toDomain(Entite entity);

    OperationNonIT toDomain(OperationNonITEntity entity);

    List<DataCenter> toDomainListDataCenter(List<DataCenterEntity> entities);

    List<EquipementPhysique> toDomainListEquipementPhysique(List<EquipementPhysiqueEntity> entities);

    List<EquipementVirtuel> toDomainListEquipementVirtuel(List<EquipementVirtuelEntity> entities);

    List<Application> toDomainListApplication(List<ApplicationEntity> entities);

    List<Messagerie> toDomainListMessagerie(List<MessagerieEntity> entities);

    List<Entite> toDomainListEntite(List<EntiteEntity> entities);

    List<OperationNonIT> toDomainListOperationNonIT(List<OperationNonITEntity> entities);
}
