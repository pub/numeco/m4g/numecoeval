package org.mte.numecoeval.expositiondonneesentrees.domain.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReferentielRuntimeException extends RuntimeException{

    private final String errorType;

    public ReferentielRuntimeException(String errorType, String message ) {
        super(message);
        this.errorType = errorType;

        Logger logger = LoggerFactory.getLogger(ReferentielRuntimeException.class);
        logger.error("{}: [ {} ]",errorType,message);
    }

    public String getErrorType() {
        return errorType;
    }
}
