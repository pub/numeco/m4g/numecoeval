package org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "EN_EQUIPEMENT_VIRTUEL")
@Entity
public class EquipementVirtuelEntity extends AbstractEntreeEntity {
    @Id
    @GeneratedValue(generator = "SEQ_EN_EQUIPEMENT_VIRTUEL", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "SEQ_EN_EQUIPEMENT_VIRTUEL", sequenceName = "SEQ_EN_EQUIPEMENT_VIRTUEL", allocationSize = 1000)
    @Column(nullable = false)
    private Long id;
    private String nomEquipementVirtuel;
    private String nomEquipementPhysique;
    private String nomSourceDonneeEquipementPhysique;
    private Integer vCPU;
    private String cluster;
    private String nomEntite;
    private Double consoElecAnnuelle;
    private String typeEqv;
    private Double capaciteStockage;
    private Double cleRepartition;
    private String qualite;

}
