package org.mte.numecoeval.expositiondonneesentrees.domain.ports.output;

import org.mte.numecoeval.expositiondonneesentrees.domain.model.AbstractEntree;

import java.util.List;
import java.util.Set;

public interface EntreePersistencePort<T extends AbstractEntree> {
    void save(T entree);

    void saveAll(List<T> entrees);

    default int updateStatutInList(String statut, String nomLot, String nomOrganisation, Set<String> noms) {
        return 0;
    }
}
