package org.mte.numecoeval.expositiondonneesentrees.utils;

import java.util.List;

public class Constants {
    public static final List<String> QUALITES_DONNEES = List.of("BASSE", "MOYENNE", "HAUTE");
    public static final String ELECTRICITY_MIX_CATEGORY = "electricity-mix";

}
