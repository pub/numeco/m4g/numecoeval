package org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.adapter;

import lombok.AllArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.DonneesEntree;
import org.mte.numecoeval.expositiondonneesentrees.domain.ports.output.EntreePersistencePort;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.repository.DonneesEntreesRepository;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.mapper.EntreeEntityMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class DonneesEntreesJpaAdapter implements EntreePersistencePort<DonneesEntree> {

    DonneesEntreesRepository repository;

    EntreeEntityMapper entreeEntityMapper;

    @Override
    public void save(DonneesEntree entree) {
        repository.save(entreeEntityMapper.toEntity(entree));
    }

    @Override
    public void saveAll(List<DonneesEntree> entrees) {
        CollectionUtils.emptyIfNull(entrees).forEach(this::save);
    }
}
