package org.mte.numecoeval.expositiondonneesentrees.domain.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@SuperBuilder
@Accessors(chain = true)
public class DemandeCalcul {
    String nomLot;
    String dateLot;
    String nomOrganisation;
    List<String> etapes;
    List<String> criteres;
}
