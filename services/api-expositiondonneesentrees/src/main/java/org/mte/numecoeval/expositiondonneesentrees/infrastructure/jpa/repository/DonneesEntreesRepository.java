package org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.repository;

import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.entity.DonneesEntreesEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DonneesEntreesRepository extends JpaRepository<DonneesEntreesEntity, Long> {
    List<DonneesEntreesEntity> findByNomLot(String nomLot);
}
