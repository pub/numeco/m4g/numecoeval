package org.mte.numecoeval.expositiondonneesentrees.infrastructure.mapper;

import org.mapstruct.Mapper;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.DemandeCalcul;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.RapportDemandeCalcul;
import org.mte.numecoeval.expositiondonneesentrees.generated.api.model.DemandeCalculRest;
import org.mte.numecoeval.expositiondonneesentrees.generated.api.model.RapportDemandeCalculRest;
import org.mte.numecoeval.expositiondonneesentrees.sync.calculs.generated.api.model.ReponseCalculRest;

@Mapper(componentModel = "spring")
public interface CalculRestMapper {

    DemandeCalcul toDomain(DemandeCalculRest restDTO);

    RapportDemandeCalculRest toRest(RapportDemandeCalcul domain);

    RapportDemandeCalcul toRest(ReponseCalculRest reponseCalculRest);
}
