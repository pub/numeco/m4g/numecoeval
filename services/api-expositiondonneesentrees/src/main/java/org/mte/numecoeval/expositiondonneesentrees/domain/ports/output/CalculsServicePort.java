package org.mte.numecoeval.expositiondonneesentrees.domain.ports.output;

import java.util.List;

public interface CalculsServicePort {
    void postSyncCalcul(List<Long> equipementPhysiqueIds, List<Long> messagerieIds);
}
