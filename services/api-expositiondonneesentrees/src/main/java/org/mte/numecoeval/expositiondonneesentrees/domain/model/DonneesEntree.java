package org.mte.numecoeval.expositiondonneesentrees.domain.model;

import lombok.*;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Accessors(chain = true)
public class DonneesEntree extends AbstractEntree {

    List<DataCenter> dataCenters = new ArrayList<>();

    List<EquipementPhysique> equipementsPhysiques = new ArrayList<>();

    List<EquipementVirtuel> equipementsVirtuels = new ArrayList<>();

    List<Application> applications = new ArrayList<>();

    List<Messagerie> messageries = new ArrayList<>();

    List<Entite> entites = new ArrayList<>();

    List<OperationNonIT> operationsNonIT = new ArrayList<>();

    String dureeUsage;
}
