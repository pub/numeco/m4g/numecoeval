package org.mte.numecoeval.expositiondonneesentrees.domain.ports.output;

import org.mte.numecoeval.expositiondonneesentrees.referentiels.generated.api.model.*;

import java.util.List;

public interface ReferentielServicePort {
    boolean hasMixElec(String pays, String nomOrganisation);

    List<TypeItemDTO> getAllTypesItem();

    CorrespondanceRefEquipementDTO getCorrespondance(String modele, String nomOrganisation);

    FacteurCaracterisationDTO getFacteurCaracterisation(String critere, String etape, String refItem, String nomOrganisation);

    List<EtapeDTO> getAllEtapes();

    List<CritereDTO> getAllCriteres();
}
