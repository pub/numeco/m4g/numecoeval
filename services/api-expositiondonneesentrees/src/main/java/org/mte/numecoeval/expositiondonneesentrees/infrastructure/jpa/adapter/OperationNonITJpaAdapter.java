package org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.adapter;

import lombok.AllArgsConstructor;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.OperationNonIT;
import org.mte.numecoeval.expositiondonneesentrees.domain.ports.output.EntreePersistencePort;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.repository.OperationNonITRepository;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.mapper.EntreeEntityMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class OperationNonITJpaAdapter implements EntreePersistencePort<OperationNonIT> {

    OperationNonITRepository repository;

    EntreeEntityMapper entreeEntityMapper;

    @Override
    public void save(OperationNonIT entree) {
        repository.save(entreeEntityMapper.toEntity(entree));
    }

    @Override
    public void saveAll(List<OperationNonIT> entrees) {
        repository.saveAll(entreeEntityMapper.toEntityListOperationNonIT(entrees));
    }


}
