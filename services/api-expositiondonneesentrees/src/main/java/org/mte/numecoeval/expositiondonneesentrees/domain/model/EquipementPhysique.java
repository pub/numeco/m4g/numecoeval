package org.mte.numecoeval.expositiondonneesentrees.domain.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@SuperBuilder
@Accessors(chain = true)
public class EquipementPhysique extends AbstractEntree {
    String nomEquipementPhysique;
    String modele;
    String type;
    String statut;
    String paysDUtilisation;
    String utilisateur;
    LocalDate dateAchat;
    LocalDate dateRetrait;
    Double dureeUsageInterne;
    Double dureeUsageAmont;
    Double dureeUsageAval;
    String nbCoeur;
    String nomCourtDatacenter;
    Double consoElecAnnuelle;
    boolean serveur;
    String nomEntite;
    Double quantite;
    String modeUtilisation;
    Double tauxUtilisation;
    String qualite;
}
