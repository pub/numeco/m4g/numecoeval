package org.mte.numecoeval.expositiondonneesentrees.domain.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ValidationException extends RuntimeException {

    final String erreur;

}
