package org.mte.numecoeval.expositiondonneesentrees.domain.exception;

public class RestException extends RuntimeException {

    public RestException(Throwable cause) {
        super(cause);
    }
}
