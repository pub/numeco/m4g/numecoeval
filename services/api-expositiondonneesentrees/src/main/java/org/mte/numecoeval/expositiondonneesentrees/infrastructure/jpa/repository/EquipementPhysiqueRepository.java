package org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.repository;

import org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.entity.EquipementPhysiqueEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

@Repository
public interface EquipementPhysiqueRepository extends JpaRepository<EquipementPhysiqueEntity, Long> {

    @Transactional
    @Modifying
    @Query("""
              UPDATE EquipementPhysiqueEntity eqp
              SET eqp.statutTraitement = :statut
              WHERE eqp.nomLot = :nomLot
              AND eqp.nomOrganisation = :nomOrganisation
              AND eqp.nomEquipementPhysique IN :noms
            """)
    int updateEquipementPhysiqueStatutTraitement(
            @Param("statut") String statut,
            @Param("nomLot") String nomLot,
            @Param("nomOrganisation") String nomOrganisation,
            @Param("noms") Set<String> noms
    );

    @Query("select id from #{#entityName} where nomLot = ?1 and statutTraitement = ?2")
    List<Long> getIdsByNomLotAndStatutTraitement(String nomLot, String statutTraitement);

    @Query(value = """
            WITH datacenters AS (
                SELECT nom_court_datacenter FROM en_data_center WHERE nom_lot = ?1
            )
            SELECT nom_equipement_physique FROM en_equipement_physique eep
            WHERE serveur = true
            AND nom_lot = ?1
            AND (nom_court_datacenter is null OR
            nom_court_datacenter NOT IN (SELECT nom_court_datacenter FROM datacenters))
            ORDER BY nom_equipement_physique
            """, nativeQuery = true)
    List<String> getEquipementPhysiqueSansDatacenter(String nomLot);

}