package org.mte.numecoeval.expositiondonneesentrees.infrastructure.cache;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class SchedulerEvictCache {

    @CacheEvict(value = "statutDesCalculs", allEntries = true)
    @Scheduled(fixedRateString = "${caching.spring.statutDesCalculs}")
    public void emptyCacheStatutDesCalculs() {
    }

    @CacheEvict(value = {
            "hasMixElec",
            "Etapes",
            "Criteres",
            "CorrespondanceRefEquipement",
            "ImpactEquipement",
            "FacteurCaracterisation"
    }, allEntries = true)
    @Scheduled(fixedRateString = "${caching.spring.referentiels}")
    public void emptyCacheReferentiel() {
    }
}
