package org.mte.numecoeval.expositiondonneesentrees.infrastructure.config;

import lombok.AllArgsConstructor;
import org.mte.numecoeval.expositiondonneesentrees.domain.ports.input.ImportDonneesEntreePort;
import org.mte.numecoeval.expositiondonneesentrees.domain.ports.input.impl.ImportDonneesEntreePortImpl;
import org.mte.numecoeval.expositiondonneesentrees.domain.ports.output.ReferentielServicePort;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.service.DefaultValueService;
import org.mte.numecoeval.expositiondonneesentrees.infrastructure.service.ErrorManagementService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@AllArgsConstructor
@ComponentScan(basePackages = "org.mte.numecoeval.expositiondonneesentrees.infrastructure.adapters")
public class ApplicationPortConfig {

    private ReferentielServicePort referentielServicePort;
    private ErrorManagementService errorManagementService;
    private DefaultValueService defaultValueService;
    private MessageProperties messageProperties;

    @Bean
    public ImportDonneesEntreePort importDonneesEntreePort() {
        return new ImportDonneesEntreePortImpl(referentielServicePort, errorManagementService, defaultValueService, messageProperties.getMessages());
    }
}
