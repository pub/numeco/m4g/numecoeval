package org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "EN_ENTITE")
@Entity
public class EntiteEntity extends AbstractEntreeEntity
{
    @Id
    @GeneratedValue(generator = "SEQ_EN_ENTITE", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "SEQ_EN_ENTITE", sequenceName="SEQ_EN_ENTITE",allocationSize=1000)
    @Column(nullable = false)
    private Long id;

    private String nomEntite;
    private Integer nbCollaborateurs;
    private String responsableEntite;
    private String responsableNumeriqueDurable;


}
