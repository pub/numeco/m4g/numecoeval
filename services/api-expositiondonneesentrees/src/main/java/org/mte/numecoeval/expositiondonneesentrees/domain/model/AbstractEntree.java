package org.mte.numecoeval.expositiondonneesentrees.domain.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
@SuperBuilder
@NoArgsConstructor
public abstract class AbstractEntree {

    protected String idLot;
    protected Long id;
    /**
     * Nom du lot de données
     */
    protected String nomLot;
    /**
     * Nom de l'organisation liée aux données - Metadata
     */
    protected String nomOrganisation;
    /**
     * Nom de la source de données - Metadata
     */
    protected String nomSourceDonnee;
    /**
     * La date du lot permet d’agréger les données provenant de différentes sources et d'en faire un suivi temporel
     */
    protected LocalDate dateLot;
    /**
     * Date de création
     */
    protected LocalDateTime dateCreation;
    /**
     * Date de dernière mise à jour
     */
    protected LocalDateTime dateUpdate;
    /**
     * Statut du traitement de la donnée dans NumEcoEval
     */
    protected String statutTraitement;


}
