package org.mte.numecoeval.expositiondonneesentrees.domain.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@SuperBuilder
@Accessors(chain = true)
public class Application extends AbstractEntree {
    String nomApplication;
    String typeEnvironnement;
    String nomEquipementVirtuel;
    String nomSourceDonneeEquipementVirtuel;
    String domaine;
    String sousDomaine;
    String nomEntite;
    String nomEquipementPhysique;
    String qualite;
}
