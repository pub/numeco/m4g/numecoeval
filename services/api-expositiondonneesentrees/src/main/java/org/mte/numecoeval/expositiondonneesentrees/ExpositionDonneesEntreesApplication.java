package org.mte.numecoeval.expositiondonneesentrees;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class ExpositionDonneesEntreesApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExpositionDonneesEntreesApplication.class, args);
    }

}
