package org.mte.numecoeval.expositiondonneesentrees.infrastructure.adapters;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.expositiondonneesentrees.domain.exception.RestException;
import org.mte.numecoeval.expositiondonneesentrees.sync.calculs.generated.api.client.CalculsEquipementPhysiqueEtOperationNonItEtMessagerieApi;
import org.mte.numecoeval.expositiondonneesentrees.sync.calculs.generated.api.model.ReponseCalculRest;
import org.mte.numecoeval.expositiondonneesentrees.sync.calculs.generated.api.model.SyncCalculRest;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import java.util.List;

@Service
@Slf4j
@AllArgsConstructor
public class CalculsRestClient {

    private CalculsEquipementPhysiqueEtOperationNonItEtMessagerieApi calculsEquipementPhysiqueEtOperationNonItEtMessagerieApi;

    public ReponseCalculRest postSyncCalcul(List<Long> equipementPhysiqueIds, List<Long> operationNonITIds, List<Long> messagerieIds) {
        try {

            var syncCalculRest = new SyncCalculRest();
            syncCalculRest.setEquipementPhysiqueIds(equipementPhysiqueIds);
            syncCalculRest.setOperationNonITIds(operationNonITIds);
            syncCalculRest.setMessagerieIds(messagerieIds);

            return calculsEquipementPhysiqueEtOperationNonItEtMessagerieApi.syncCalculByIds(syncCalculRest).block();

        } catch (WebClientResponseException e) {
            throw new RestException(e);
        }

    }

}
