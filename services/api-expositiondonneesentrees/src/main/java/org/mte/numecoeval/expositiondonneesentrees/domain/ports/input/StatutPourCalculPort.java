package org.mte.numecoeval.expositiondonneesentrees.domain.ports.input;

import org.mte.numecoeval.expositiondonneesentrees.generated.api.model.StatutCalculRest;

public interface StatutPourCalculPort {

    /**
     * Génère le statut global des calculs
     *
     * @param nomLot          nom du lot
     * @param nomOrganisation nom organisation
     * @return le StatutCalculRest
     */
    StatutCalculRest statutDesCalculs(String nomLot, String nomOrganisation);

}
