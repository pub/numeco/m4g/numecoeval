package org.mte.numecoeval.expositiondonneesentrees.infrastructure.jpa.entity;

import jakarta.persistence.MappedSuperclass;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@MappedSuperclass
public abstract class AbstractEntreeEntity {

    @CreationTimestamp
    protected LocalDateTime dateCreation;

    @UpdateTimestamp
    protected LocalDateTime dateUpdate;

    /**
     * Nom de l'organisation liée aux données - Metadata
     */
    protected String nomOrganisation;

    /**
     * Nom de la source de données - Metadata
     */
    protected String nomSourceDonnee;

    /**
     * Nom du lot de données
     */
    protected String nomLot;

    /**
     * La date du lot permet d’agréger les données provenant de différentes sources et d'en faire un suivi temporel
     */
    protected LocalDate dateLot;

    /**
     * Statut du traitement de la donnée dans NumEcoEval
     */
    protected String statutTraitement;
}
