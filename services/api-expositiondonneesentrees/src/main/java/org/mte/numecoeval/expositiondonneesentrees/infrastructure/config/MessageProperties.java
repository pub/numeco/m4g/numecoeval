package org.mte.numecoeval.expositiondonneesentrees.infrastructure.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@EnableConfigurationProperties
@ConfigurationProperties
@Getter
@Setter
public class MessageProperties {

    private Map<String, String> messages;

}
