package org.mte.numecoeval.expositiondonneesentrees.domain.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@SuperBuilder
@Accessors(chain = true)
public class OperationNonIT extends AbstractEntree {
    String nomItemNonIT;
    Double quantite;
    String type;
    Double dureeDeVie;
    String localisation;
    String nomEntite;
    String nomCourtDatacenter;
    String description;
    Double consoElecAnnuelle;
    String qualite;
}
