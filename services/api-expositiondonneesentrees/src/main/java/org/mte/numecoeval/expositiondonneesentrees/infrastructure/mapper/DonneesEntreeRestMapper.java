package org.mte.numecoeval.expositiondonneesentrees.infrastructure.mapper;

import org.mapstruct.Mapper;
import org.mte.numecoeval.expositiondonneesentrees.domain.model.RapportImport;
import org.mte.numecoeval.expositiondonneesentrees.generated.api.model.RapportImportRest;

@Mapper(componentModel = "spring")
public interface DonneesEntreeRestMapper {

    RapportImportRest toRestDTO(RapportImport rapportImport);

}
