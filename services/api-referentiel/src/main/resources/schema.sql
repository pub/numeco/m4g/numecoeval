create or replace function create_constraint_if_not_exists (
    t_name text, c_name text, constraint_sql text
)
returns void AS
'
begin
    -- Look for our constraint
    if not exists (select constraint_name
                   from information_schema.constraint_column_usage
                   where table_name = t_name  and constraint_name = c_name) then
        execute ''ALTER TABLE '' || t_name || '' ADD CONSTRAINT '' || c_name || '' '' || constraint_sql;
    end if;
end;
' language 'plpgsql';

CREATE TABLE IF NOT EXISTS ref_etapeacv
(
    code    varchar(255) NOT NULL,
    libelle varchar(255) NULL,
    CONSTRAINT ref_etapeacv_pkey PRIMARY KEY (code)
);

CREATE TABLE IF NOT EXISTS ref_critere
(
    nom_critere varchar(255) NOT NULL,
    description varchar(255) NULL,
    unite       varchar(255) NULL,
    CONSTRAINT ref_critere_pkey PRIMARY KEY (nom_critere)
);

CREATE TABLE IF NOT EXISTS ref_hypothese
(
    code     varchar(255) NOT NULL,
    "source" varchar(255) NULL,
    valeur   varchar(255) NULL,
    description   varchar(255) NULL,
    nom_organisation   varchar(255) NOT NULL,
    CONSTRAINT ref_hypothese_pkey PRIMARY KEY (code, nom_organisation)
);

CREATE TABLE IF NOT EXISTS ref_type_item
(
    "type"                    varchar(255) NOT NULL,
    categorie                 varchar(255) NULL,
    commentaire               varchar(255) NULL,
    duree_vie_defaut          float8       NULL,
    ref_conso_moyenne         varchar(255) NULL,
    serveur                   bool         NOT NULL,
    "source"                  varchar(255) NULL,
    ref_item_par_defaut       varchar(255) NULL,
    nom_organisation          varchar(255) NOT NULL,
    CONSTRAINT ref_type_item_pkey PRIMARY KEY (type, nom_organisation)
);

CREATE TABLE IF NOT EXISTS ref_impact_messagerie
(
    constante_coefficient_directeur float8       NULL,
    constante_ordonnee_origine      float8       NULL,
    "source"                        varchar(255) NULL,
    nom_critere                     varchar(255) NOT NULL,
    CONSTRAINT ref_impact_messagerie_pkey PRIMARY KEY (nom_critere)
);

CREATE TABLE IF NOT EXISTS ref_correspondance_ref_eqp
(
    modele_equipement_source varchar(255) NOT NULL,
    ref_equipement_cible     varchar(255) NULL,
    nom_organisation         varchar(255) NOT NULL,
    CONSTRAINT ref_correspondance_ref_eqp_pkey PRIMARY KEY (modele_equipement_source, nom_organisation)
);
    CREATE TABLE IF NOT EXISTS ref_facteurcaracterisation
(
     nom      varchar(255) NOT NULL,
     etapeacv           varchar(255) NOT NULL,
     nomcritere         varchar(255) NOT NULL,
     description        varchar(255) NULL,
     niveau             varchar(255) NULL,
     tiers              varchar(255) NULL,
     categorie          varchar(255) NULL,
     conso_elec_moyenne float8       NULL,
     localisation       varchar(255) NULL,
     valeur             float8       NULL,
     unite              varchar(255) NULL,
     source             varchar(255) NULL,
     nom_organisation   varchar(255) NOT NULL,
     CONSTRAINT ref_facteurcaracterisation_pkey PRIMARY KEY (nom, etapeacv, nomcritere, nom_organisation)
);
-- suppression des contraintes de clés étrangères
ALTER TABLE ref_impact_messagerie DROP CONSTRAINT IF EXISTS fkohnlpwfp0ebk7dswmfbe5l3k0;
ALTER TABLE IF EXISTS ref_type_item ADD COLUMN IF NOT EXISTS nom_organisation varchar(255) DEFAULT '';
ALTER TABLE IF EXISTS ref_facteurcaracterisation ADD COLUMN IF NOT EXISTS nom_organisation varchar(255) DEFAULT '';
ALTER TABLE IF EXISTS ref_hypothese ADD COLUMN IF NOT EXISTS nom_organisation varchar(255) DEFAULT '';
ALTER TABLE IF EXISTS ref_correspondance_ref_eqp ADD COLUMN IF NOT EXISTS nom_organisation varchar(255) DEFAULT '';

-- suppression des anciennes clés primaires et ajout du nom_organisation à la clé

ALTER TABLE ref_hypothese DROP CONSTRAINT IF EXISTS ref_hypothese_pkey;
SELECT create_constraint_if_not_exists('ref_hypothese', 'ref_hypothese_org_pkey', 'PRIMARY KEY (code, nom_organisation);');
ALTER TABLE ref_type_item DROP CONSTRAINT IF EXISTS ref_type_item_pkey;
SELECT create_constraint_if_not_exists('ref_type_item', 'ref_type_item_org_pkey', 'PRIMARY KEY (type, nom_organisation);');
ALTER TABLE ref_correspondance_ref_eqp DROP CONSTRAINT IF EXISTS ref_correspondance_ref_eqp_pkey;
SELECT create_constraint_if_not_exists('ref_correspondance_ref_eqp', 'ref_correspondance_ref_eqp_org_pkey', 'PRIMARY KEY (modele_equipement_source, nom_organisation);');
ALTER TABLE ref_facteurcaracterisation DROP CONSTRAINT IF EXISTS ref_facteurcaracterisation_pkey;
SELECT create_constraint_if_not_exists('ref_facteurcaracterisation', 'ref_facteurcaracterisation_org_pkey', 'PRIMARY KEY (nom, etapeacv, nomcritere, nom_organisation);');