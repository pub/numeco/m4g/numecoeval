package org.mte.numecoeval.referentiel.infrastructure.mapper;

import org.mapstruct.Mapper;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.ImpactMessagerieDTO;
import org.mte.numecoeval.referentiel.domain.model.ImpactMessagerie;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.ImpactMessagerieEntity;

import java.util.Collection;
import java.util.List;

@Mapper(componentModel = "spring")
public interface ImpactMessagerieMapper {

    ImpactMessagerieEntity toEntity(ImpactMessagerie impactMessagerie);
    List<ImpactMessagerieEntity> toEntities(Collection<ImpactMessagerie> impactMessageries);

    ImpactMessagerie toDomain(ImpactMessagerieEntity impactMessagerieEntity);
    ImpactMessagerie toDomain(ImpactMessagerieDTO dto);
    List<ImpactMessagerie> toDomains(List<ImpactMessagerieEntity> entities);
    List<ImpactMessagerie> toDomainsFromDTO(List<ImpactMessagerieDTO> dtos);

    List<ImpactMessagerieDTO> toDTOs(List<ImpactMessagerie> impactMessageries);
    ImpactMessagerieDTO toDTO(ImpactMessagerie impactMessagerie);
}
