package org.mte.numecoeval.referentiel.infrastructure.restapi.controller;

import jakarta.servlet.http.HttpServletResponse;
import org.mte.numecoeval.referentiel.domain.ports.output.ReferentielCsvExportService;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public interface BaseExportReferentiel<T> {

    default void exportCSV(HttpServletResponse servletResponse, ReferentielCsvExportService<T> csvExportService, String baseFilename) throws IOException {
        servletResponse.setStatus(200);
        servletResponse.setContentType("text/csv");
        servletResponse.setCharacterEncoding("UTF-8");
        servletResponse.addHeader("Content-Disposition","attachment; filename="+baseFilename+"-"+
                DateTimeFormatter.ofPattern("yyyy-MM-dd_hhmmss").format(LocalDateTime.now()) +".csv");
        csvExportService.writeToCsv(servletResponse.getWriter());
    }
}
