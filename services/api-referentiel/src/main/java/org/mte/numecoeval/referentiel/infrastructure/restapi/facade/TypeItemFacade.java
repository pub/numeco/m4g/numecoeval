package org.mte.numecoeval.referentiel.infrastructure.restapi.facade;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.domain.model.TypeItem;
import org.mte.numecoeval.referentiel.domain.model.id.TypeItemId;
import org.mte.numecoeval.referentiel.domain.ports.output.ReferentielPersistencePort;
import org.mte.numecoeval.referentiel.infrastructure.mapper.TypeItemMapper;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.TypeItemDTO;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.id.TypeItemIdDTO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@AllArgsConstructor
public class TypeItemFacade {

    private ReferentielPersistencePort<TypeItem, TypeItemId> persistencePort;
    private TypeItemMapper mapper;

    public TypeItemDTO getTypeItemForType(TypeItemIdDTO typeItemIdDTO) throws ReferentielException {
        TypeItem typeItem = persistencePort.get(mapper.toDomain(typeItemIdDTO));
        return mapper.toDTO(typeItem);
    }

    public List<TypeItemDTO> getAllTypesItem() {
        return persistencePort.getAll().stream()
                .map(TypeItem -> mapper.toDTO(TypeItem))
                .toList();
    }

    public void purgeByOrganisationAndAddAll(List<TypeItemDTO> typeItems, String nomOrganisation) throws ReferentielException {
        persistencePort.purgeByOrganisation(nomOrganisation);
        persistencePort.saveAll(mapper.toDomainsDTO(typeItems));
    }

}
