package org.mte.numecoeval.referentiel.infrastructure.restapi.controller.impactmessagerie;

import jakarta.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.domain.ports.input.impl.ImportImpactMessageriePortImpl;
import org.mte.numecoeval.referentiel.infrastructure.adapter.export.ImpactMessagerieCsvExportService;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.ImpactMessagerieEntity;
import org.mte.numecoeval.referentiel.infrastructure.restapi.controller.BaseExportReferentiel;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.ImpactMessagerieDTO;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.RapportImportDTO;
import org.mte.numecoeval.referentiel.infrastructure.restapi.facade.ImpactMessagerieFacade;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.util.List;

@RestController
@Slf4j
@AllArgsConstructor
public class ReferentielImpactMessagerieRestApiImpl implements BaseExportReferentiel<ImpactMessagerieEntity>, ReferentielInterneImpactMessagerieRestApi, ReferentielImpactMessagerieRestApi {

    private ImpactMessagerieFacade impactMessagerieFacade;

    private ImpactMessagerieCsvExportService csvExportService;

    @Override
    public RapportImportDTO importCSV(MultipartFile fichier) throws IOException, ReferentielException {
        if (fichier == null || fichier.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Le fichier n'existe pas ou alors il est vide");
        }
        var rapportImport = new ImportImpactMessageriePortImpl().importCSV(fichier.getInputStream());
        impactMessagerieFacade.purgeAndAddAll(rapportImport.getObjects());
        return new RapportImportDTO(
                fichier.getOriginalFilename(),
                rapportImport.getErreurs(),
                rapportImport.getNbrLignesImportees()
        );
    }

    @Override
    public ImpactMessagerieDTO getImpactMessagerie(String critere) {
        return impactMessagerieFacade.getImpactMessagerieForCritere(critere);
    }

    @Override
    public List<ImpactMessagerieDTO> getAllImpactMessagerie() {
        return impactMessagerieFacade.getAllImpactMessagerie();
    }

    @Override
    public void exportCSV(HttpServletResponse servletResponse) throws IOException {
        exportCSV(servletResponse, csvExportService, "impactMessagerie");
    }
}
