package org.mte.numecoeval.referentiel.infrastructure.jpa.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import lombok.*;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.id.TypeItemIdEntity;

@Builder
@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@IdClass(TypeItemIdEntity.class)
@AllArgsConstructor
@Entity(name = "REF_TYPE_ITEM")
public class TypeItemEntity implements AbstractReferentielEntity {
    @Id
    String type;
    // Nom de l'organisation dans le cas où cette dernière à des références customisées, propres à son organisation
    @Id
    String nomOrganisation;
    String categorie;
    boolean serveur;
    String commentaire;
    Double dureeVieDefaut;
    // ref de l'hypothèse nécesssaire pour le calcul de l'impact de cet l'item
    String refHypothese;
    String source;
    // Référence de l'item par défaut, permet des correspondances en cas d'absence de correspondance directe.
    String refItemParDefaut;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        TypeItemEntity that = (TypeItemEntity) o;

        return new EqualsBuilder().append(categorie, that.categorie).append(serveur, that.serveur).append(type, that.type).append(commentaire, that.commentaire).append(dureeVieDefaut, that.dureeVieDefaut).append(refHypothese, that.refHypothese).append(source, that.source).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(type).append(categorie).append(serveur).append(commentaire).append(dureeVieDefaut).append(refHypothese).append(source).toHashCode();
    }
}
