package org.mte.numecoeval.referentiel.domain.model;

import lombok.*;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
public class FacteurCaracterisation implements AbstractReferentiel {
    String nom;
    String etape;
    String critere;
    String description;
    String niveau;
    String tiers;
    String categorie;
    Double consoElecMoyenne;
    String localisation;
    Double valeur;
    String unite;
    String source;
    // Nom de l'organisation dans le cas où cette dernière à des références customisées, propres à son organisation
    String nomOrganisation;
}
