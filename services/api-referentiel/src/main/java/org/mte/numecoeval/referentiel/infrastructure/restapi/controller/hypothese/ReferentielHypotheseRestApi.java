package org.mte.numecoeval.referentiel.infrastructure.restapi.controller.hypothese;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.ErrorResponseDTO;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.HypotheseDTO;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

public interface ReferentielHypotheseRestApi {

    @Operation(
            summary = "Endpoint interne à NumEcoEval - Récupération d'une hypothèse par son code",
            description = """
                    Endpoint interne utilisé dans la génération des indicateurs par le module api-calcul de NumEcoEval.
                    Renvoie une hypothèse en fonction de sa clé.
                    """,
            tags = "Interne NumEcoEval",
            operationId = "getHypothese"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Hypothèse trouvée",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = HypotheseDTO.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ErrorResponseDTO.class))}),
            @ApiResponse(responseCode = "404", description = "Hypothèse non trouvée", content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ErrorResponseDTO.class))})})
    @GetMapping(path = "/referentiel/hypothese", produces = MediaType.APPLICATION_JSON_VALUE)
    HypotheseDTO get(
            @RequestParam("cle") @Schema(description = "Clé de l'hypothèse") final String cle,
            @RequestParam(value = "nomOrganisation", defaultValue = "")
            @Schema(description = "Choix de l'organisation pour laquelle on veut exporter des hypothèses, si ce sont des références par défaut, communes à toutes les organisations, laisser le paramètre vide", defaultValue = "") String nomOrganisation
    );

}
