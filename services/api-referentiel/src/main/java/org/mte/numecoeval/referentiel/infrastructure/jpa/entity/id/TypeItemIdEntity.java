package org.mte.numecoeval.referentiel.infrastructure.jpa.entity.id;

import lombok.*;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@Accessors(chain = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@EqualsAndHashCode
public class TypeItemIdEntity implements AbstractReferentieIdEntity {

    String type;
    String nomOrganisation;
    
}
