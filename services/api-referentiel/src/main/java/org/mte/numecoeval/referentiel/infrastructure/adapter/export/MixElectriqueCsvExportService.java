package org.mte.numecoeval.referentiel.infrastructure.adapter.export;

import org.apache.commons.csv.CSVPrinter;
import org.mte.numecoeval.referentiel.domain.model.MixElectrique;
import org.mte.numecoeval.referentiel.domain.ports.input.impl.ImportMixElectriquePortImpl;
import org.mte.numecoeval.referentiel.domain.ports.output.ReferentielCsvExportService;
import org.mte.numecoeval.referentiel.infrastructure.jpa.repository.FacteurCaracterisationRepository;
import org.mte.numecoeval.referentiel.infrastructure.mapper.FacteurCaracterisationMapper;
import org.mte.numecoeval.referentiel.infrastructure.mapper.MixElectriqueMapper;
import org.mte.numecoeval.referentiel.utils.Constants;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class MixElectriqueCsvExportService extends ReferentielCsvExportService<MixElectrique> {

    private final FacteurCaracterisationRepository repository;
    private final FacteurCaracterisationMapper facteurCaracterisationMapper;
    private final MixElectriqueMapper mixElectriqueMapper;

    public MixElectriqueCsvExportService(FacteurCaracterisationRepository repository, FacteurCaracterisationMapper facteurCaracterisationMapper, MixElectriqueMapper mixElectriqueMapper) {
        super(MixElectrique.class);
        this.repository = repository;
        this.facteurCaracterisationMapper = facteurCaracterisationMapper;
        this.mixElectriqueMapper = mixElectriqueMapper;
    }

    @Override
    public String[] getHeaders() {
        return ImportMixElectriquePortImpl.getHeaders();
    }

    @Override
    public List<MixElectrique> getObjectsToWrite() {
        var facteurCaracterisationList = facteurCaracterisationMapper.toDomains(repository.findByNiveauAndCategorie(Constants.MIXELEC_NIVEAU, Constants.MIXELEC_CATEGORIE));
        return mixElectriqueMapper.toMixElectriques(facteurCaracterisationList);
    }

    @Override
    protected String getObjectId(MixElectrique object) {
        return String.join("-", object.getPays(), object.getCritere());
    }

    @Override
    public void printRecord(CSVPrinter csvPrinter, MixElectrique objectToWrite) throws IOException {
        csvPrinter.printRecord(
                objectToWrite.getPays(),
                objectToWrite.getRaccourcisAnglais(),
                objectToWrite.getCritere(),
                formatDouble(objectToWrite.getValeur()),
                objectToWrite.getSource()
        );
    }

}
