package org.mte.numecoeval.referentiel.infrastructure.restapi.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
@EqualsAndHashCode
@Builder
@Schema(
        description = "Référentiel des types d'items utilisables dans le système. La clé du référentiel est le champ type."
)
public class TypeItemDTO implements Serializable {
    @Schema(
            description = "Type d'item, clé du référentiel"
    )
    String type;
    @Schema(
            description = "Catégorie de l'item"
    )
    String categorie;
    @Schema(
            description = "Flag indiquant si l'item est un serveur"
    )
    boolean serveur;
    @Schema(
            description = "Commentaire de l'entrée dans le référentiel"
    )
    String commentaire;
    @Schema(
            description = "Durée de vie par défaut de ce type d'item"
    )
    Double dureeVieDefaut;
    @Schema(
            description = "Référence vers l'hypothèse nécessaire pour le calcul d'impact de cet item"
    )
    String refHypothese;
    @Schema(
            description = "Source de l'information du référentiel"
    )
    String source;
    @Schema(
            description = "Référence de l'item par défaut, permet des correspondances en cas d'absence de correspondance directe"
    )
    String refItemParDefaut;
    @Schema(
            description = "Nom de l'organisation dans le cas où cette dernière à des références customisées, propres à son organisation"
    )
    String nomOrganisation;

}
