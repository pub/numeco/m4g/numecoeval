package org.mte.numecoeval.referentiel.infrastructure.restapi.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
@EqualsAndHashCode
@Builder
@Schema(
        description = "Référentiel de correspondance entre un modèle d'équipement physique et une référence d'équipement dans les référentiels."
)
public class CorrespondanceRefEquipementDTO implements Serializable {
    @Schema(
            description = "Modèle de l'équipement, clé du référentiel"
    )
    String modeleEquipementSource;
    @Schema(
            description = "Référence d'équipement correspondant au modèle de l'équipement"
    )
    String refEquipementCible;
    @Schema(
            description = "Nom de l'organisation dans le cas où cette dernière à des références customisées, propres à son organisation"
    )
    String nomOrganisation;
}
