package org.mte.numecoeval.referentiel.infrastructure.mapper;

import org.mapstruct.Mapper;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.CritereDTO;
import org.mte.numecoeval.referentiel.domain.model.Critere;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.CritereEntity;

import java.util.Collection;
import java.util.List;

@Mapper(componentModel = "spring")
public interface CritereMapper {
    List<Critere> toDomainsFromDTO(List<CritereDTO> criteresDTO);

    List<CritereEntity> toEntities(Collection<Critere> referentiel);

    List<CritereDTO> toDTO(List<Critere> all);

    List<Critere> toDomains(List<CritereEntity> all);
}
