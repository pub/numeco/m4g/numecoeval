package org.mte.numecoeval.referentiel.infrastructure.adapter.export;

import org.apache.commons.csv.CSVPrinter;
import org.mte.numecoeval.referentiel.domain.ports.input.impl.ImportFacteurCaracterisationPortImpl;
import org.mte.numecoeval.referentiel.domain.ports.output.ReferentielCsvExportService;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.FacteurCaracterisationEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.repository.FacteurCaracterisationRepository;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class FacteurCaracterisationCsvExportService extends ReferentielCsvExportService<FacteurCaracterisationEntity> {
    private final FacteurCaracterisationRepository repository;

    public FacteurCaracterisationCsvExportService(FacteurCaracterisationRepository repository) {
        super(FacteurCaracterisationEntity.class);
        this.repository = repository;
    }

    @Override
    public String[] getHeaders() {
        return ImportFacteurCaracterisationPortImpl.getHeaders();
    }

    @Override
    public List<FacteurCaracterisationEntity> getObjectsToWrite() {
        return repository.findAll();
    }

    @Override
    protected String getObjectId(FacteurCaracterisationEntity object) {
        return String.join("-", object.getNom(), object.getEtape(), object.getCritere());
    }

    @Override
    public void printRecord(CSVPrinter csvPrinter, FacteurCaracterisationEntity objectToWrite) throws IOException {
        csvPrinter.printRecord(objectToWrite.getNom(),
                objectToWrite.getEtape(), objectToWrite.getCritere(), objectToWrite.getDescription(),
                objectToWrite.getNiveau(), objectToWrite.getTiers(), objectToWrite.getCategorie(),
                formatDouble(objectToWrite.getConsoElecMoyenne()), objectToWrite.getLocalisation(),
                formatDouble(objectToWrite.getValeur()), objectToWrite.getUnite(),
                objectToWrite.getSource(), objectToWrite.getNomOrganisation()
        );
    }
}
