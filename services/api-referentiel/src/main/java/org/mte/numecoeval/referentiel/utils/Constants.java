package org.mte.numecoeval.referentiel.utils;

public class Constants {

    public static final String EQUIPEMENT_NIVEAU = "2-Equipement";
    public static final String MIXELEC_NIVEAU = "0-Base data";
    public static final String MIXELEC_CATEGORIE = "electricity-mix";
    public static final String MIXELEC_ETAPEACV = "FABRICATION";
    public static final String MIXELEC_NOM_LOW_VOLTAGE = "Electricity Mix/ Production mix/ Low voltage/ ";
    public static final String RESEAU_NIVEAU = "3-System";
    public static final String RESEAU_TIERS = "Network";
    public static final String CATEGORIE_EQUIPEMENT_PHYSIQUE = "EquipementPhysique";

}
