package org.mte.numecoeval.referentiel.infrastructure.jpa.entity;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.id.FacteurCaracterisationIdEntity;

@Getter
@Setter
@Accessors(chain = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@IdClass(FacteurCaracterisationIdEntity.class)
@Table(name = "REF_FACTEURCARACTERISATION")
@EqualsAndHashCode
public class FacteurCaracterisationEntity implements AbstractReferentielEntity {
    @Id
    String nom;
    @Id
    @Column(name = "etapeacv")
    String etape;
    @Id
    @Column(name = "nomcritere")
    String critere;
    // Nom de l'organisation dans le cas où cette dernière à des références customisées, propres à son organisation
    @Id
    String nomOrganisation;
    String localisation;
    String description;
    String niveau;
    String tiers;
    String categorie;
    Double consoElecMoyenne;

    Double valeur;
    String unite;
    String source;


}
