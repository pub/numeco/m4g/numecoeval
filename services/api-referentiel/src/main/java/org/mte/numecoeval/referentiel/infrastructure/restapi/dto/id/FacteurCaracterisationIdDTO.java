package org.mte.numecoeval.referentiel.infrastructure.restapi.dto.id;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@EqualsAndHashCode
public class FacteurCaracterisationIdDTO {
    String nom;
    String etape;
    String critere;
    String nomOrganisation;
}
