package org.mte.numecoeval.referentiel.domain.exception;

public class ReferentielRuntimeException extends RuntimeException {
    public ReferentielRuntimeException(String message) {
        super(message);
    }
}
