package org.mte.numecoeval.referentiel.infrastructure.jpa.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import jakarta.persistence.Table;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.id.CritereIdEntity;

@Getter
@Setter
@Accessors(chain = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@IdClass(CritereIdEntity.class)
@Table(name = "REF_CRITERE")
public class CritereEntity implements AbstractReferentielEntity {
    @Id
    String nomCritere;
    String unite;
    String description;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        CritereEntity that = (CritereEntity) o;

        return new EqualsBuilder().append(nomCritere, that.nomCritere).append(unite, that.unite).append(description, that.description).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(nomCritere).append(unite).append(description).toHashCode();
    }
}
