package org.mte.numecoeval.referentiel.infrastructure.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mte.numecoeval.referentiel.domain.model.FacteurCaracterisation;
import org.mte.numecoeval.referentiel.domain.model.ImpactEquipement;
import org.mte.numecoeval.referentiel.domain.model.id.ImpactEquipementId;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.ImpactEquipementDTO;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.id.ImpactEquipementIdDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ImpactEquipementMapper {
    ImpactEquipementId toDomainId(ImpactEquipementIdDTO id);

    ImpactEquipement toDomain(ImpactEquipementDTO dto);

    List<ImpactEquipement> toDomainsFromDTO(List<ImpactEquipementDTO> iesDTO);

    ImpactEquipementDTO toDTO(ImpactEquipement impactEquipement);

    @Mapping(target = "nom", source = "refEquipement")
    @Mapping(target = "niveau", expression = "java(org.mte.numecoeval.referentiel.utils.Constants.EQUIPEMENT_NIVEAU)")
    @Mapping(target = "nomOrganisation", constant = "")
    FacteurCaracterisation toFacteurCaracterisation(ImpactEquipement impactEquipement);

    List<FacteurCaracterisation> toFacteurCaracterisations(List<ImpactEquipement> impactEquipements);

    @Mapping(target = "refEquipement", source = "nom")
    ImpactEquipement toImpactEquipement(FacteurCaracterisation facteurCaracterisation);

    List<ImpactEquipement> toImpactEquipements(List<FacteurCaracterisation> facteurCaracterisations);

}
