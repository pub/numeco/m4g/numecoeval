package org.mte.numecoeval.referentiel.infrastructure.restapi.controller.critere;

import jakarta.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.CritereDTO;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.domain.ports.input.impl.ImportCriterePortImpl;
import org.mte.numecoeval.referentiel.infrastructure.adapter.export.CritereCsvExportService;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.CritereEntity;
import org.mte.numecoeval.referentiel.infrastructure.restapi.controller.BaseExportReferentiel;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.RapportImportDTO;
import org.mte.numecoeval.referentiel.infrastructure.restapi.facade.CritereFacade;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.util.List;

@RestController
@Slf4j
@AllArgsConstructor
public class ReferentielCritereRestApiImpl implements BaseExportReferentiel<CritereEntity>, ReferentielCritereRestApi, ReferentielAdministrationCritereRestApi {
    private CritereFacade referentielFacade;
    private CritereCsvExportService csvExportService;


    @Override
    public List<CritereDTO> getAll() {
        return referentielFacade.getAll();
    }

    @Override
    public RapportImportDTO importCSV(MultipartFile fichier) throws IOException, ReferentielException {

        if (fichier == null || fichier.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Le fichier n'existe pas ou alors il est vide");
        }
        var rapportImport = new ImportCriterePortImpl().importCSV(fichier.getInputStream());
        referentielFacade.purgeAndAddAll(rapportImport.getObjects());

        return new RapportImportDTO(
                fichier.getOriginalFilename(),
                rapportImport.getErreurs(),
                rapportImport.getNbrLignesImportees()
        );

    }

    @Override
    public void exportCSV(HttpServletResponse servletResponse) throws IOException {
        exportCSV(servletResponse, csvExportService, "criteres");
    }
}
