package org.mte.numecoeval.referentiel.infrastructure.restapi.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@EqualsAndHashCode
@Schema(
        description = "Référentiel de l'impact écologique d'un facteur de caractérisation. La clé est composé des champs nom, etape et critere."
)
public class FacteurCaracterisationDTO {
    @Schema(
            description = "Nom du facteur de caractérisation, fait partie de la clé dans le référentiel"
    )
    String nom;
    @Schema(
            description = "Étape ACV concernée, fait partie de la clé dans le référentiel"
    )
    String etape;
    @Schema(
            description = "Critère d'impact écologique concerné, fait partie de la clé dans le référentiel"
    )
    String critere;
    @Schema(
            description = "Description de l'entrée dans le référentiel"
    )
    String description;
    @Schema(
            description = "Niveau du facteur de caractérisation concerné"
    )
    String niveau;
    @Schema(
            description = "Tiers du facteur de caractérisation concerné"
    )
    String tiers;
    @Schema(
            description = "Catégorie du facteur de caractérisation concerné"
    )
    String categorie;
    @Schema(
            description = "Consommation électrique moyenne"
    )
    Double consoElecMoyenne;
    @Schema(
            description = "Localisation du facteur de caractérisation concerné"
    )
    String localisation;
    @Schema(
            description = "Valeur de l'impact écologique"
    )
    Double valeur;
    @Schema(
            description = "Unité du critère d'impact écologique"
    )
    String unite;
    @Schema(
            description = "Source de l'impact écologique pour ce facteur de caractérisation"
    )
    String source;
    @Schema(
            description = "Nom de l'organisation dans le cas où cette dernière à des références customisées, propres à son organisation"
    )
    String nomOrganisation;
}
