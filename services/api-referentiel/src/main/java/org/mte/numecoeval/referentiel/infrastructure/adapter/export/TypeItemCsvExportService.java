package org.mte.numecoeval.referentiel.infrastructure.adapter.export;

import org.apache.commons.csv.CSVPrinter;
import org.mte.numecoeval.referentiel.domain.ports.input.impl.ImportTypeItemPortImpl;
import org.mte.numecoeval.referentiel.domain.ports.output.ReferentielCsvExportService;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.TypeItemEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.repository.TypeItemRepository;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class TypeItemCsvExportService extends ReferentielCsvExportService<TypeItemEntity> {

    private final TypeItemRepository repository;

    public TypeItemCsvExportService(TypeItemRepository repository) {
        super(TypeItemEntity.class);
        this.repository = repository;
    }

    @Override
    public String[] getHeaders() {
        return ImportTypeItemPortImpl.getHeaders();
    }

    @Override
    public List<TypeItemEntity> getObjectsToWrite() {
        return repository.findAll();
    }

    @Override
    protected String getObjectId(TypeItemEntity object) {
        return object.getType();
    }

    @Override
    public void printRecord(CSVPrinter csvPrinter, TypeItemEntity objectToWrite) throws IOException {
        csvPrinter.printRecord(objectToWrite.getType(),
                objectToWrite.getCategorie(),
                objectToWrite.isServeur(),
                objectToWrite.getCommentaire(),
                formatDouble(objectToWrite.getDureeVieDefaut()),
                objectToWrite.getRefHypothese(),
                objectToWrite.getSource(), objectToWrite.getRefItemParDefaut(),
                objectToWrite.getNomOrganisation());
    }

}
