package org.mte.numecoeval.referentiel.infrastructure.jpa.repository;

import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.transaction.Transactional;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.CorrespondanceRefEquipementEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.id.CorrespondanceRefEquipementIdEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Optional;

@RepositoryRestResource(path = "CorrespondanceRefEquipement", itemResourceRel = "CorrespondancesRefEquipement")
@Tag(name = "CorrespondanceRefEquipement")
public interface CorrespondanceRefEquipementRepository extends JpaRepository<CorrespondanceRefEquipementEntity, CorrespondanceRefEquipementIdEntity> {
    Optional<CorrespondanceRefEquipementEntity> findById(CorrespondanceRefEquipementIdEntity id);

    @Transactional
    @Modifying
    int deleteByNomOrganisation(String nomOrganisation);
}
