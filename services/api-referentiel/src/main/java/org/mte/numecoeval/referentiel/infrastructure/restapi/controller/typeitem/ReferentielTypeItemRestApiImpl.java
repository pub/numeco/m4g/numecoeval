package org.mte.numecoeval.referentiel.infrastructure.restapi.controller.typeitem;

import jakarta.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.domain.ports.input.impl.ImportTypeItemPortImpl;
import org.mte.numecoeval.referentiel.infrastructure.adapter.export.TypeItemCsvExportService;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.TypeItemEntity;
import org.mte.numecoeval.referentiel.infrastructure.restapi.controller.BaseExportReferentiel;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.RapportImportDTO;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.TypeItemDTO;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.id.TypeItemIdDTO;
import org.mte.numecoeval.referentiel.infrastructure.restapi.facade.TypeItemFacade;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.util.List;

@RestController
@Slf4j
@AllArgsConstructor
public class ReferentielTypeItemRestApiImpl implements BaseExportReferentiel<TypeItemEntity>, ReferentielTypeItemRestApi, ReferentielAdministrationTypeItemRestApi {

    private TypeItemFacade typeItemFacade;

    private TypeItemCsvExportService csvExportService;

    @Override
    public RapportImportDTO importCSV(MultipartFile fichier, String nomOrganisation) throws IOException, ReferentielException {
        if (fichier == null || fichier.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Le fichier n'existe pas ou alors il est vide");
        }
        var rapportImport = new ImportTypeItemPortImpl().importCSV(fichier.getInputStream(), nomOrganisation);
        typeItemFacade.purgeByOrganisationAndAddAll(rapportImport.getObjects(), nomOrganisation);

        return new RapportImportDTO(
                fichier.getOriginalFilename(),
                rapportImport.getErreurs(),
                rapportImport.getNbrLignesImportees()
        );
    }

    @SneakyThrows
    @Override
    public TypeItemDTO getTypeItem(String type, String nomOrganisation) {
        TypeItemIdDTO typeItemIdDTO = new TypeItemIdDTO();
        typeItemIdDTO.setType(type);
        typeItemIdDTO.setNomOrganisation(nomOrganisation);
        return typeItemFacade.getTypeItemForType(typeItemIdDTO);
    }

    @Override
    public List<TypeItemDTO> getTypesItem() {
        return typeItemFacade.getAllTypesItem();
    }

    @Override
    public void exportCSV(HttpServletResponse servletResponse) throws IOException {
        exportCSV(servletResponse, csvExportService, "typeItem");
    }
}
