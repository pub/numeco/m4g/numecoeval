package org.mte.numecoeval.referentiel.infrastructure.adapter.export;

import org.apache.commons.csv.CSVPrinter;
import org.mte.numecoeval.referentiel.domain.model.TypeEquipement;
import org.mte.numecoeval.referentiel.domain.model.TypeItem;
import org.mte.numecoeval.referentiel.domain.ports.input.impl.ImportTypeEquipementPortImpl;
import org.mte.numecoeval.referentiel.domain.ports.output.ReferentielCsvExportService;
import org.mte.numecoeval.referentiel.infrastructure.jpa.repository.TypeItemRepository;
import org.mte.numecoeval.referentiel.infrastructure.mapper.TypeEquipementMapper;
import org.mte.numecoeval.referentiel.infrastructure.mapper.TypeItemMapper;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

import static org.mte.numecoeval.referentiel.utils.Constants.CATEGORIE_EQUIPEMENT_PHYSIQUE;

@Service
public class TypeEquipementCsvExportService extends ReferentielCsvExportService<TypeEquipement> {

    private final TypeItemRepository repository;
    private final TypeItemMapper typeItemMapper;
    private final TypeEquipementMapper typeEquipementMapper;


    public TypeEquipementCsvExportService(TypeItemRepository repository, TypeItemMapper typeItemMapper, TypeEquipementMapper typeEquipementMapper) {
        super(TypeEquipement.class);
        this.repository = repository;
        this.typeItemMapper = typeItemMapper;
        this.typeEquipementMapper = typeEquipementMapper;
    }

    @Override
    public String[] getHeaders() {
        return ImportTypeEquipementPortImpl.getHeaders();
    }

    @Override
    public List<TypeEquipement> getObjectsToWrite() {
        List<TypeItem> typeItemList = typeItemMapper.toDomains(repository.findByCategorie(CATEGORIE_EQUIPEMENT_PHYSIQUE));
        return typeEquipementMapper.toTypeEquipements(typeItemList);
    }

    @Override
    protected String getObjectId(TypeEquipement object) {
        return object.getType();
    }

    @Override
    public void printRecord(CSVPrinter csvPrinter, TypeEquipement objectToWrite) throws IOException {
        csvPrinter.printRecord(objectToWrite.getType(),
                objectToWrite.isServeur(),
                objectToWrite.getCommentaire(),
                formatDouble(objectToWrite.getDureeVieDefaut()),
                objectToWrite.getSource(), objectToWrite.getRefEquipementParDefaut());
    }

}
