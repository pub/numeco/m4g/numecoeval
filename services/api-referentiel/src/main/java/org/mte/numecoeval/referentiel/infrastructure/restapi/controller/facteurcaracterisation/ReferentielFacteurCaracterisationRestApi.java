package org.mte.numecoeval.referentiel.infrastructure.restapi.controller.facteurcaracterisation;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.ErrorResponseDTO;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.FacteurCaracterisationDTO;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

public interface ReferentielFacteurCaracterisationRestApi {

    @Operation(summary = "Endpoint interne à NumEcoEval - Récupération des Facteurs de Caractérisation",
            description = """
                    Endpoint interne utilisé dans la génération des indicateurs par le module api-calcul de NumEcoEval.
                    Récupération des Facteurs de Caractérisation en fonction de 5 paramètres:
                    <ul>
                        <li>Le nom du critère d'impact écologique: critere</li>
                        <li>Le code de l'étape ACV: etapeacv</li>
                        <li>Le nom du facteur de caractérisation recherché: nom</li>
                        <li>La localisation: localisation</li>
                        <li>La catégorie: categorie</li>
                    </ul>
                    .
                    """,
            tags = "Interne NumEcoEval",
            operationId = "getFacteurCaracterisation")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Liste des Facteurs de Caractérisation trouvés",
                    content = {@Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = FacteurCaracterisationDTO.class)))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ErrorResponseDTO.class))}),
            @ApiResponse(responseCode = "404", description = "Facteur Caractérisation non trouvé", content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ErrorResponseDTO.class))})})
    @GetMapping(path = "/referentiel/facteursCaracterisation", produces = MediaType.APPLICATION_JSON_VALUE)
    List<FacteurCaracterisationDTO> get(
            @RequestParam(value = "critere", required = false)
            @Schema(description = "Nom du critère d'impact écologique") final String critere,
            @RequestParam(value = "etapeacv", required = false)
            @Schema(description = "Code de l'étape ACV") final String etapeacv,
            @RequestParam(value = "nom", required = false)
            @Schema(description = "Nom du facteur de caractérisation recherché") final String nom,
            @RequestParam(value = "localisation", required = false)
            @Schema(description = "Nom de la localisation") final String localisation,
            @RequestParam(value = "categorie", required = false)
            @Schema(description = "Nom de la catégorie") final String categorie,
            @RequestParam(value = "nomOrganisation", defaultValue = "")
            @Schema(description = "Choix de l'organisation pour laquelle on veut exporter des hypothèses, si ce sont des références par défaut, communes à toutes les organisations, laisser le paramètre vide", defaultValue = "") String nomOrganisation
    );

}
