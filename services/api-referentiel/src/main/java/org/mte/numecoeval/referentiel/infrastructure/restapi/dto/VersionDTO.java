package org.mte.numecoeval.referentiel.infrastructure.restapi.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
@EqualsAndHashCode
@Builder
@Schema(
        description = "Version applicative"
)
public class VersionDTO implements Serializable {
    @Schema(
            description = "La version"
    )
    String version;

}
