package org.mte.numecoeval.referentiel.infrastructure.jpa.adapter;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.ListUtils;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.domain.model.CorrespondanceRefEquipement;
import org.mte.numecoeval.referentiel.domain.model.id.CorrespondanceRefEquipementId;
import org.mte.numecoeval.referentiel.domain.ports.output.ReferentielPersistencePort;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.CorrespondanceRefEquipementEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.repository.CorrespondanceRefEquipementRepository;
import org.mte.numecoeval.referentiel.infrastructure.mapper.CorrespondanceRefEquipementMapper;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@AllArgsConstructor
public class CorrespondanceRefEquipementJpaAdapter implements ReferentielPersistencePort<CorrespondanceRefEquipement, CorrespondanceRefEquipementId> {

    CorrespondanceRefEquipementMapper mapper;
    CorrespondanceRefEquipementRepository repository;

    @Override
    public CorrespondanceRefEquipement save(CorrespondanceRefEquipement referentiel) throws ReferentielException {
        var entityToSave = mapper.toEntity(referentiel);
        if (entityToSave != null) {
            var entitySaved = repository.save(entityToSave);
            return mapper.toDomain(entitySaved);
        }
        return null;
    }

    @Override
    public void saveAll(Collection<CorrespondanceRefEquipement> referentiels) throws ReferentielException {
        repository.saveAll(ListUtils.emptyIfNull(referentiels
                .stream()
                .map(typeEquipement -> mapper.toEntity(typeEquipement))
                .toList()));
    }

    @Override
    public CorrespondanceRefEquipement get(CorrespondanceRefEquipementId id) throws ReferentielException {
        Optional<CorrespondanceRefEquipementEntity> correspondanceRefEquipementOpt = repository.findById(mapper.toEntityId(id));
        CorrespondanceRefEquipementEntity correspondanceRefEquipementEntity = correspondanceRefEquipementOpt.orElseThrow(() -> new ReferentielException("Correspondance au RefEquipement non trouvé pour l'id " + id));
        return mapper.toDomain(correspondanceRefEquipementEntity);
    }

    @Override
    public void purge() {
        repository.deleteAll();
    }

    @Override
    public void purgeByOrganisation(String nomOrganisation) {
        repository.deleteByNomOrganisation(nomOrganisation);
    }

    @Override
    public List<CorrespondanceRefEquipement> getAll() {

        return ListUtils.emptyIfNull(repository.findAll())
                .stream()
                .map(typeEquipementEntity -> mapper.toDomain(typeEquipementEntity))
                .toList();
    }

}
