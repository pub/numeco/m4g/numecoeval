package org.mte.numecoeval.referentiel.infrastructure.restapi.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
@EqualsAndHashCode
@Builder
@Schema(
        description = "Référentiel des types d'équipements physiques utilisables dans le système. La clé du référentiel est le champ type."
)
public class TypeEquipementDTO implements Serializable {
    @Schema(
            description = "Type de l'équipment physique, clé du référentiel"
    )
    String type;
    @Schema(
            description = "Flag indiquant si l'équipement physique est un serveur"
    )
    boolean serveur;
    @Schema(
            description = "Commentaire de l'entrée dans le référentiel"
    )
    String commentaire;
    @Schema(
            description = "Durée de vie par défaut de ce type d'équipement physique"
    )
    Double dureeVieDefaut;
    @Schema(
            description = "Source de l'information du référentiel"
    )
    String source;
    @Schema(
            description = "Référence de l'équipement par défaut, permet des correspondances en cas d'absence de correspondance direct"
    )
    String refEquipementParDefaut;
}
