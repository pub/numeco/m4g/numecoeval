package org.mte.numecoeval.referentiel.infrastructure.restapi.controller.critere;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.CritereDTO;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.ErrorResponseDTO;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

public interface ReferentielCritereRestApi {

    @Operation(
            summary = "Endpoint interne à NumEcoEval - Récupération de tous les critères d'impacts écologiques",
            description = """
                    Renvoie l'intégralité des types des critères d'impacts écologiques utilisés dans NumEcoEval.                                                                                 
                    """,
            tags = "Interne NumEcoEval",
            operationId = "getAllCriteres"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Critères trouvés",
                    content = {@Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = CritereDTO.class)))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ErrorResponseDTO.class))}),
            @ApiResponse(responseCode = "404", description = "Critères non trouvés", content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ErrorResponseDTO.class))})})
    @GetMapping(path = "/referentiel/criteres", produces = MediaType.APPLICATION_JSON_VALUE)
    List<CritereDTO> getAll();
}
