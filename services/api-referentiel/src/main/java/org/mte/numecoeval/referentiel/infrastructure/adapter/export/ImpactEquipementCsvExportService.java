package org.mte.numecoeval.referentiel.infrastructure.adapter.export;

import org.apache.commons.csv.CSVPrinter;
import org.mte.numecoeval.referentiel.domain.model.ImpactEquipement;
import org.mte.numecoeval.referentiel.domain.ports.input.impl.ImportImpactEquipementPortImpl;
import org.mte.numecoeval.referentiel.domain.ports.output.ReferentielCsvExportService;
import org.mte.numecoeval.referentiel.infrastructure.jpa.repository.FacteurCaracterisationRepository;
import org.mte.numecoeval.referentiel.infrastructure.mapper.FacteurCaracterisationMapper;
import org.mte.numecoeval.referentiel.infrastructure.mapper.ImpactEquipementMapper;
import org.mte.numecoeval.referentiel.utils.Constants;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class ImpactEquipementCsvExportService extends ReferentielCsvExportService<ImpactEquipement> {

    private final FacteurCaracterisationRepository repository;
    private final FacteurCaracterisationMapper facteurCaracterisationMapper;
    private final ImpactEquipementMapper equipementMapper;

    public ImpactEquipementCsvExportService(FacteurCaracterisationRepository repository, FacteurCaracterisationMapper facteurCaracterisationMapper, ImpactEquipementMapper equipementMapper) {
        super(ImpactEquipement.class);
        this.repository = repository;
        this.facteurCaracterisationMapper = facteurCaracterisationMapper;
        this.equipementMapper = equipementMapper;
    }

    @Override
    public String[] getHeaders() {
        return ImportImpactEquipementPortImpl.getHeaders();
    }

    @Override
    public List<ImpactEquipement> getObjectsToWrite() {
        var facteurCaracterisationList = facteurCaracterisationMapper.toDomains(repository.findByNiveau(Constants.EQUIPEMENT_NIVEAU));
        return equipementMapper.toImpactEquipements(facteurCaracterisationList);
    }

    @Override
    protected String getObjectId(ImpactEquipement object) {
        return String.join("-", object.getRefEquipement(), object.getEtape(), object.getCritere());
    }

    @Override
    public void printRecord(CSVPrinter csvPrinter, ImpactEquipement objectToWrite) throws IOException {
        csvPrinter.printRecord(objectToWrite.getRefEquipement(),
                objectToWrite.getEtape(), objectToWrite.getCritere(),
                formatDouble(objectToWrite.getConsoElecMoyenne()), formatDouble(objectToWrite.getValeur()),
                objectToWrite.getSource(), objectToWrite.getType()
        );
    }

}
