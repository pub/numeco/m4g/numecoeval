package org.mte.numecoeval.referentiel.infrastructure.restapi.facade;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.domain.model.FacteurCaracterisation;
import org.mte.numecoeval.referentiel.domain.model.id.FacteurCaracterisationId;
import org.mte.numecoeval.referentiel.domain.ports.output.ReferentielPersistencePort;
import org.mte.numecoeval.referentiel.infrastructure.mapper.FacteurCaracterisationMapper;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.FacteurCaracterisationDTO;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.id.FacteurCaracterisationIdDTO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@AllArgsConstructor
public class FacteurCaracterisationFacade {

    private ReferentielPersistencePort<FacteurCaracterisation, FacteurCaracterisationId> persistencePort;


    private FacteurCaracterisationMapper mapper;


    public FacteurCaracterisationDTO get(FacteurCaracterisationIdDTO id) throws ReferentielException {
        FacteurCaracterisationId facteurCaracterisationId = mapper.toDomainId(id);
        FacteurCaracterisation facteurCaracterisation = persistencePort.get(facteurCaracterisationId);
        return mapper.toDTO(facteurCaracterisation);
    }

    public List<FacteurCaracterisationDTO> getByFilters(String... params) throws ReferentielException {
        List<FacteurCaracterisation> facteurCaracterisation = persistencePort.findByFilters(params[0], params[1], params[2], params[3], params[4], params[5]);
        return mapper.toDTOs(facteurCaracterisation);
    }

    public void purgeByOrganisationAndAddAll(List<FacteurCaracterisationDTO> fcsDTO, String nomOrganisation) throws ReferentielException {
        persistencePort.purgeByOrganisation(nomOrganisation);
        persistencePort.saveAll(mapper.toDomainsDTO(fcsDTO));
    }

    public void upsert(List<FacteurCaracterisationDTO> fcsDTO) throws ReferentielException {
        persistencePort.saveAll(mapper.toDomainsFromDTO(fcsDTO));
    }
}
