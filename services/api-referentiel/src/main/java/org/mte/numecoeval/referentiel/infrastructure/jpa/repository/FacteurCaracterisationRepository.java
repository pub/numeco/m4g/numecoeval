package org.mte.numecoeval.referentiel.infrastructure.jpa.repository;

import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.transaction.Transactional;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.FacteurCaracterisationEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.id.FacteurCaracterisationIdEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;
import java.util.Optional;

@RepositoryRestResource(path = "FacteurCaracterisation", itemResourceRel = "FacteursCaracterisation")
@Tag(name = "FacteurCaracterisation - CRUD/Spring Data REST")
public interface FacteurCaracterisationRepository extends JpaRepository<FacteurCaracterisationEntity, FacteurCaracterisationIdEntity> {

    @Transactional
    @Modifying
    void deleteByNiveauAndCategorie(String niveau, String categorie);

    @Transactional
    @Modifying
    void deleteByNiveau(String niveau);

    @Transactional
    @Modifying
    void deleteByNiveauAndTiers(String niveau, String tiers);

    @Query("""
            SELECT fc FROM #{#entityName} fc WHERE
            ((?1 IS NULL) OR (?1 IS NOT NULL AND fc.critere = ?1)) AND
            ((?2 IS NULL) OR (?2 IS NOT NULL AND fc.etape = ?2)) AND
            ((?3 IS NULL) OR (?3 IS NOT NULL AND fc.nom = ?3)) AND
            ((?4 IS NULL) OR (?4 IS NOT NULL AND fc.localisation = ?4)) AND
            ((?5 IS NULL) OR (?5 IS NOT NULL AND fc.categorie = ?5)) AND
            ((?6 IS NULL) OR (?6 IS NOT NULL AND fc.nomOrganisation = ?6))
            """)
    List<FacteurCaracterisationEntity> findByCritereAndEtapeAndNomAndLocalisationAndCategorieAndNomOrganisation(String nomcritere, String etape, String nom, String localisation, String categorie, String nomOrganisation);

    List<FacteurCaracterisationEntity> findByNiveauAndCategorieAndLocalisationAndCritere(String niveau, String categorie, String localisation, String critere);

    Optional<FacteurCaracterisationEntity> findByNomAndEtapeAndCritere(String nom, String etape, String critere);

    List<FacteurCaracterisationEntity> findByNiveauAndCategorie(String niveau, String categorie);

    List<FacteurCaracterisationEntity> findByNiveauAndTiers(String niveau, String tiers);

    List<FacteurCaracterisationEntity> findByNiveau(String niveau);

    @Transactional
    @Modifying
    int deleteByNomOrganisation(String nomOrganisation);
}
