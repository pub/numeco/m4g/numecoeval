package org.mte.numecoeval.referentiel.infrastructure.jpa.adapter;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.domain.model.FacteurCaracterisation;
import org.mte.numecoeval.referentiel.domain.model.ImpactEquipement;
import org.mte.numecoeval.referentiel.domain.model.id.ImpactEquipementId;
import org.mte.numecoeval.referentiel.domain.ports.output.ReferentielPersistencePort;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.FacteurCaracterisationEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.repository.FacteurCaracterisationRepository;
import org.mte.numecoeval.referentiel.infrastructure.mapper.FacteurCaracterisationMapper;
import org.mte.numecoeval.referentiel.infrastructure.mapper.ImpactEquipementMapper;
import org.mte.numecoeval.referentiel.utils.Constants;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;


@Service
@Slf4j
@AllArgsConstructor
public class ImpactEquipementJpaAdapter implements ReferentielPersistencePort<ImpactEquipement, ImpactEquipementId> {

    private FacteurCaracterisationRepository facteurCaracterisationRepository;
    private ImpactEquipementMapper mapper;
    private FacteurCaracterisationMapper facteurCaracterisationMapper;


    @Override
    public ImpactEquipement save(ImpactEquipement referentiel) throws ReferentielException {
        var facteurCaracterisation = mapper.toFacteurCaracterisation(referentiel);
        var entityToSave = facteurCaracterisationMapper.toEntity(facteurCaracterisation);
        if (entityToSave != null) {
            facteurCaracterisationRepository.save(entityToSave);
            return referentiel;
        }
        return null;
    }

    @Override
    public void saveAll(Collection<ImpactEquipement> referentiels) throws ReferentielException {
        purge();
        List<FacteurCaracterisation> facteurCaracterisationList = mapper.toFacteurCaracterisations(referentiels.stream().toList());
        facteurCaracterisationRepository.saveAll(facteurCaracterisationMapper.toEntities(facteurCaracterisationList));
    }

    @Override
    public ImpactEquipement get(ImpactEquipementId id) throws ReferentielException {
        if (id != null) {
            Optional<FacteurCaracterisationEntity> facteurCaracterisationEntities = facteurCaracterisationRepository.findByNomAndEtapeAndCritere(id.getRefEquipement(), id.getEtape(), id.getCritere());
            if (facteurCaracterisationEntities.isPresent()) {
                return mapper.toImpactEquipement(facteurCaracterisationMapper.toDomain(facteurCaracterisationEntities.get()));
            } else {
                throw new ReferentielException("Impact équipement non trouvé pour l'id " + id);
            }
        }
        throw new ReferentielException("Impact équipement non trouvé pour l'id null");
    }

    @Override
    public void purge() {

        facteurCaracterisationRepository.deleteByNiveau(Constants.EQUIPEMENT_NIVEAU);
    }

    @Override
    public List<ImpactEquipement> getAll() {
        return mapper.toImpactEquipements(facteurCaracterisationMapper.toDomains(facteurCaracterisationRepository.findByNiveau(Constants.EQUIPEMENT_NIVEAU)));
    }
}
