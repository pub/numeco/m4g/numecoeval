package org.mte.numecoeval.referentiel.domain.model.id;

import lombok.*;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
public class FacteurCaracterisationId {
    String nom;
    String etape;
    String critere;
    String nomOrganisation;

}
