package org.mte.numecoeval.referentiel.infrastructure.jpa.repository;

import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.transaction.Transactional;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.TypeItemEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.id.TypeItemIdEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;
import java.util.Optional;

@RepositoryRestResource(path = "TypeItem", itemResourceRel = "TypesEquipements")
@Tag(name = "TypeItem - CRUD/Spring Data REST")
public interface TypeItemRepository extends JpaRepository<TypeItemEntity, TypeItemIdEntity> {
    Optional<TypeItemEntity> findById(TypeItemIdEntity id);

    List<TypeItemEntity> findByCategorie(String categorie);

    @Transactional
    @Modifying
    int deleteByNomOrganisation(String nomOrganisation);

}
