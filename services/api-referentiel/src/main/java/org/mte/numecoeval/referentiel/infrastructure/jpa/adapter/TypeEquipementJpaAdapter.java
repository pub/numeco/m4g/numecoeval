package org.mte.numecoeval.referentiel.infrastructure.jpa.adapter;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.domain.model.TypeEquipement;
import org.mte.numecoeval.referentiel.domain.model.TypeItem;
import org.mte.numecoeval.referentiel.domain.ports.output.ReferentielPersistencePort;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.TypeItemEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.id.TypeItemIdEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.repository.TypeItemRepository;
import org.mte.numecoeval.referentiel.infrastructure.mapper.TypeEquipementMapper;
import org.mte.numecoeval.referentiel.infrastructure.mapper.TypeItemMapper;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.mte.numecoeval.referentiel.utils.Constants.CATEGORIE_EQUIPEMENT_PHYSIQUE;

@Service
@Slf4j
@AllArgsConstructor
public class TypeEquipementJpaAdapter implements ReferentielPersistencePort<TypeEquipement, String> {

    TypeItemRepository typeItemRepository;
    TypeItemMapper typeItemMapper;
    TypeEquipementMapper typeEquipementMapper;

    @Override
    public TypeEquipement save(TypeEquipement referentiel) throws ReferentielException {
        var entityToSave = typeItemMapper.toEntity(typeEquipementMapper.toTypeItem(referentiel));
        if (entityToSave != null) {
            typeItemRepository.save(entityToSave);
            return referentiel;
        }
        return null;
    }

    @Override
    public void saveAll(Collection<TypeEquipement> referentiels) throws ReferentielException {
        List<TypeItem> typeItemList = typeEquipementMapper.toTypesItem(referentiels.stream().toList());
        typeItemRepository.saveAll(typeItemMapper.toEntities(typeItemList));
    }


    @Override
    public TypeEquipement get(String id) throws ReferentielException {
        if (id == null) throw new ReferentielException("Type Equipement non trouvé pour l'id null");
        TypeItemIdEntity typeItemIdEntity = new TypeItemIdEntity().setType(id);
        Optional<TypeItemEntity> typeItemEntities = typeItemRepository.findById(typeItemIdEntity);
        if (typeItemEntities.isPresent()) {
            return typeEquipementMapper.toTypeEquipement(typeItemMapper.toDomain(typeItemEntities.get()));
        } else {
            throw new ReferentielException("Type Equipement non trouvé pour l'id " + id);
        }
    }

    @Override
    public void purge() {
        typeItemRepository.deleteAll();
    }

    @Override
    public List<TypeEquipement> getAll() {
        List<TypeItemEntity> typeItems = typeItemRepository.findByCategorie(CATEGORIE_EQUIPEMENT_PHYSIQUE);
        return typeEquipementMapper.toTypeEquipements(typeItemMapper.toDomains(typeItems));
    }
}
