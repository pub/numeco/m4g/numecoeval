package org.mte.numecoeval.referentiel.infrastructure.jpa.entity.id;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@Getter
@Setter
@Accessors(chain = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
public class FacteurCaracterisationIdEntity implements AbstractReferentieIdEntity {
    String nom;
    String etape;
    String critere;
    String nomOrganisation;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        FacteurCaracterisationIdEntity that = (FacteurCaracterisationIdEntity) o;

        return new EqualsBuilder().append(nom, that.nom).append(etape, that.etape).append(critere, that.critere).append(nomOrganisation, that.nomOrganisation).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(nom).append(etape).append(critere).append(nomOrganisation).toHashCode();
    }
}
