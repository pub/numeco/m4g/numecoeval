package org.mte.numecoeval.referentiel.infrastructure.adapter.export;

import org.apache.commons.csv.CSVPrinter;
import org.mte.numecoeval.referentiel.domain.ports.input.impl.ImportEtapePortImpl;
import org.mte.numecoeval.referentiel.domain.ports.output.ReferentielCsvExportService;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.EtapeEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.repository.EtapeRepository;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class EtapeCsvExportService extends ReferentielCsvExportService<EtapeEntity> {
    private final EtapeRepository repository;

    public EtapeCsvExportService(EtapeRepository repository) {
        super(EtapeEntity.class);
        this.repository = repository;
    }

    @Override
    public String[] getHeaders() {
        return ImportEtapePortImpl.getHeaders();
    }

    @Override
    public List<EtapeEntity> getObjectsToWrite() {
        return repository.findAll();
    }

    @Override
    protected String getObjectId(EtapeEntity object) {
        return object.getCode();
    }

    @Override
    public void printRecord(CSVPrinter csvPrinter, EtapeEntity objectToWrite) throws IOException {
        csvPrinter.printRecord(objectToWrite.getCode(),objectToWrite.getLibelle());
    }
}
