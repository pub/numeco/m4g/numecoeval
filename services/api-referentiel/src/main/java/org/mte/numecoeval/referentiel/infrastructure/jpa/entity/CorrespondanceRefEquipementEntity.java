package org.mte.numecoeval.referentiel.infrastructure.jpa.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import jakarta.persistence.Table;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.id.CorrespondanceRefEquipementIdEntity;

@Getter
@Setter
@Accessors(chain = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@IdClass(CorrespondanceRefEquipementIdEntity.class)
@Entity
@Table(name = "REF_CORRESPONDANCE_REF_EQP")
public class CorrespondanceRefEquipementEntity implements AbstractReferentielEntity {
    @Id
    String modeleEquipementSource;
    @Id
    // Nom de l'organisation dans le cas où cette dernière à des références customisées, propres à son organisation
    String nomOrganisation;
    String refEquipementCible;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        CorrespondanceRefEquipementEntity that = (CorrespondanceRefEquipementEntity) o;

        return new EqualsBuilder()
                .append(modeleEquipementSource, that.modeleEquipementSource)
                .append(refEquipementCible, that.refEquipementCible)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(modeleEquipementSource)
                .append(refEquipementCible)
                .toHashCode();
    }
}
