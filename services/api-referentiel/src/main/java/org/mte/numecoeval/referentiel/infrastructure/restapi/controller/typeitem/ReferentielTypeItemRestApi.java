package org.mte.numecoeval.referentiel.infrastructure.restapi.controller.typeitem;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.ErrorResponseDTO;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.TypeItemDTO;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

public interface ReferentielTypeItemRestApi {

    @Operation(
            summary = "Endpoint interne à NumEcoEval - Récupération de tous les types d'items",
            description = """
                    Endpoint interne utilisé à la réception de données d'entrées par le module api-expositiondonneesentrees de NumEcoEval.
                    Renvoie l'intégralité des types d'items utilisables par NumEcoEval.
                                        
                    Les types d'items servent notamment à alimenter la durée de vie par défaut des items
                    reçues.
                    """,
            tags = "Interne NumEcoEval",
            operationId = "getAllTypeItem"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Types Item",
                    content = {@Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = TypeItemDTO.class)))}),
            @ApiResponse(responseCode = "400", description = "Invalid request",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorResponseDTO.class))}),
            @ApiResponse(responseCode = "404", description = "Types Item non trouvé", content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ErrorResponseDTO.class))})})
    @GetMapping(path = "/referentiel/typesItem", produces = MediaType.APPLICATION_JSON_VALUE)
    List<TypeItemDTO> getTypesItem();

    @Operation(
            summary = "Endpoint interne à NumEcoEval - Récupération d'items via leur type",
            description = """
                    Endpoint interne utilisé à la réception de données d'entrées par le module api-expositiondonneesentrees de NumEcoEval. Renvoie l'intégralité des types d'items d'un certain type.                                                                                 
                    """,
            tags = "Interne NumEcoEval",
            operationId = "getTypeItem"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Types Item",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = TypeItemDTO.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorResponseDTO.class))}),
            @ApiResponse(responseCode = "404", description = "Types Item non trouvé", content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ErrorResponseDTO.class))})})
    @GetMapping(path = "/referentiel/typesItem/{type}", produces = MediaType.APPLICATION_JSON_VALUE)
    TypeItemDTO getTypeItem(
            @PathVariable("type") @Schema(description = "type recherché") String type,
            @RequestParam(value = "nomOrganisation", defaultValue = "")
            @Schema(description = "Choix de l'organisation pour laquelle on veut exporter des hypothèses, si ce sont des références par défaut, communes à toutes les organisations, laisser le paramètre vide", defaultValue = "") String nomOrganisation
    ) throws ReferentielException;
}
