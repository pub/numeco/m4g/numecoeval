package org.mte.numecoeval.referentiel.infrastructure.restapi.controller.typeequipement;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.ErrorResponseDTO;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.TypeEquipementDTO;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

public interface ReferentielTypeEquipementRestApi {

    @Operation(
            summary = "Endpoint interne à NumEcoEval - Récupération de tous les types d'équipement",
            description = """
                    Endpoint interne utilisé à la réception de données d'entrées par le module api-expositiondonneesentrees de NumEcoEval.
                    Renvoie l'intégralité des types d'équipements utilisables par NumEcoEval.
                                        
                    Les types d'équipement servent notamment à alimenter la durée de vie par défaut des équipements
                    reçues.
                    """,
            tags = "Interne NumEcoEval",
            operationId = "getAllTypeEquipement"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Types Equipement",
                    content = {@Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = TypeEquipementDTO.class)))}),
            @ApiResponse(responseCode = "400", description = "Invalid request",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorResponseDTO.class))}),
            @ApiResponse(responseCode = "404", description = "Types Equipement  non trouvé", content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ErrorResponseDTO.class))})})
    @GetMapping(path = "/referentiel/typesEquipement", produces = MediaType.APPLICATION_JSON_VALUE)
    List<TypeEquipementDTO> getTypesEquipement();

    @Operation(
            summary = "Endpoint interne à NumEcoEval - Récupération d'un type d'équipement via son type",
            description = """
                    Endpoint interne utilisé à la réception de données d'entrées par le module api-expositiondonneesentrees de NumEcoEval. Renvoie l'intégralité des types d'équipements d'un certain type.                                                                                 
                    """,
            tags = "Interne NumEcoEval",
            operationId = "getTypeEquipement"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Types Equipement",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = TypeEquipementDTO.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorResponseDTO.class))}),
            @ApiResponse(responseCode = "404", description = "Types Equipement non trouvé", content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ErrorResponseDTO.class))})})
    @GetMapping(path = "/referentiel/typesEquipement/{type}", produces = MediaType.APPLICATION_JSON_VALUE)
    TypeEquipementDTO getTypeEquipement(@PathVariable("type") @Schema(description = "type recherché") String type) throws ReferentielException;
}
