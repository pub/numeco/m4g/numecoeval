package org.mte.numecoeval.referentiel.domain.model;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
public class ImpactEquipement implements AbstractReferentiel {

    String refEquipement;
    String etape;
    String critere;
    String source;
    String type;
    Double valeur;
    Double consoElecMoyenne;
    String description;


}
