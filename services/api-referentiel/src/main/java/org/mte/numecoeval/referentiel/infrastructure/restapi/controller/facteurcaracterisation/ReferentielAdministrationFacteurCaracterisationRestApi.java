package org.mte.numecoeval.referentiel.infrastructure.restapi.controller.facteurcaracterisation;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.servlet.http.HttpServletResponse;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.ErrorResponseDTO;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.RapportImportDTO;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface ReferentielAdministrationFacteurCaracterisationRestApi {

    @Operation(
            summary = "Alimentation du référentiel FacteurCaracterisation par csv : annule et remplace.",
            description = """
                    Le référentiel est global à tout le système. L’import se fait uniquement avec un fichier CSV.
                    Lors de l’import, on peut choisir le mode pour supprimer les données déjà présentes dans la base ou pas. Par défaut, les anciennes données sont conservées.
                    <ul>
                    <li>Entrée : Le mode d'import 'FULL' ou 'DELTA' et le fichier CSV avec les données à insérer</li>
                    <li>Sortie : Rapport du fichier CSV (nombre de lignes totales, nombre de lignes en erreur, nombre de lignes traitées, liste des erreurs par lignes).</li>
                    </ul>
                    """,
            tags = {"Import Référentiels"},
            operationId = "importFacteurCaracterisationCSV"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Rapport d'import du fichier CSV"),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ErrorResponseDTO.class))})})
    @PostMapping(path = "/referentiel/facteursCaracterisation/csv", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    RapportImportDTO importCSV(
            @RequestPart("file") MultipartFile file,
            @RequestParam(value = "mode", defaultValue = "DELTA")
            @Schema(description = "Choix du mode d'insertion FULL (purge puis insertion des nouvelles données) ou " +
                    "DELTA (upsert : maj de la base de données existante)", defaultValue = "DELTA") String mode,
            @RequestParam(value = "nomOrganisation", defaultValue = "")
            @Schema(description = "Choix de l'organisation pour laquelle on veut importer des hypothèses, si ce sont des références par défaut, communes à toutes les organisations, laisser le paramètre vide", defaultValue = "") String nomOrganisation
    ) throws IOException, ReferentielException;


    @Operation(
            summary = "Exporter les facteurs de caractérisation sous format csv",
            description = """
                    <ul>
                    <li>Entrée : Aucune </li>
                    <li>Sortie : Renvoie la liste des étapes de ref_FacteurCaracterisation </li>
                    </ul>
                    """,
            tags = {"Export Référentiels"},
            operationId = "exportFacteurCaracterisationCSV"
    )
    @GetMapping("/referentiel/facteursCaracterisation/csv")
    void exportCSV(HttpServletResponse servletResponse) throws IOException;
}
