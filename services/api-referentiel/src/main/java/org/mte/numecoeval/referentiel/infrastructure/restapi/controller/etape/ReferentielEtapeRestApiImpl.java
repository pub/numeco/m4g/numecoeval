package org.mte.numecoeval.referentiel.infrastructure.restapi.controller.etape;

import jakarta.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.EtapeDTO;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.domain.ports.input.impl.ImportEtapePortImpl;
import org.mte.numecoeval.referentiel.infrastructure.adapter.export.EtapeCsvExportService;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.EtapeEntity;
import org.mte.numecoeval.referentiel.infrastructure.restapi.controller.BaseExportReferentiel;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.RapportImportDTO;
import org.mte.numecoeval.referentiel.infrastructure.restapi.facade.EtapeFacade;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.util.List;

@RestController
@Slf4j
@AllArgsConstructor
public class ReferentielEtapeRestApiImpl implements BaseExportReferentiel<EtapeEntity>,ReferentielEtapeRestApi, ReferentielAdministrationEtapeRestApi {
    private EtapeFacade referentielFacade;
    private EtapeCsvExportService csvExportService;



    @Override
    public List<EtapeDTO> getAll() {
        return referentielFacade.getAll();
    }

    @Override
    public RapportImportDTO importCSV(MultipartFile fichier) throws IOException, ReferentielException {
        if (fichier == null || fichier.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Le fichier n'existe pas ou alors il est vide");
        }
        var rapportImport = new ImportEtapePortImpl().importCSV(fichier.getInputStream());

        referentielFacade.purgeAndAddAll(rapportImport.getObjects());

        return new RapportImportDTO(
                fichier.getOriginalFilename(),
                rapportImport.getErreurs(),
                rapportImport.getNbrLignesImportees()
        );
    }

    @Override
    public void exportCSV(HttpServletResponse servletResponse) throws IOException {
        exportCSV(servletResponse, csvExportService, "etapes");
    }
}
