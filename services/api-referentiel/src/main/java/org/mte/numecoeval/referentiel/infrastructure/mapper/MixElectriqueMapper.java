package org.mte.numecoeval.referentiel.infrastructure.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mte.numecoeval.referentiel.domain.model.FacteurCaracterisation;
import org.mte.numecoeval.referentiel.domain.model.MixElectrique;
import org.mte.numecoeval.referentiel.domain.model.id.MixElectriqueId;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.MixElectriqueDTO;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.id.MixElectriqueIdDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MixElectriqueMapper {

    MixElectriqueId toDomainId(MixElectriqueIdDTO id);

    List<MixElectrique> toDomainsFromDTO(List<MixElectriqueDTO> mixElecs);

    MixElectriqueDTO toDTO(MixElectrique mixElectrique);

    @Mapping(target = "localisation", source = "pays")
    @Mapping(target = "nom", expression = "java(org.mte.numecoeval.referentiel.utils.Constants.MIXELEC_NOM_LOW_VOLTAGE + mixElectrique.getRaccourcisAnglais())")
    @Mapping(target = "etape", expression = "java(org.mte.numecoeval.referentiel.utils.Constants.MIXELEC_ETAPEACV)")
    @Mapping(target = "niveau", expression = "java(org.mte.numecoeval.referentiel.utils.Constants.MIXELEC_NIVEAU)")
    @Mapping(target = "categorie", expression = "java(org.mte.numecoeval.referentiel.utils.Constants.MIXELEC_CATEGORIE)")
    @Mapping(target = "nomOrganisation", constant = "")
    FacteurCaracterisation toFacteurCaracterisation(MixElectrique mixElectrique);

    List<FacteurCaracterisation> toFacteurCaracterisations(List<MixElectrique> mixElectriques);

    @Mapping(target = "pays", source = "localisation")
    MixElectrique toMixElectrique(FacteurCaracterisation facteurCaracterisation);

    List<MixElectrique> toMixElectriques(List<FacteurCaracterisation> facteurCaracterisations);
}