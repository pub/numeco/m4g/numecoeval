package org.mte.numecoeval.referentiel.domain.model;

import java.io.Serializable;

public interface AbstractReferentiel extends Serializable {
    // Actuellement l'interface n'a pas de comportement par défaut ni de champ partagé
}
