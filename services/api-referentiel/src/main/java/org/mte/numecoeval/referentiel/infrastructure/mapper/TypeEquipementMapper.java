package org.mte.numecoeval.referentiel.infrastructure.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mte.numecoeval.referentiel.domain.model.TypeEquipement;
import org.mte.numecoeval.referentiel.domain.model.TypeItem;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.TypeEquipementDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TypeEquipementMapper {

    TypeEquipement toDomaine(TypeEquipementDTO typeEquipementDTO);

    TypeEquipementDTO toDto(TypeEquipement typeEquipement);

    @Mapping(target = "refItemParDefaut", source = "refEquipementParDefaut")
    @Mapping(target = "categorie", expression = "java(org.mte.numecoeval.referentiel.utils.Constants.CATEGORIE_EQUIPEMENT_PHYSIQUE)")
    @Mapping(target = "nomOrganisation", constant = "")
    TypeItem toTypeItem(TypeEquipement typeEquipement);

    List<TypeItem> toTypesItem(List<TypeEquipement> typesEquipement);

    @Mapping(target = "refEquipementParDefaut", source = "refItemParDefaut")
    TypeEquipement toTypeEquipement(TypeItem typeItem);

    List<TypeEquipement> toTypeEquipements(List<TypeItem> typeItems);


}
