package org.mte.numecoeval.referentiel.infrastructure.restapi.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
@AllArgsConstructor
public class RapportImportDTO {

    String fichier;

    List<String> erreurs = new ArrayList<>();

    long nbrLignesImportees = 0;
}
