package org.mte.numecoeval.referentiel.infrastructure.restapi.controller.typeequipement;

import jakarta.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.domain.model.TypeEquipement;
import org.mte.numecoeval.referentiel.domain.ports.input.impl.ImportTypeEquipementPortImpl;
import org.mte.numecoeval.referentiel.infrastructure.adapter.export.TypeEquipementCsvExportService;
import org.mte.numecoeval.referentiel.infrastructure.restapi.controller.BaseExportReferentiel;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.RapportImportDTO;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.TypeEquipementDTO;
import org.mte.numecoeval.referentiel.infrastructure.restapi.facade.TypeEquipementFacade;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.util.List;

@RestController
@Slf4j
@AllArgsConstructor
public class ReferentielTypeEquipementRestApiImpl implements BaseExportReferentiel<TypeEquipement>, ReferentielTypeEquipementRestApi, ReferentielAdministrationTypeEquipementRestApi {

    private TypeEquipementFacade typeEquipementFacade;

    private TypeEquipementCsvExportService csvExportService;

    @Override
    public RapportImportDTO importCSV(MultipartFile fichier) throws IOException, ReferentielException {
        if (fichier == null || fichier.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Le fichier n'existe pas ou alors il est vide");
        }
        var rapportImport = new ImportTypeEquipementPortImpl().importCSV(fichier.getInputStream());
        typeEquipementFacade.purgeAndAddAll(rapportImport.getObjects());
        return new RapportImportDTO(
                fichier.getOriginalFilename(),
                rapportImport.getErreurs(),
                rapportImport.getNbrLignesImportees()
        );
    }

    @Override
    public TypeEquipementDTO getTypeEquipement(String type) throws ReferentielException {
        return typeEquipementFacade.getTypeEquipementForType(type);
    }

    @Override
    public List<TypeEquipementDTO> getTypesEquipement() {
        return typeEquipementFacade.getAllTypesEquipement();
    }

    @Override
    public void exportCSV(HttpServletResponse servletResponse) throws IOException {
        exportCSV(servletResponse, csvExportService, "typeEquipement");
    }

}
