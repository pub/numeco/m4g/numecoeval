package org.mte.numecoeval.referentiel.infrastructure.restapi.controller.impactmessagerie;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.servlet.http.HttpServletResponse;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.ErrorResponseDTO;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.RapportImportDTO;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface ReferentielInterneImpactMessagerieRestApi {

    @Operation(
            summary = "Alimentation du référentiel Impact Messagerie par csv : annule et remplace.",
            description = """
                    Le référentiel est global à tout le système. L’import se fait uniquement avec un fichier CSV.
                    Lors de l’import, les données précédentes sont supprimées.
                    <ul>
                    <li>Entrée : Le fichier CSV du référentiel</li>
                    <li>Sortie : Rapport du fichier CSV (nombre de lignes totales, nombre de lignes en erreur, nombre de lignes traitées, liste des erreurs par lignes).</li>
                    </ul>
                    """,
            tags = {"Import Référentiels"},
            operationId = "importImpactMessagerieCSV"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Rapport d'import du fichier CSV"),
            @ApiResponse(responseCode = "400", description = "Invalid request",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorResponseDTO.class))})})
    @PostMapping(path = "/referentiel/impactMessagerie/csv", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    RapportImportDTO importCSV(@RequestPart("file") MultipartFile file) throws IOException, ReferentielException;


    @Operation(
            summary = "Exporter les impacts de messagerie sous format csv",
            description = """
                    <ul>
                    <li>Entrée : Aucune </li>
                    <li>Sortie : Renvoie la liste des étapes de ref_ImpactMessagerie </li>
                    </ul>
                    """,
            tags = {"Export Référentiels"},
            operationId = "exportImpactMessagerieCSV"
    )
    @GetMapping("/referentiel/impactMessagerie/csv")
    void exportCSV(HttpServletResponse servletResponse) throws IOException;
}
