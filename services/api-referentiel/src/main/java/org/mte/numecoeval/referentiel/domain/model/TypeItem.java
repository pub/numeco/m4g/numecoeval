package org.mte.numecoeval.referentiel.domain.model;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Builder
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TypeItem implements AbstractReferentiel {
    String type;
    String categorie;
    boolean serveur;
    String commentaire;
    Double dureeVieDefaut;
    String refHypothese;
    String source;
    // Référence de l'item par défaut, permet des correspondances en cas d'absence de correspondance directe.
    String refItemParDefaut;
    // Nom de l'organisation dans le cas où cette dernière à des références customisées, propres à son organisation
    String nomOrganisation;

}
