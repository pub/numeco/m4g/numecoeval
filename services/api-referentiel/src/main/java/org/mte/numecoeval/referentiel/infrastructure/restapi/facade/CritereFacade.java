package org.mte.numecoeval.referentiel.infrastructure.restapi.facade;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.CritereDTO;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.domain.model.Critere;
import org.mte.numecoeval.referentiel.domain.model.id.CritereId;
import org.mte.numecoeval.referentiel.domain.ports.output.ReferentielPersistencePort;
import org.mte.numecoeval.referentiel.infrastructure.mapper.CritereMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@AllArgsConstructor
public class CritereFacade {

    private ReferentielPersistencePort<Critere, CritereId> persistencePort;

    private CritereMapper mapper;

    /**
     * creation liste etapes
     *
     * @param criteresDTO
     */
    public void purgeAndAddAll(List<CritereDTO> criteresDTO) throws ReferentielException {
        persistencePort.purge();
        persistencePort.saveAll(mapper.toDomainsFromDTO(criteresDTO));
    }

    /**
     * Recuperation de la liste des hypotheses
     *
     * @return
     */
    public List<CritereDTO> getAll() {
        return mapper.toDTO(persistencePort.getAll());
    }
}
