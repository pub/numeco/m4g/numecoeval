package org.mte.numecoeval.referentiel.domain.ports.input.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.mte.numecoeval.referentiel.domain.data.ResultatImport;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.domain.ports.input.ImportCSVReferentielPort;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.CorrespondanceRefEquipementDTO;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Slf4j
public class ImportCorrespondanceRefEquipementPortImpl implements ImportCSVReferentielPort<CorrespondanceRefEquipementDTO> {
    public static final String MODELE_EQUIPEMENT_SOURCE = "modeleEquipementSource";
    public static final String REF_EQUIPEMENT_CIBLE = "refEquipementCible";
    public static final String REF_NOM_ORGANISATION = "nomOrganisation";
    private static final String[] HEADERS = new String[]{MODELE_EQUIPEMENT_SOURCE, REF_EQUIPEMENT_CIBLE, REF_NOM_ORGANISATION};
    private static final String[] MANDATORY_HEADERS = new String[]{MODELE_EQUIPEMENT_SOURCE, REF_EQUIPEMENT_CIBLE};

    public static String[] getHeaders() {
        return HEADERS;
    }

    public void checkCSVRecord(CSVRecord csvRecord, String nomOrganisation) throws ReferentielException {
        checkAllHeadersAreMapped(csvRecord, MANDATORY_HEADERS);
        checkFieldIsMappedAndNotBlankInCSVRecord(csvRecord, MODELE_EQUIPEMENT_SOURCE);
        checkFieldIsMappedAndNotBlankInCSVRecord(csvRecord, REF_EQUIPEMENT_CIBLE);
        checkNomOrganisation(csvRecord, nomOrganisation);
    }

    @Override
    public ResultatImport<CorrespondanceRefEquipementDTO> importCSV(InputStream csvInputStream, String nomOrganisation) {
        ResultatImport<CorrespondanceRefEquipementDTO> resultatImport = new ResultatImport<>();
        List<CorrespondanceRefEquipementDTO> dtos = new ArrayList<>();

        try (Reader reader = new InputStreamReader(csvInputStream)) {
            Iterable<CSVRecord> records = CSVFormat.DEFAULT.builder()
                    .setHeader()
                    .setDelimiter(CSV_SEPARATOR)
                    .setTrim(true)
                    .setAllowMissingColumnNames(true)
                    .setSkipHeaderRecord(true)
                    .build().parse(reader);
            records.forEach(csvRecord -> {
                try {
                    checkCSVRecord(csvRecord, nomOrganisation);
                    dtos.add(CorrespondanceRefEquipementDTO.builder()
                            .modeleEquipementSource(csvRecord.get(MODELE_EQUIPEMENT_SOURCE).trim())
                            .refEquipementCible(csvRecord.get(REF_EQUIPEMENT_CIBLE).trim())
                            .nomOrganisation(getStringValueFromRecordDefaultEmpty(csvRecord, REF_NOM_ORGANISATION))
                            .build());
                } catch (Exception e) {
                    log.error("Erreur prévue lors de la lecture de la ligne {} : {}", csvRecord.getRecordNumber() + 1, e.getMessage());
                    resultatImport.getErreurs().add(e.getMessage());
                }
            });

        } catch (Exception e) {
            log.error("Erreur de traitement du fichier", e);

            resultatImport.setErreurs(Collections.singletonList("Le fichier CSV n'a pas pu être lu."));
            resultatImport.setNbrLignesImportees(0);
            resultatImport.setObjects(null);
            return resultatImport;
        }

        resultatImport.setObjects(dtos);
        resultatImport.setNbrLignesImportees(dtos.size());

        return resultatImport;
    }
}
