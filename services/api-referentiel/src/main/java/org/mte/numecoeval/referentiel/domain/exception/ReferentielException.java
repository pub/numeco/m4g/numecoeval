package org.mte.numecoeval.referentiel.domain.exception;

public class ReferentielException extends Exception {

    public ReferentielException(String message) {
        super(message);
    }
}
