package org.mte.numecoeval.referentiel.infrastructure.restapi.dto.id;

import lombok.*;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@Getter
@Setter
@EqualsAndHashCode
@Accessors(chain = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
public class TypeItemIdDTO implements Serializable {
    String type;
    String nomOrganisation;
}
