package org.mte.numecoeval.referentiel.infrastructure.restapi.controller.impactmessagerie;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.ErrorResponseDTO;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.ImpactMessagerieDTO;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

public interface ReferentielImpactMessagerieRestApi {


    @Operation(summary = "Endpoint interne à NumEcoEval - Récupération d'un impact de messagerie",
            description = """
                    Endpoint interne utilisé dans l'enrichissement des données pour un calcul dans le module api-enrichissement de NumEcoEval.
                    Renvoie un élément du référentiel des impacts de messagerie.
                    """,
            tags = "Interne NumEcoEval",
            operationId = "getImpactMessagerie"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = " Référentiel Impact Messagerie",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ImpactMessagerieDTO.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorResponseDTO.class))}),
            @ApiResponse(responseCode = "404", description = " Référentiel Impact Messagerie  non trouvé", content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ErrorResponseDTO.class))})})
    @GetMapping(path = "/referentiel/impactsMessagerie/{critere}", produces = MediaType.APPLICATION_JSON_VALUE)
    ImpactMessagerieDTO getImpactMessagerie(@PathVariable
                                                  @Schema(description = "Critère recherché") String critere);


    @Operation(summary = "Endpoint interne à NumEcoEval - Récupération des impacts de messagerie",
            description = """
                    Endpoint interne utilisé dans la génération des indicateurs par le module api-calcul de NumEcoEval.
                    Renvoie l'intégralité des impacts de messagerie.
                    """,
            tags = "Interne NumEcoEval",
            operationId = "getAllImpactMessagerie"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = " Référentiel Impact Messagerie",
                    content = {@Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = ImpactMessagerieDTO.class)))}),
            @ApiResponse(responseCode = "400", description = "Invalid request",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorResponseDTO.class))}),
            @ApiResponse(responseCode = "404", description = " Référentiel Impact Messagerie  non trouvé", content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ErrorResponseDTO.class))})})
    @GetMapping(path = "/referentiel/impactsMessagerie", produces = MediaType.APPLICATION_JSON_VALUE)
    List<ImpactMessagerieDTO> getAllImpactMessagerie();
}
