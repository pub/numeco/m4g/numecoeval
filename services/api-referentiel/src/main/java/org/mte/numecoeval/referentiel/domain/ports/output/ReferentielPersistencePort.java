package org.mte.numecoeval.referentiel.domain.ports.output;

import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.domain.model.AbstractReferentiel;

import java.util.Collection;
import java.util.List;


public interface ReferentielPersistencePort<T extends AbstractReferentiel, P extends Object> {

    /**
     * Sauvegarde et mise à jour
     *
     * @param referentiel
     * @return
     */
    T save(T referentiel) throws ReferentielException;

    /**
     * Ajout en masse d'une liste
     *
     * @param referentiel
     * @throws ReferentielException
     */
    void saveAll(Collection<T> referentiel) throws ReferentielException;

    /**
     * Recherche par id
     *
     * @param id
     * @return
     */
    T get(P id) throws ReferentielException;

    /**
     * purge entity
     */
    void purge();

    default void purgeByOrganisation(String nomOrganisation) {
    }

    /**
     * liste des elements d'une table
     *
     * @return
     */
    List<T> getAll();

    /**
     * Find by filters
     *
     * @param params param list
     * @return the list of objects
     */
    default List<T> findByFilters(String... params) throws ReferentielException {
        return List.of();
    }

}
