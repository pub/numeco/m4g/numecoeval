package org.mte.numecoeval.referentiel.infrastructure.jpa.adapter;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.ListUtils;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.domain.model.FacteurCaracterisation;
import org.mte.numecoeval.referentiel.domain.model.id.FacteurCaracterisationId;
import org.mte.numecoeval.referentiel.domain.ports.output.ReferentielPersistencePort;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.FacteurCaracterisationEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.id.FacteurCaracterisationIdEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.repository.FacteurCaracterisationRepository;
import org.mte.numecoeval.referentiel.infrastructure.mapper.FacteurCaracterisationMapper;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@AllArgsConstructor
public class FacteurCaracterisationJpaAdapter implements ReferentielPersistencePort<FacteurCaracterisation, FacteurCaracterisationId> {

    private FacteurCaracterisationRepository repository;

    private FacteurCaracterisationMapper mapper;

    @Override
    public FacteurCaracterisation save(FacteurCaracterisation referentiel) throws ReferentielException {
        var entityToSave = mapper.toEntity(referentiel);
        if (entityToSave != null) {
            var entitySaved = repository.save(entityToSave);
            return mapper.toDomain(entitySaved);
        }
        return null;
    }

    @Override
    public void saveAll(Collection<FacteurCaracterisation> referentiel) throws ReferentielException {
        repository.saveAll(mapper.toEntities(referentiel));
    }

    @Override
    public FacteurCaracterisation get(FacteurCaracterisationId id) throws ReferentielException {
        if (id != null) {
            FacteurCaracterisationIdEntity ieIdEntity = mapper.toEntityId(id);
            Optional<FacteurCaracterisationEntity> optionalEntity = repository.findById(ieIdEntity);
            return mapper.toDomain(
                    optionalEntity
                            .orElseThrow(() -> new ReferentielException("Facteur Caractérisation non trouvé"))
            );
        }
        throw new ReferentielException("Facteur Caractérisation non trouvé");
    }

    @Override
    public List<FacteurCaracterisation> findByFilters(String... params) throws ReferentielException {
        List<FacteurCaracterisationEntity> facteurCaracterisationEntity =
                repository.findByCritereAndEtapeAndNomAndLocalisationAndCategorieAndNomOrganisation(params[0], params[1], params[2], params[3], params[4], params[5]);

        return mapper.toDomains(facteurCaracterisationEntity);
    }

    @Override
    public void purge() {
        repository.deleteAll();
    }

    @Override
    public void purgeByOrganisation(String nomOrganisation) {
        repository.deleteByNomOrganisation(nomOrganisation);
    }

    @Override
    public List<FacteurCaracterisation> getAll() {
        return ListUtils.emptyIfNull(mapper.toDomains(repository.findAll()));
    }
}
