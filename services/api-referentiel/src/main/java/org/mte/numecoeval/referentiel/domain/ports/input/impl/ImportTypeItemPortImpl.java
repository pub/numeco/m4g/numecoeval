package org.mte.numecoeval.referentiel.domain.ports.input.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.BooleanUtils;
import org.mte.numecoeval.referentiel.domain.data.ResultatImport;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.domain.ports.input.ImportCSVReferentielPort;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.TypeItemDTO;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Slf4j
public class ImportTypeItemPortImpl implements ImportCSVReferentielPort<TypeItemDTO> {
    private static final String HEADER_TYPE = "type";
    private static final String[] HEADERS = new String[]{HEADER_TYPE, "categorie", "serveur", "commentaire", "dureeVieDefaut", "refHypothese", "source", "refItemParDefaut", "nomOrganisation"};
    private static final String[] MANDATORY_HEADERS = new String[]{HEADER_TYPE, "categorie", "serveur", "commentaire", "dureeVieDefaut", "refHypothese", "source", "refItemParDefaut"};

    public static String[] getHeaders() {
        return HEADERS;
    }

    public void checkCSVRecord(CSVRecord csvRecord, String nomOrganisation) throws ReferentielException {
        checkAllHeadersAreMapped(csvRecord, MANDATORY_HEADERS);
        checkFieldIsMappedAndNotBlankInCSVRecord(csvRecord, HEADER_TYPE);
        checkNomOrganisation(csvRecord, nomOrganisation);
    }

    @Override
    public ResultatImport<TypeItemDTO> importCSV(InputStream csvInputStream, String nomOrganisation) {
        ResultatImport<TypeItemDTO> resultatImport = new ResultatImport<>();
        List<TypeItemDTO> dtos = new ArrayList<>();

        try (Reader reader = new InputStreamReader(csvInputStream)) {
            Iterable<CSVRecord> records = CSVFormat.DEFAULT.builder()
                    .setHeader()
                    .setAllowMissingColumnNames(true)
                    .setDelimiter(CSV_SEPARATOR)
                    .setTrim(true)
                    .setSkipHeaderRecord(true)
                    .build().parse(reader);
            records.forEach(csvRecord -> {
                try {
                    checkCSVRecord(csvRecord, nomOrganisation);
                    dtos.add(TypeItemDTO.builder()
                            .type(csvRecord.get(HEADER_TYPE).trim())
                            .categorie(csvRecord.get("categorie").trim())
                            .serveur(BooleanUtils.toBoolean(csvRecord.get("serveur").trim()))
                            .commentaire(csvRecord.get("commentaire").trim())
                            .dureeVieDefaut(getDoubleValueFromRecord(csvRecord, "dureeVieDefaut", null))
                            .refHypothese(csvRecord.get("refHypothese").trim())
                            .source(csvRecord.get("source").trim())
                            .refItemParDefaut(getStringValueFromRecord(csvRecord, "refItemParDefaut"))
                            .nomOrganisation(getStringValueFromRecordDefaultEmpty(csvRecord, "nomOrganisation"))
                            .build());
                } catch (Exception e) {
                    log.error("Erreur prévue lors de la lecture de la ligne {} : {}", csvRecord.getRecordNumber() + 1, e.getMessage());
                    resultatImport.getErreurs().add(e.getMessage());
                }
            });

        } catch (Exception e) {
            log.error("Erreur de traitement du fichier", e);

            resultatImport.setErreurs(Collections.singletonList("Le fichier CSV n'a pas pu être lu."));
            resultatImport.setNbrLignesImportees(0);
            resultatImport.setObjects(null);
            return resultatImport;
        }
        resultatImport.setObjects(dtos);
        resultatImport.setNbrLignesImportees(dtos.size());
        return resultatImport;
    }

}
