package org.mte.numecoeval.referentiel.infrastructure.mapper;

import org.mapstruct.Mapper;
import org.mte.numecoeval.referentiel.domain.model.CorrespondanceRefEquipement;
import org.mte.numecoeval.referentiel.domain.model.id.CorrespondanceRefEquipementId;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.CorrespondanceRefEquipementEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.id.CorrespondanceRefEquipementIdEntity;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.CorrespondanceRefEquipementDTO;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.id.CorrespondanceRefEquipementIdDTO;

import java.util.Collection;
import java.util.List;

@Mapper(componentModel = "spring")
public interface CorrespondanceRefEquipementMapper {

    CorrespondanceRefEquipement toDomain(CorrespondanceRefEquipementEntity entity);

    CorrespondanceRefEquipementId toDomain(CorrespondanceRefEquipementIdDTO correspondanceRefEquipementIdDTO);

    CorrespondanceRefEquipement toDomain(CorrespondanceRefEquipementDTO dto);

    Collection<CorrespondanceRefEquipement> toDomains(List<CorrespondanceRefEquipementDTO> dtos);

    CorrespondanceRefEquipementEntity toEntity(CorrespondanceRefEquipement domain);

    CorrespondanceRefEquipementIdEntity toEntityId(CorrespondanceRefEquipementId id);

    List<CorrespondanceRefEquipementEntity> toEntities(Collection<CorrespondanceRefEquipement> domains);

    CorrespondanceRefEquipementDTO toDto(CorrespondanceRefEquipement domain);


}
