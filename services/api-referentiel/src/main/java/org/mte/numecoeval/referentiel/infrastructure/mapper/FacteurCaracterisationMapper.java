package org.mte.numecoeval.referentiel.infrastructure.mapper;

import org.mapstruct.Mapper;
import org.mte.numecoeval.referentiel.domain.model.FacteurCaracterisation;
import org.mte.numecoeval.referentiel.domain.model.id.FacteurCaracterisationId;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.FacteurCaracterisationEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.id.FacteurCaracterisationIdEntity;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.FacteurCaracterisationDTO;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.id.FacteurCaracterisationIdDTO;

import java.util.Collection;
import java.util.List;

@Mapper(componentModel = "spring")
public interface FacteurCaracterisationMapper {
    FacteurCaracterisationId toDomainId(FacteurCaracterisationIdDTO id);

    FacteurCaracterisationEntity toEntity(FacteurCaracterisation referentiel);

    List<FacteurCaracterisationEntity> toEntities(Collection<FacteurCaracterisation> referentiel);

    FacteurCaracterisationIdEntity toEntityId(FacteurCaracterisationId id);

    FacteurCaracterisation toDomain(FacteurCaracterisationEntity entity);

    List<FacteurCaracterisation> toDomains(List<FacteurCaracterisationEntity> entities);

    List<FacteurCaracterisation> toDomainsDTO(List<FacteurCaracterisationDTO> dtos);

    FacteurCaracterisation toDomain(FacteurCaracterisationDTO dto);

    List<FacteurCaracterisation> toDomainsFromDTO(List<FacteurCaracterisationDTO> iesDTO);

    FacteurCaracterisationDTO toDTO(FacteurCaracterisation facteurCaracterisation);

    List<FacteurCaracterisationDTO> toDTOs(List<FacteurCaracterisation> facteurCaracterisations);

}
