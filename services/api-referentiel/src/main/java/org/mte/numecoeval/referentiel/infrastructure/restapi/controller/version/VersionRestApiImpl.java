package org.mte.numecoeval.referentiel.infrastructure.restapi.controller.version;

import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.VersionDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class VersionRestApiImpl implements VersionRestApi {

    @Value("${version}")
    private String version;

    @Override
    public VersionDTO getVersion() {
        return new VersionDTO(version);
    }

}
