package org.mte.numecoeval.referentiel.infrastructure.mapper;

import org.mapstruct.Mapper;
import org.mte.numecoeval.referentiel.domain.model.TypeItem;
import org.mte.numecoeval.referentiel.domain.model.id.TypeItemId;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.TypeItemEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.id.TypeItemIdEntity;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.TypeItemDTO;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.id.TypeItemIdDTO;

import java.util.Collection;
import java.util.List;

@Mapper(componentModel = "spring")
public interface TypeItemMapper {

    TypeItem toDomain(TypeItemEntity TypeItemEntity);

    TypeItemId toDomain(TypeItemIdDTO typeItemIdDTO);

    List<TypeItem> toDomains(List<TypeItemEntity> TypeItemEntities);

    List<TypeItem> toDomainsDTO(List<TypeItemDTO> TypeItemDTOs);

    TypeItem toDomain(TypeItemDTO TypeItemDTO);

    TypeItemEntity toEntity(TypeItem TypeItem);

    TypeItemIdEntity toEntityId(TypeItemId id);

    List<TypeItemEntity> toEntities(Collection<TypeItem> TypeItems);

    TypeItemDTO toDTO(TypeItem TypeItem);


}
