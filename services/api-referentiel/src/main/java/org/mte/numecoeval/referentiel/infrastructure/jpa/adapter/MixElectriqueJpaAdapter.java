package org.mte.numecoeval.referentiel.infrastructure.jpa.adapter;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.domain.model.FacteurCaracterisation;
import org.mte.numecoeval.referentiel.domain.model.MixElectrique;
import org.mte.numecoeval.referentiel.domain.model.id.MixElectriqueId;
import org.mte.numecoeval.referentiel.domain.ports.output.ReferentielPersistencePort;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.FacteurCaracterisationEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.repository.FacteurCaracterisationRepository;
import org.mte.numecoeval.referentiel.infrastructure.mapper.FacteurCaracterisationMapper;
import org.mte.numecoeval.referentiel.infrastructure.mapper.MixElectriqueMapper;
import org.mte.numecoeval.referentiel.utils.Constants;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;


@Service
@Slf4j
@AllArgsConstructor
public class MixElectriqueJpaAdapter implements ReferentielPersistencePort<MixElectrique, MixElectriqueId> {
    FacteurCaracterisationRepository facteurCaracterisationRepository;
    MixElectriqueMapper mixElectriqueMapper;
    FacteurCaracterisationMapper facteurCaracterisationMapper;


    @Override
    public MixElectrique save(MixElectrique referentiel) throws ReferentielException {
        var facteurCaracterisation = mixElectriqueMapper.toFacteurCaracterisation(referentiel);

        var entityToSave = facteurCaracterisationMapper.toEntity(facteurCaracterisation);
        if (entityToSave != null) {
            facteurCaracterisationRepository.save(entityToSave);
            return referentiel;
        }
        return null;
    }

    @Override
    public void saveAll(Collection<MixElectrique> mixElecs) throws ReferentielException {
        List<FacteurCaracterisation> facteurCaracterisationList = mixElectriqueMapper.toFacteurCaracterisations(mixElecs.stream().toList());
        facteurCaracterisationRepository.saveAll(facteurCaracterisationMapper.toEntities(facteurCaracterisationList));
    }


    @Override
    public MixElectrique get(MixElectriqueId id) throws ReferentielException {
        if (id != null) {

            List<FacteurCaracterisationEntity> facteurCaracterisationEntities = facteurCaracterisationRepository.findByNiveauAndCategorieAndLocalisationAndCritere(Constants.MIXELEC_NIVEAU, Constants.MIXELEC_CATEGORIE, id.getPays(), id.getCritere());
            if (!facteurCaracterisationEntities.isEmpty()) {
                if (facteurCaracterisationEntities.size() > 1) {
                    log.warn("Il y a plusieurs mix électriques pour la même localisation {} et critère {}", id.getPays(), id.getCritere());
                }
                return mixElectriqueMapper.toMixElectrique(facteurCaracterisationMapper.toDomain(facteurCaracterisationEntities.get(0)));
            } else {
                throw new ReferentielException("Mix Electrique non trouvé pour l'id " + id);
            }
        }
        throw new ReferentielException("Mix Electrique non trouvé pour l'id null");
    }

    @Override
    public void purge() {
        facteurCaracterisationRepository.deleteByNiveauAndCategorie(Constants.MIXELEC_NIVEAU, Constants.MIXELEC_CATEGORIE);
    }

    @Override
    public List<MixElectrique> getAll() {
        return mixElectriqueMapper.toMixElectriques(facteurCaracterisationMapper.toDomains(facteurCaracterisationRepository.findByNiveauAndCategorie(Constants.MIXELEC_NIVEAU, Constants.MIXELEC_CATEGORIE)));
    }
}
