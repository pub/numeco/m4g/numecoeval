package org.mte.numecoeval.referentiel.domain.model.id;

import lombok.*;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
public class HypotheseId implements Serializable {
    String code;
    String nomOrganisation;
}
