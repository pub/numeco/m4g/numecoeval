package org.mte.numecoeval.referentiel.domain.model;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Builder
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CorrespondanceRefEquipement implements AbstractReferentiel {

    String modeleEquipementSource;
    String refEquipementCible;
    // Nom de l'organisation dans le cas où cette dernière à des références customisées, propres à son organisation
    String nomOrganisation;

}
