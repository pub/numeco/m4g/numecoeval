package org.mte.numecoeval.referentiel.infrastructure.restapi.controller.facteurcaracterisation;

import jakarta.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.domain.ports.input.impl.ImportFacteurCaracterisationPortImpl;
import org.mte.numecoeval.referentiel.infrastructure.adapter.export.FacteurCaracterisationCsvExportService;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.FacteurCaracterisationEntity;
import org.mte.numecoeval.referentiel.infrastructure.restapi.controller.BaseExportReferentiel;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.FacteurCaracterisationDTO;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.RapportImportDTO;
import org.mte.numecoeval.referentiel.infrastructure.restapi.facade.FacteurCaracterisationFacade;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.util.List;

@RestController
@Slf4j
@AllArgsConstructor
public class ReferentielFacteurCaracterisationRestApiImpl implements BaseExportReferentiel<FacteurCaracterisationEntity>, ReferentielFacteurCaracterisationRestApi, ReferentielAdministrationFacteurCaracterisationRestApi {
    private FacteurCaracterisationFacade referentielFacade;

    private FacteurCaracterisationCsvExportService csvExportService;

    @SneakyThrows
    @Override
    public List<FacteurCaracterisationDTO> get(String critere, String etapeacv, String nom, String localisation, String categorie, String nomOrganisation) {
        String[] params = {critere, etapeacv, nom, localisation, categorie, nomOrganisation};
        return referentielFacade.getByFilters(params);
    }


    @Override
    public RapportImportDTO importCSV(MultipartFile fichier, String mode, String nomOrganisation) throws IOException, ReferentielException {
        if (fichier == null || fichier.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Le fichier n'existe pas ou alors il est vide");
        }
        var rapportImport = new ImportFacteurCaracterisationPortImpl().importCSV(fichier.getInputStream(), nomOrganisation);
        if ("FULL".equals(mode)) {
            referentielFacade.purgeByOrganisationAndAddAll(rapportImport.getObjects(), nomOrganisation);
        } else if ("DELTA".equals(mode)) {
            referentielFacade.upsert(rapportImport.getObjects());
        } else {
            throw new ReferentielException("Le mode d'import indiqué n'est pas valide");
        }

        return new RapportImportDTO(
                fichier.getOriginalFilename(),
                rapportImport.getErreurs(),
                rapportImport.getNbrLignesImportees()
        );

    }

    @Override
    public void exportCSV(HttpServletResponse servletResponse) throws IOException {
        exportCSV(servletResponse, csvExportService, "facteursCaracterisation");
    }
}
