package org.mte.numecoeval.referentiel.infrastructure.restapi.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@Accessors(chain = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
@EqualsAndHashCode
@Builder
@Schema(
        description = "Référentiel des hypothèses utilisées pour les calculs"
)
public class HypotheseDTO implements Serializable {
    @Schema(
            description = "Code de l'hypothèse, clé du référentiel"
    )
    String code;
    @Schema(
            description = "Valeur de l'hypothèse"
    )
    String valeur;
    @Schema(
            description = "Source de l'hypothèse"
    )
    String source;
    @Schema(
            description = "Description de l'hypothèse"
    )
    String description;
    @Schema(
            description = "Nom de l'organisation dans le cas où cette dernière à des références customisées, propres à son organisation"
    )
    String nomOrganisation;
}
