package org.mte.numecoeval.referentiel.infrastructure.restapi.controller.correspondance;

import jakarta.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.domain.ports.input.impl.ImportCorrespondanceRefEquipementPortImpl;
import org.mte.numecoeval.referentiel.infrastructure.adapter.export.CorrespondanceRefEquipemenetCsvExportService;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.CorrespondanceRefEquipementEntity;
import org.mte.numecoeval.referentiel.infrastructure.restapi.controller.BaseExportReferentiel;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.CorrespondanceRefEquipementDTO;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.RapportImportDTO;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.id.CorrespondanceRefEquipementIdDTO;
import org.mte.numecoeval.referentiel.infrastructure.restapi.facade.CorrespondanceRefEquipementFacade;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;

@RestController
@Slf4j
@AllArgsConstructor
public class ReferentielCorrespondanceRefEquipementRestApiImpl implements BaseExportReferentiel<CorrespondanceRefEquipementEntity>, ReferentielCorrespondanceRefEquipementRestApi, ReferentielAdministrationCorrespondanceRefEquipementRestApi {
    private CorrespondanceRefEquipementFacade referentielFacade;

    private CorrespondanceRefEquipemenetCsvExportService csvExportService;

    @Override
    public RapportImportDTO importCSV(MultipartFile fichier, String nomOrganisation) throws IOException, ReferentielException {
        if (fichier == null || fichier.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Le fichier n'existe pas ou alors il est vide");
        }
        var rapportImport = new ImportCorrespondanceRefEquipementPortImpl().importCSV(fichier.getInputStream(), nomOrganisation);
        referentielFacade.purgeByOrganisationAndAddAll(rapportImport.getObjects(), nomOrganisation);

        return new RapportImportDTO(
                fichier.getOriginalFilename(),
                rapportImport.getErreurs(),
                rapportImport.getNbrLignesImportees()
        );
    }

    @SneakyThrows
    @Override
    public CorrespondanceRefEquipementDTO get(String modele, String nomOrganisation) {
        CorrespondanceRefEquipementIdDTO idDTO = new CorrespondanceRefEquipementIdDTO();
        idDTO.setModeleEquipementSource(modele);
        idDTO.setNomOrganisation(nomOrganisation);
        return referentielFacade.get(idDTO);
    }

    @Override
    public void exportCSV(HttpServletResponse servletResponse) throws IOException {
        exportCSV(servletResponse, csvExportService, "correspondancesRefEquipement");
    }
}
