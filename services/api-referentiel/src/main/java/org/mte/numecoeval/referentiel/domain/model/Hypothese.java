package org.mte.numecoeval.referentiel.domain.model;

import lombok.*;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
public class Hypothese implements AbstractReferentiel {
    String code;
    String valeur;
    String source;
    String description;
    // Nom de l'organisation dans le cas où cette dernière à des références customisées, propres à son organisation
    String nomOrganisation;

}
