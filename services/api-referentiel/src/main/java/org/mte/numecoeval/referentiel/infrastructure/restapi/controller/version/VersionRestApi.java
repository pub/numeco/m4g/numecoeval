package org.mte.numecoeval.referentiel.infrastructure.restapi.controller.version;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.VersionDTO;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;

public interface VersionRestApi {

    @Operation(
            summary = "Endpoint interne à NumEcoEval - Récupération de la version courante",
            description = """
                    Endpoint interne utilisé pour connaître la version installée
                    """,
            tags = "Interne NumEcoEval",
            operationId = "getVersion"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Version",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = VersionDTO.class))})
    })
    @GetMapping(path = "/version", produces = MediaType.APPLICATION_JSON_VALUE)
    VersionDTO getVersion();

}
