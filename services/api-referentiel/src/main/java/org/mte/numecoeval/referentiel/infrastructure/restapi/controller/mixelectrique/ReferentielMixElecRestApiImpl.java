package org.mte.numecoeval.referentiel.infrastructure.restapi.controller.mixelectrique;

import jakarta.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.domain.model.MixElectrique;
import org.mte.numecoeval.referentiel.domain.ports.input.impl.ImportMixElectriquePortImpl;
import org.mte.numecoeval.referentiel.infrastructure.adapter.export.MixElectriqueCsvExportService;
import org.mte.numecoeval.referentiel.infrastructure.restapi.controller.BaseExportReferentiel;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.MixElectriqueDTO;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.RapportImportDTO;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.id.MixElectriqueIdDTO;
import org.mte.numecoeval.referentiel.infrastructure.restapi.facade.MixElectriqueFacade;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.util.List;

@RestController
@Slf4j
@AllArgsConstructor
public class ReferentielMixElecRestApiImpl implements BaseExportReferentiel<MixElectrique>, ReferentielMixElectriqueRestApi, ReferentielAdministrationMixElectriqueRestApi {
    private MixElectriqueFacade referentielFacade;

    private MixElectriqueCsvExportService csvExportService;

    @Override
    public RapportImportDTO importCSV(MultipartFile fichier) throws IOException, ReferentielException {
        if (fichier == null || fichier.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Le fichier n'existe pas ou alors il est vide");
        }
        var rapportImport = new ImportMixElectriquePortImpl().importCSV(fichier.getInputStream());
        referentielFacade.purgeAndAddAll(rapportImport.getObjects());

        return new RapportImportDTO(
                fichier.getOriginalFilename(),
                rapportImport.getErreurs(),
                rapportImport.getNbrLignesImportees()
        );
    }

    @SneakyThrows
    @Override
    public List<MixElectriqueDTO> get(String pays) {

        if ("_all".equals(pays)) {
            return referentielFacade.getAll();
        } else {
            return referentielFacade.getByPays(pays);
        }

    }

    @SneakyThrows
    @Override
    public MixElectriqueDTO get(String pays, String critere) {
        MixElectriqueIdDTO id = MixElectriqueIdDTO.builder()
                .pays(pays)
                .critere(critere)
                .build();

        return referentielFacade.get(id);
    }

    @Override
    public void exportCSV(HttpServletResponse servletResponse) throws IOException {
        exportCSV(servletResponse, csvExportService, "mixElec");
    }

}
