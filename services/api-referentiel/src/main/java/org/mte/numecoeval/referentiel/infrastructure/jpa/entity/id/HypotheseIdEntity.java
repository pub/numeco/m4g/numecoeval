package org.mte.numecoeval.referentiel.infrastructure.jpa.entity.id;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@Getter
@Setter
@Accessors(chain = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
public class HypotheseIdEntity implements AbstractReferentieIdEntity {

    String code;
    String nomOrganisation;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        HypotheseIdEntity that = (HypotheseIdEntity) o;

        return new EqualsBuilder().append(code, that.code).append(nomOrganisation, that.nomOrganisation).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(code).append(nomOrganisation).toHashCode();
    }
}
