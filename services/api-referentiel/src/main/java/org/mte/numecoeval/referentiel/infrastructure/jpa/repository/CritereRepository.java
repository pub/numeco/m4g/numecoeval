package org.mte.numecoeval.referentiel.infrastructure.jpa.repository;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.CritereEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.id.CritereIdEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.Description;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(
        path = "Critere",
        collectionResourceDescription = @Description("""
                Endpoints CRUD généré par Spring Data REST pour la récupération de plusieurs critères d'impact écologique.
                """),
        itemResourceDescription = @Description("""
                Endpoints CRUD généré par Spring Data REST pour la récupération d'un critère d'impact écologique.
                """)
)
@Tag(name = "Critères - CRUD/Spring Data REST")
public interface CritereRepository extends JpaRepository<CritereEntity, CritereIdEntity> {

}
