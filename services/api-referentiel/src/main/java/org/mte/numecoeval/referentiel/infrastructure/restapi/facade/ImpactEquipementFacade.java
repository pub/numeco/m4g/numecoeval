package org.mte.numecoeval.referentiel.infrastructure.restapi.facade;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.ImpactEquipementDTO;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.domain.model.ImpactEquipement;
import org.mte.numecoeval.referentiel.domain.model.id.ImpactEquipementId;
import org.mte.numecoeval.referentiel.domain.ports.output.ReferentielPersistencePort;
import org.mte.numecoeval.referentiel.infrastructure.mapper.ImpactEquipementMapper;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.id.ImpactEquipementIdDTO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@AllArgsConstructor
public class ImpactEquipementFacade {

    private ReferentielPersistencePort<ImpactEquipement, ImpactEquipementId> persistencePort;


    private ImpactEquipementMapper mapper;


    public ImpactEquipementDTO get(ImpactEquipementIdDTO id) throws ReferentielException {
        ImpactEquipementId equipementId = mapper.toDomainId(id);
        ImpactEquipement impactEquipement = persistencePort.get(equipementId);
        return mapper.toDTO(impactEquipement);
    }

    public void purgeAndAddAll(List<ImpactEquipementDTO> iesDTO) throws ReferentielException {
        persistencePort.purge();
        persistencePort.saveAll(mapper.toDomainsFromDTO(iesDTO));
    }
}
