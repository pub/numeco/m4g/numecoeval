package org.mte.numecoeval.referentiel.infrastructure.jpa.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import jakarta.persistence.Table;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.id.HypotheseIdEntity;

@Getter
@Setter
@Accessors(chain = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@Entity
@IdClass(HypotheseIdEntity.class)
@Table(name = "REF_HYPOTHESE")
public class HypotheseEntity implements AbstractReferentielEntity {
    @Id
    String code;
    @Id
    // Nom de l'organisation dans le cas où cette dernière à des références customisées, propres à son organisation
    String nomOrganisation;
    String valeur;
    String source;
    String description;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        HypotheseEntity that = (HypotheseEntity) o;

        return new EqualsBuilder().append(code, that.code).append(valeur, that.valeur).append(source, that.source).append(description, that.description).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(code).append(valeur).append(source).append(description).toHashCode();
    }
}
