package org.mte.numecoeval.referentiel;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles(profiles = {"test"})
class ReferentielApplicationTests {
    @Test
    void contextLoads() {
        Assertions.assertNotNull(ReferentielApplication.class);
    }

}
