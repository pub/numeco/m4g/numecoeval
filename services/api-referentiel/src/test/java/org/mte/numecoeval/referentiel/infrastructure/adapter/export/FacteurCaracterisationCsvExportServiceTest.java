package org.mte.numecoeval.referentiel.infrastructure.adapter.export;

import org.apache.commons.csv.CSVPrinter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mte.numecoeval.referentiel.domain.ports.input.impl.ImportFacteurCaracterisationPortImpl;
import org.mte.numecoeval.referentiel.factory.TestDataFactory;
import org.mte.numecoeval.referentiel.infrastructure.jpa.repository.FacteurCaracterisationRepository;
import org.mte.numecoeval.referentiel.utils.Constants;

import java.io.IOException;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

public class FacteurCaracterisationCsvExportServiceTest {
    @InjectMocks
    FacteurCaracterisationCsvExportService exportService;

    @Mock
    FacteurCaracterisationRepository repository;

    @Mock
    CSVPrinter csvPrinter;

    @BeforeEach
    void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getHeadersShouldReturnSameHeadersAsImport() {
        assertEquals(ImportFacteurCaracterisationPortImpl.getHeaders(), exportService.getHeaders());
    }

    @Test
    void getObjectsToWriteShouldReturnRepositoryFindAll() {

        var entities = List.of(
                TestDataFactory.FacteurCaracterisationFactory.entity(
                        "Ecran 27 pouces",
                        TestDataFactory.EtapeFactory.entity("UTILISATION", ""),
                        TestDataFactory.CritereFactory.entity("Changement climatique", "", ""),
                        "description", Constants.EQUIPEMENT_NIVEAU, "tiers", "Monitor", 0.01, "France", 0.02, "kgCO2e",
                        "NegaOctet", "ADEO"),
                TestDataFactory.FacteurCaracterisationFactory.entity(
                        "Ecran 27 pouces",
                        TestDataFactory.EtapeFactory.entity("DISTRIBUTION", ""),
                        TestDataFactory.CritereFactory.entity("Changement climatique", "", ""),
                        "description", Constants.EQUIPEMENT_NIVEAU, "tiers", "Monitor", 0.01, "France", 0.02, "kgCO2e",
                        "NegaOctet", "ADEO")
        );
        when(repository.findAll()).thenReturn(entities);

        assertEquals(entities, exportService.getObjectsToWrite());
    }

    @Test
    void printRecordShouldUseEntityAttributes() throws IOException {
        var entity = TestDataFactory.FacteurCaracterisationFactory.entity(
                "Ecran 27 pouces",
                TestDataFactory.EtapeFactory.entity("DISTRIBUTION", ""),
                TestDataFactory.CritereFactory.entity("Changement climatique", "", ""),
                "description", Constants.EQUIPEMENT_NIVEAU, "tiers", "Monitor", 0.01, "France", 0.02, "kgCO2e",
                "NegaOctet", "ADEO");
        DecimalFormat df = new DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale.ENGLISH));
        df.setMaximumFractionDigits(340);

        assertDoesNotThrow(() -> exportService.printRecord(csvPrinter, entity));

        Mockito.verify(csvPrinter, times(1)).printRecord(entity.getNom(),
                entity.getEtape(), entity.getCritere(), entity.getDescription(), entity.getNiveau(), entity.getTiers(), entity.getCategorie(),
                df.format(entity.getConsoElecMoyenne()), entity.getLocalisation(), df.format(entity.getValeur()),
                entity.getUnite(), entity.getSource(), entity.getNomOrganisation());
    }

    @Test
    void logRecordErrorShouldLogSpecificErrorForRecord() {
        var entity = TestDataFactory.FacteurCaracterisationFactory.entity(
                "Ecran 27 pouces",
                TestDataFactory.EtapeFactory.entity("DISTRIBUTION", ""),
                TestDataFactory.CritereFactory.entity("Changement climatique", "", ""),
                "description", Constants.EQUIPEMENT_NIVEAU, "tiers", "Monitor", 0.01, "France", 0.02, "kgCO2e",
                "NegaOctet", "ADEO");

        assertDoesNotThrow(() -> exportService.logRecordError(entity, new Exception("Test")));
    }

    @Test
    void logRecordErrorShouldLogGenericErrorForRecord() {
        assertDoesNotThrow(() -> exportService.logRecordError(null, new Exception("Test")));
    }

    @Test
    void logRecordErrorShouldLogGenericErrorForFile() {
        assertDoesNotThrow(() -> exportService.logWriterError(new Exception("Test")));
    }

    @Test
    void writeToCsvShouldReturnCSV() {
        // given
        var entities = List.of(
                TestDataFactory.FacteurCaracterisationFactory.entity(
                        "Ecran 27 pouces",
                        TestDataFactory.EtapeFactory.entity("DISTRIBUTION", ""),
                        TestDataFactory.CritereFactory.entity("Changement climatique", "", ""),
                        "description", Constants.EQUIPEMENT_NIVEAU, "tiers", "Monitor", 0.01, "France", 0.02, "kgCO2e",
                        "NegaOctet", "ADEO")
        );
        when(repository.findAll()).thenReturn(entities);
        StringWriter stringWriter = new StringWriter();

        // when
        exportService.writeToCsv(stringWriter);

        // Then
        String result = stringWriter.toString();
        assertEquals(
                "nom;etapeacv;critere;description;niveau;tiers;categorie;consoElecMoyenne;localisation;valeur;unite;source;nomOrganisation\r\n" +
                        "Ecran 27 pouces;DISTRIBUTION;Changement climatique;description;2-Equipement;tiers;Monitor;0.01;France;0.02;kgCO2e;NegaOctet;ADEO\r\n",
                result
        );
    }


}
