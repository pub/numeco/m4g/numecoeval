package org.mte.numecoeval.referentiel.infrastructure.restapi.facade;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.domain.model.Critere;
import org.mte.numecoeval.referentiel.domain.model.id.CritereId;
import org.mte.numecoeval.referentiel.domain.ports.output.ReferentielPersistencePort;
import org.mte.numecoeval.referentiel.factory.TestDataFactory;
import org.mte.numecoeval.referentiel.infrastructure.mapper.CritereMapper;
import org.mte.numecoeval.referentiel.infrastructure.mapper.CritereMapperImpl;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.mte.numecoeval.referentiel.factory.TestDataFactory.DEFAULT_CRITERE;
import static org.mte.numecoeval.referentiel.factory.TestDataFactory.DEFAULT_UNITE;

class CritereFacadeTest {

    @InjectMocks
    CritereFacade facadeToTest;

    @Mock
    private ReferentielPersistencePort<Critere, CritereId> persistencePort;

    private CritereMapper mapper = new CritereMapperImpl();

    @BeforeEach
    void setup() {
        MockitoAnnotations.openMocks(this);
        ReflectionTestUtils.setField(facadeToTest, "mapper", mapper);
    }

    @Test
    void getAll_ShouldAllReturnMatchingDTO() {
        var expectedDomains = Arrays.asList(
                TestDataFactory.CritereFactory.domain(DEFAULT_CRITERE, "test", DEFAULT_UNITE),
                TestDataFactory.CritereFactory.domain("Acidification", "Autre Test", "m3 eau")
        );

        Mockito.when(persistencePort.getAll()).thenReturn(expectedDomains);

        var result = assertDoesNotThrow( () -> facadeToTest.getAll() );

        assertEquals(expectedDomains.size(), result.size());

        expectedDomains.forEach( expectedDomain -> {
            var matchingDTO = result.stream()
                    .filter(critereDTO -> expectedDomain.getNomCritere().equals(critereDTO.getNomCritere()))
                    .findAny();

            assertTrue(matchingDTO.isPresent(), "Il n'existe pas de DTO correspondant au domain");
            var resultDTO = matchingDTO.get();
            assertEquals(expectedDomain.getNomCritere(), resultDTO.getNomCritere());
            assertEquals(expectedDomain.getDescription(), resultDTO.getDescription());
            assertEquals(expectedDomain.getUnite(), resultDTO.getUnite());

        });
    }

    @Test
    void purgeAndAddAll_ShouldCallPurgeThenSaveAll() throws ReferentielException {
        var dtosToSave = Arrays.asList(
                TestDataFactory.CritereFactory.dto(DEFAULT_CRITERE, "test", DEFAULT_UNITE),
                TestDataFactory.CritereFactory.dto("Acidification", "Autre Test", "m3 eau")
        );
        ArgumentCaptor<Collection<Critere>> valueCapture = ArgumentCaptor.forClass(Collection.class);

        assertDoesNotThrow(() -> facadeToTest.purgeAndAddAll(dtosToSave));

        Mockito.verify(persistencePort, Mockito.times(1)).purge();
        Mockito.verify(persistencePort, Mockito.times(1)).saveAll(valueCapture.capture());

        var expectedDomains = valueCapture.getValue();
        assertNotNull(expectedDomains);
        assertEquals(dtosToSave.size(), expectedDomains.size());
        dtosToSave.forEach(dto -> {
            var matchingDomain = expectedDomains.stream()
                    .filter(domain -> dto.getNomCritere().equals(domain.getNomCritere()))
                    .findAny();
            assertTrue(matchingDomain.isPresent());
            var critere = matchingDomain.get();
            assertEquals(dto.getNomCritere(), critere.getNomCritere());
            assertEquals(dto.getDescription(), critere.getDescription());
            assertEquals(dto.getUnite(), critere.getUnite());
        });
    }
}
