package org.mte.numecoeval.referentiel.domain.port.input;

import org.junit.jupiter.api.Test;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.TypeEquipementDTO;
import org.mte.numecoeval.referentiel.domain.ports.input.ImportCSVReferentielPort;
import org.mte.numecoeval.referentiel.domain.ports.input.impl.ImportTypeEquipementPortImpl;
import org.springframework.util.ResourceUtils;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

class ImportTypeEquipementPortTest {

    ImportCSVReferentielPort<TypeEquipementDTO> importPortToTest = new ImportTypeEquipementPortImpl();

    @Test
    void importCSV_shouldImportAllDatas() throws Exception {
        File file = ResourceUtils.getFile("classpath:csv/unit/typeEquipement.csv");
        var resultatImport = importPortToTest.importCSV(new FileInputStream(file));

        assertEquals(2, resultatImport.getNbrLignesImportees());
        assertEquals(0, resultatImport.getErreurs().size());
        assertEquals(2, resultatImport.getObjects().size());
        assertTrue(resultatImport.getObjects().stream().anyMatch(typeEquipementDTO ->
                "Serveur".equals(typeEquipementDTO.getType())
                        && typeEquipementDTO.isServeur()
                        && Double.valueOf(6.0).equals(typeEquipementDTO.getDureeVieDefaut())
                        && "Exemple de serveur basique".equals(typeEquipementDTO.getSource())
                        && "Test simple".equals(typeEquipementDTO.getCommentaire())
                        && "serveur_par_defaut".equals(typeEquipementDTO.getRefEquipementParDefaut())
                )
        );
        assertTrue(resultatImport.getObjects().stream().anyMatch(typeEquipementDTO ->
                "Laptop".equals(typeEquipementDTO.getType())
                        && !typeEquipementDTO.isServeur()
                        && Double.valueOf(5.0).equals(typeEquipementDTO.getDureeVieDefaut())
                        && "ADEME, durée de vie des EEE, UF utiliser une ordinateur, écran non cathodique et LCD, https://vu.fr/bOQZ".equals(typeEquipementDTO.getSource())
                        && "Test simple".equals(typeEquipementDTO.getCommentaire())
                        && "laptop_par_defaut".equals(typeEquipementDTO.getRefEquipementParDefaut())
                )
        );
    }

    @Test
    void importCSV_whenErrorInMiddleOfFile_shouldImportDatasWithErrors() throws Exception {
        File file = ResourceUtils.getFile("classpath:csv/unit/typeEquipement_errorInMiddle.csv");
        var resultatImport = importPortToTest.importCSV(new FileInputStream(file));

        assertEquals(2, resultatImport.getNbrLignesImportees());
        assertEquals(1, resultatImport.getErreurs().size());
        assertTrue(resultatImport.getErreurs().stream().anyMatch("La ligne n°3 est invalide : La colonne type ne peut être vide"::equals));
        assertEquals(2, resultatImport.getObjects().size());
        assertTrue(resultatImport.getObjects().stream().anyMatch(typeEquipementDTO ->
                        "Serveur".equals(typeEquipementDTO.getType())
                                && typeEquipementDTO.isServeur()
                                && Double.valueOf(6.0).equals(typeEquipementDTO.getDureeVieDefaut())
                                && "Exemple de serveur basique".equals(typeEquipementDTO.getSource())
                                && "Test simple".equals(typeEquipementDTO.getCommentaire())
                                && "serveur_par_defaut".equals(typeEquipementDTO.getRefEquipementParDefaut())
                )
        );
        assertTrue(resultatImport.getObjects().stream().anyMatch(typeEquipementDTO ->
                        "Laptop".equals(typeEquipementDTO.getType())
                                && !typeEquipementDTO.isServeur()
                                && Double.valueOf(5.0).equals(typeEquipementDTO.getDureeVieDefaut())
                                && "ADEME, durée de vie des EEE, UF utiliser une ordinateur, écran non cathodique et LCD, https://vu.fr/bOQZ".equals(typeEquipementDTO.getSource())
                                && "Test simple".equals(typeEquipementDTO.getCommentaire())
                                && "laptop_par_defaut".equals(typeEquipementDTO.getRefEquipementParDefaut())
                )
        );
    }

    @Test
    void importCSV_whenWrongFile_shouldReturnOnlyErrors() throws Exception {
        File file = ResourceUtils.getFile("classpath:csv/unit/wrongCSVFile.csv");
        var resultatImport = importPortToTest.importCSV(new FileInputStream(file));

        assertEquals(0, resultatImport.getNbrLignesImportees());
        assertEquals(1, resultatImport.getErreurs().size());
        assertTrue(resultatImport.getErreurs().stream().anyMatch("La ligne n°2 est invalide : Entêtes incohérentes"::equals));
    }

    @Test
    void importCSV_whenStreamAlreadyClosedShouldReturnReportWithOneError() throws IOException {
        DataInputStream dataInputStream = mock(DataInputStream.class);
        doNothing().when(dataInputStream).close();

        var resultatImport = importPortToTest.importCSV(dataInputStream);

        verify(dataInputStream).close();
        assertEquals(0, resultatImport.getNbrLignesImportees());
        assertEquals(1, resultatImport.getErreurs().size());
        assertEquals("Le fichier CSV n'a pas pu être lu.", resultatImport.getErreurs().get(0));
    }

    @Test
    void importCSV_whenFileNotFoundShouldReturnReportWithOneError() throws IOException {
        DataInputStream dataInputStream = mock(DataInputStream.class);
        doThrow(new FileNotFoundException("start Read csv etape")).when(dataInputStream).close();

        var resultatImport = importPortToTest.importCSV(dataInputStream);

        verify(dataInputStream).close();
        assertEquals(0, resultatImport.getNbrLignesImportees());
        assertEquals(1, resultatImport.getErreurs().size());
        assertEquals("Le fichier CSV n'a pas pu être lu.", resultatImport.getErreurs().get(0));
    }
}
