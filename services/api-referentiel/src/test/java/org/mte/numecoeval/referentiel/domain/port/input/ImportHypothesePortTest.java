package org.mte.numecoeval.referentiel.domain.port.input;

import org.junit.jupiter.api.Test;
import org.mte.numecoeval.referentiel.domain.ports.input.ImportCSVReferentielPort;
import org.mte.numecoeval.referentiel.domain.ports.input.impl.ImportHypothesePortImpl;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.HypotheseDTO;
import org.springframework.util.ResourceUtils;

import java.io.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

class ImportHypothesePortTest {

    ImportCSVReferentielPort<HypotheseDTO> importPortToTest = new ImportHypothesePortImpl();

    @Test
    void importCSV_shouldImportAllDatas() throws Exception {
        File file = ResourceUtils.getFile("classpath:csv/unit/hypothese.csv");
        var resultatImport = importPortToTest.importCSV(new FileInputStream(file), "ADEO");

        assertEquals(2, resultatImport.getNbrLignesImportees());
        assertEquals(0, resultatImport.getErreurs().size());
        assertEquals(2, resultatImport.getObjects().size());
        assertTrue(resultatImport.getObjects().stream().anyMatch(dto -> "PUEPardDfault".equals(dto.getCode()) && "1.6".equals(dto.getValeur()) && "expertise IJO".equals(dto.getSource()) && "ADEO".equals(dto.getNomOrganisation())));
        assertTrue(resultatImport.getObjects().stream().anyMatch(dto -> "DureeVieServeurParDefaut".equals(dto.getCode()) && "3".equals(dto.getValeur()) && "US 46".equals(dto.getSource()) && "ADEO".equals(dto.getNomOrganisation())));
    }

    @Test
    void importCSV_whenErrorInMiddleOfFile_shouldImportDatasWithErrors() throws Exception {
        File file = ResourceUtils.getFile("classpath:csv/unit/hypothese_errorInMiddle.csv");
        var resultatImport = importPortToTest.importCSV(new FileInputStream(file), "ADEO");

        assertEquals(2, resultatImport.getNbrLignesImportees());
        assertEquals(2, resultatImport.getErreurs().size());
        assertTrue(resultatImport.getErreurs().stream().anyMatch("La ligne n°3 est invalide : La colonne cle ne peut être vide"::equals));
        assertTrue(resultatImport.getErreurs().stream().anyMatch("La ligne n°4 est invalide : La colonne valeur ne peut être vide"::equals));
        assertEquals(2, resultatImport.getObjects().size());
        assertTrue(resultatImport.getObjects().stream().anyMatch(dto -> "PUEPardDfault".equals(dto.getCode()) && "1.6".equals(dto.getValeur()) && "expertise IJO".equals(dto.getSource()) && "ADEO".equals(dto.getNomOrganisation())));
        assertTrue(resultatImport.getObjects().stream().anyMatch(dto -> "DureeVieServeurParDefaut".equals(dto.getCode()) && "3".equals(dto.getValeur()) && "US 46".equals(dto.getSource()) && "ADEO".equals(dto.getNomOrganisation())));
    }

    @Test
    void importCSV_whenWrongFile_shouldReturnOnlyErrors() throws Exception {
        File file = ResourceUtils.getFile("classpath:csv/unit/wrongCSVFile.csv");
        var resultatImport = importPortToTest.importCSV(new FileInputStream(file), "ADEO");

        assertEquals(0, resultatImport.getNbrLignesImportees());
        assertEquals(1, resultatImport.getErreurs().size());
        assertTrue(resultatImport.getErreurs().stream().anyMatch("La ligne n°2 est invalide : Entêtes incohérentes"::equals));
    }

    @Test
    void importCSV_whenStreamAlreadyClosedShouldReturnReportWithOneError() throws IOException {
        DataInputStream dataInputStream = mock(DataInputStream.class);
        doNothing().when(dataInputStream).close();

        var resultatImport = importPortToTest.importCSV(dataInputStream, "ADEO");

        verify(dataInputStream).close();
        assertEquals(0, resultatImport.getNbrLignesImportees());
        assertEquals(1, resultatImport.getErreurs().size());
        assertEquals("Le fichier CSV n'a pas pu être lu.", resultatImport.getErreurs().get(0));
    }

    @Test
    void importCSV_whenFileNotFoundShouldReturnReportWithOneError() throws IOException {
        DataInputStream dataInputStream = mock(DataInputStream.class);
        doThrow(new FileNotFoundException("start Read csv etape")).when(dataInputStream).close();

        var resultatImport = importPortToTest.importCSV(dataInputStream, "ADEO");

        verify(dataInputStream).close();
        assertEquals(0, resultatImport.getNbrLignesImportees());
        assertEquals(1, resultatImport.getErreurs().size());
        assertEquals("Le fichier CSV n'a pas pu être lu.", resultatImport.getErreurs().get(0));
    }
}
