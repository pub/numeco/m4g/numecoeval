package org.mte.numecoeval.referentiel.infrastructure.adapter.export;

import org.apache.commons.csv.CSVPrinter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mte.numecoeval.referentiel.domain.ports.input.impl.ImportTypeItemPortImpl;
import org.mte.numecoeval.referentiel.factory.TestDataFactory;
import org.mte.numecoeval.referentiel.infrastructure.jpa.repository.TypeItemRepository;

import java.io.IOException;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

class TypeItemCsvExportServiceTest {

    @InjectMocks
    TypeItemCsvExportService exportService;

    @Mock
    TypeItemRepository repository;

    @Mock
    CSVPrinter csvPrinter;

    @BeforeEach
    void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getHeadersShouldReturnSameHeadersAsImport() {
        Assertions.assertEquals(ImportTypeItemPortImpl.getHeaders(), exportService.getHeaders());
    }

    @Test
    void getObjectsToWriteShouldReturnRepositoryFindAll() {
        var entities = List.of(
                TestDataFactory.TypeItemFactory.entity(
                        "Serveur", null, true, "Exemple de serveur basique",
                        6.0, null, "NegaOctet", "serveur_par_defaut", null),
                TestDataFactory.TypeItemFactory.entity(
                        "deplacement voiture hybride", "deplacement_hybride", false,
                        "Exemple d'opération non it", 50.0, "CONSO_MOYENNE_VOITURE_HYBRIDE", "SSG", "deplacement_voiture_hybride", null
                )
        );
        when(repository.findAll()).thenReturn(entities);

        assertEquals(entities, exportService.getObjectsToWrite());
    }

    @Test
    void printRecordShouldUseEntityAttributes() throws IOException {
        var entity = TestDataFactory.TypeItemFactory.entity(
                "Serveur", null, true, "Exemple de serveur basique",
                6.0, null, "NegaOctet", "serveur_par_defaut", null);
        DecimalFormat df = new DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale.ENGLISH));
        df.setMaximumFractionDigits(340);

        assertDoesNotThrow(() -> exportService.printRecord(csvPrinter, entity));

        Mockito.verify(csvPrinter, times(1)).printRecord(
                entity.getType(),
                entity.getCategorie(),
                entity.isServeur(),
                entity.getCommentaire(),
                df.format(entity.getDureeVieDefaut()),
                entity.getRefHypothese(),
                entity.getSource(),
                entity.getRefItemParDefaut(),
                entity.getNomOrganisation());
    }

    @Test
    void logRecordErrorShouldLogSpecificErrorForRecord() {
        var entity = TestDataFactory.TypeItemFactory.entity(
                "Serveur", null, true, "Exemple de serveur basique",
                6.0, null, "NegaOctet", "serveur_par_defaut", null);

        assertDoesNotThrow(() -> exportService.logRecordError(entity, new Exception("Test")));
    }

    @Test
    void logRecordErrorShouldLogGenericErrorForRecord() {
        assertDoesNotThrow(() -> exportService.logRecordError(null, new Exception("Test")));
    }

    @Test
    void logRecordErrorShouldLogGenericErrorForFile() {
        assertDoesNotThrow(() -> exportService.logWriterError(new Exception("Test")));
    }

    @Test
    void writeToCsvShouldReturnCSV() {
        // given
        var entities = List.of(
                TestDataFactory.TypeItemFactory.entity(
                        "Serveur", null, true, "Exemple de serveur basique",
                        6.0, null, "NegaOctet", "serveur_par_defaut", "ADEO")
        );
        when(repository.findAll()).thenReturn(entities);
        StringWriter stringWriter = new StringWriter();

        // when
        exportService.writeToCsv(stringWriter);

        // Then
        String result = stringWriter.toString();
        assertEquals(
                """
                        type;categorie;serveur;commentaire;dureeVieDefaut;refHypothese;source;refItemParDefaut;nomOrganisation
                        Serveur;;true;Exemple de serveur basique;6;;NegaOctet;serveur_par_defaut;ADEO
                        """,
                result.replaceAll("\r\n", "\n")
        );
    }
}
