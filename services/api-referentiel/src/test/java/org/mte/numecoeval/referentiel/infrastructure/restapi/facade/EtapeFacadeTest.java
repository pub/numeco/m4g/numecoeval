package org.mte.numecoeval.referentiel.infrastructure.restapi.facade;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.domain.model.Etape;
import org.mte.numecoeval.referentiel.domain.model.id.EtapeId;
import org.mte.numecoeval.referentiel.domain.ports.output.ReferentielPersistencePort;
import org.mte.numecoeval.referentiel.factory.TestDataFactory;
import org.mte.numecoeval.referentiel.infrastructure.mapper.EtapeMapper;
import org.mte.numecoeval.referentiel.infrastructure.mapper.EtapeMapperImpl;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

class EtapeFacadeTest {

    @InjectMocks
    EtapeFacade facadeToTest;

    @Mock
    private ReferentielPersistencePort<Etape, EtapeId> persistencePort;

    private EtapeMapper mapper = new EtapeMapperImpl();

    @BeforeEach
    void setup() {
        MockitoAnnotations.openMocks(this);
        ReflectionTestUtils.setField(facadeToTest, "mapper", mapper);
    }

    @Test
    void getAll_ShouldAllReturnMatchingDTO() {
        var expectedDomains = Arrays.asList(
                TestDataFactory.EtapeFactory.domain(TestDataFactory.DEFAULT_CRITERE, "test"),
                TestDataFactory.EtapeFactory.domain("Acidification", "Autre Test")
        );

        Mockito.when(persistencePort.getAll()).thenReturn(expectedDomains);

        var result = assertDoesNotThrow( () -> facadeToTest.getAll() );

        assertEquals(expectedDomains.size(), result.size());

        expectedDomains.forEach( expectedDomain -> {
            var matchingDTO = result.stream()
                    .filter(dto -> expectedDomain.getCode().equals(dto.getCode()))
                    .findAny();

            assertTrue(matchingDTO.isPresent(), "Il n'existe pas de DTO correspondant au domain");
            var resultDTO = matchingDTO.get();
            assertEquals(expectedDomain.getCode(), resultDTO.getCode());
            assertEquals(expectedDomain.getLibelle(), resultDTO.getLibelle());

        });
    }

    @Test
    void purgeAndAddAll_ShouldCallPurgeThenSaveAll() throws ReferentielException {
        var dtosToSave = Arrays.asList(
                TestDataFactory.EtapeFactory.dto(TestDataFactory.DEFAULT_CRITERE, "test"),
                TestDataFactory.EtapeFactory.dto("Acidification", "Autre Test")
        );
        ArgumentCaptor<Collection<Etape>> valueCapture = ArgumentCaptor.forClass(Collection.class);

        assertDoesNotThrow(() -> facadeToTest.purgeAndAddAll(dtosToSave));

        Mockito.verify(persistencePort, Mockito.times(1)).purge();
        Mockito.verify(persistencePort, Mockito.times(1)).saveAll(valueCapture.capture());

        var expectedCriteres = valueCapture.getValue();
        assertNotNull(expectedCriteres);
        assertEquals(dtosToSave.size(), expectedCriteres.size());
        dtosToSave.forEach(dto -> {
            var matchingCriteres = expectedCriteres.stream()
                    .filter(domain -> dto.getCode().equals(domain.getCode()))
                    .findAny();
            assertTrue(matchingCriteres.isPresent());
            var critere = matchingCriteres.get();
            assertEquals(dto.getCode(), critere.getCode());
            assertEquals(dto.getLibelle(), critere.getLibelle());
        });
    }
}
