package org.mte.numecoeval.referentiel.infrastructure.configuration.security;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.ObjectPostProcessor;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.test.context.ActiveProfiles;

import java.util.HashMap;

import static org.mockito.Mockito.mock;

@SpringBootTest
@ActiveProfiles(profiles = {"test"})
class SecurityConfigTest {
    @Autowired
    private SecurityConfig securityConfig;

    @Test
    void testGlobalFilterChain() throws Exception {
        ObjectPostProcessor<Object> opp = mock(ObjectPostProcessor.class);
        AuthenticationProvider provider = mock(AuthenticationProvider.class);
        AuthenticationManagerBuilder builder = new AuthenticationManagerBuilder(opp);
        AuthenticationManagerBuilder authenticationBuilder = new AuthenticationManagerBuilder(opp);
        securityConfig.globalFilterChain(new HttpSecurity(opp, authenticationBuilder, new HashMap<>()));
        Assertions.assertNotNull(securityConfig);

    }
}

