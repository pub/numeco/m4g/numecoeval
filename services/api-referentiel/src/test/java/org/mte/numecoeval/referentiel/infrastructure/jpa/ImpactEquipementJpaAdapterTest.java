package org.mte.numecoeval.referentiel.infrastructure.jpa;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.domain.model.ImpactEquipement;
import org.mte.numecoeval.referentiel.domain.model.id.ImpactEquipementId;
import org.mte.numecoeval.referentiel.infrastructure.jpa.adapter.ImpactEquipementJpaAdapter;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.FacteurCaracterisationEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.repository.FacteurCaracterisationRepository;
import org.mte.numecoeval.referentiel.infrastructure.mapper.FacteurCaracterisationMapper;
import org.mte.numecoeval.referentiel.infrastructure.mapper.FacteurCaracterisationMapperImpl;
import org.mte.numecoeval.referentiel.infrastructure.mapper.ImpactEquipementMapper;
import org.mte.numecoeval.referentiel.infrastructure.mapper.ImpactEquipementMapperImpl;
import org.mte.numecoeval.referentiel.utils.Constants;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Collections;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;

class ImpactEquipementJpaAdapterTest {

    @InjectMocks
    private ImpactEquipementJpaAdapter jpaAdapter;

    @Mock
    FacteurCaracterisationRepository repository;
    FacteurCaracterisationMapper facteurCaracterisationMapper = new FacteurCaracterisationMapperImpl();

    ImpactEquipementMapper mapper = new ImpactEquipementMapperImpl();

    private static final String CRITERE = "Changement climatique";
    private static final String ETAPE = "UTILISATION";
    private static final String NOM = "Ecran 27 pouces";


    @BeforeEach
    void setup() {
        MockitoAnnotations.openMocks(this);
        ReflectionTestUtils.setField(jpaAdapter, "mapper", mapper);
        ReflectionTestUtils.setField(jpaAdapter, "facteurCaracterisationMapper", facteurCaracterisationMapper);

    }

    private FacteurCaracterisationEntity facteurCaracterisationEntity() {
        return new FacteurCaracterisationEntity()
                .setNom(NOM)
                .setEtape(ETAPE)
                .setCritere(CRITERE)
                .setNiveau(Constants.EQUIPEMENT_NIVEAU)
                .setSource("Test")
                .setConsoElecMoyenne(0.020)
                .setValeur(0.120);
    }

    private ImpactEquipement impactEquipement() {
        return new ImpactEquipement()
                .setRefEquipement(NOM)
                .setEtape(ETAPE)
                .setCritere(CRITERE)
                .setSource("Test")
                .setConsoElecMoyenne(0.020)
                .setValeur(0.120);
    }


    @Test
    void get_shouldReturnDomain() {
        var expectedEntity = facteurCaracterisationEntity();

        var wantedId = new ImpactEquipementId()
                .setRefEquipement(NOM)
                .setEtape(ETAPE)
                .setCritere(CRITERE);

        Mockito.when(repository.findByNomAndEtapeAndCritere(wantedId.getRefEquipement(), wantedId.getEtape(), wantedId.getCritere())).thenReturn(Optional.of(expectedEntity));

        var actualDomain = assertDoesNotThrow(() -> jpaAdapter.get(wantedId));

        assertNotNull(actualDomain.getEtape());
        assertEquals(expectedEntity.getEtape(), actualDomain.getEtape());
        assertNotNull(actualDomain.getCritere());
        assertEquals(expectedEntity.getCritere(), actualDomain.getCritere());
        assertEquals(expectedEntity.getNom(), actualDomain.getRefEquipement());
        assertEquals(expectedEntity.getConsoElecMoyenne(), actualDomain.getConsoElecMoyenne());
        assertEquals(expectedEntity.getValeur(), actualDomain.getValeur());
        assertEquals(expectedEntity.getSource(), actualDomain.getSource());
    }


    @Test
    void get_shouldThrowException() {
        var wantedId = new ImpactEquipementId()
                .setRefEquipement("NonExistant")
                .setEtape("Absent")
                .setCritere("Inexistant");

        Mockito.when(repository.findByNomAndEtapeAndCritere(wantedId.getRefEquipement(), wantedId.getEtape(), wantedId.getCritere())).thenReturn(Optional.empty());
        ReferentielException expectedException = assertThrows(ReferentielException.class, () -> jpaAdapter.get(wantedId));
        assertEquals("Impact équipement non trouvé pour l'id ImpactEquipementId(refEquipement=NonExistant, etape=Absent, critere=Inexistant)", expectedException.getMessage());
    }


    @Test
    void get_whenNull_shouldThrowException() {
        ReferentielException expectedException = assertThrows(ReferentielException.class, () -> jpaAdapter.get(null));
        assertEquals("Impact équipement non trouvé pour l'id null", expectedException.getMessage());
    }

    @Test
    void purge_shouldCallDeleteAll() {
        jpaAdapter.purge();
        Mockito.verify(repository, Mockito.times(1)).deleteByNiveau(Constants.EQUIPEMENT_NIVEAU);
    }

    @Test
    void getAll_shouldCallfindAll() {
        jpaAdapter.getAll();
        Mockito.verify(repository, Mockito.times(1)).findByNiveau(any());
    }

    @Test
    void saveAll_shouldCallsaveAll() {
        var domainToSave = impactEquipement();
        var entitiesToSave = facteurCaracterisationMapper.toEntities(mapper.toFacteurCaracterisations(Collections.singletonList(domainToSave)));

        assertDoesNotThrow(() -> jpaAdapter.saveAll(Collections.singletonList(domainToSave)));
        Mockito.verify(repository).saveAll(entitiesToSave);
        Mockito.verify(repository, Mockito.times(1)).saveAll(entitiesToSave);
    }

    @Test
    void save_shouldSaveAndReturnDomain() {
        var domain = impactEquipement();
        var actualEntity = facteurCaracterisationMapper.toEntity(mapper.toFacteurCaracterisation(domain));
        var expectedEntity = facteurCaracterisationEntity();

        assertEquals(expectedEntity.getNom(), actualEntity.getNom());
        assertEquals(expectedEntity.getNiveau(), actualEntity.getNiveau());

        Mockito.when(repository.save(actualEntity)).thenReturn(actualEntity);

        assertDoesNotThrow(() -> jpaAdapter.save(domain));
        Mockito.verify(repository).save(actualEntity);
    }

    @ParameterizedTest
    @NullSource
    void save_shouldSaveAndReturnNull(ImpactEquipement nullValue) {
        var expectedDomain = assertDoesNotThrow(() -> jpaAdapter.save(nullValue));
        Assertions.assertNull(expectedDomain);
    }
}
