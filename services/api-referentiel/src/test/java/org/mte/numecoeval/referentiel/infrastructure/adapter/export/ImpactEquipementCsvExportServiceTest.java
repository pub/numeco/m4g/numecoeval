package org.mte.numecoeval.referentiel.infrastructure.adapter.export;

import org.apache.commons.csv.CSVPrinter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mte.numecoeval.referentiel.domain.model.ImpactEquipement;
import org.mte.numecoeval.referentiel.domain.ports.input.impl.ImportImpactEquipementPortImpl;
import org.mte.numecoeval.referentiel.factory.TestDataFactory;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.FacteurCaracterisationEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.repository.FacteurCaracterisationRepository;
import org.mte.numecoeval.referentiel.infrastructure.mapper.FacteurCaracterisationMapper;
import org.mte.numecoeval.referentiel.infrastructure.mapper.FacteurCaracterisationMapperImpl;
import org.mte.numecoeval.referentiel.infrastructure.mapper.ImpactEquipementMapper;
import org.mte.numecoeval.referentiel.infrastructure.mapper.ImpactEquipementMapperImpl;
import org.mte.numecoeval.referentiel.utils.Constants;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

class ImpactEquipementCsvExportServiceTest {

    @InjectMocks
    ImpactEquipementCsvExportService exportService;

    @Mock
    FacteurCaracterisationRepository repository;
    FacteurCaracterisationMapper facteurCaracterisationMapper = new FacteurCaracterisationMapperImpl();
    ImpactEquipementMapper equipementMapper = new ImpactEquipementMapperImpl();

    @Mock
    CSVPrinter csvPrinter;

    @BeforeEach
    void setup() {
        MockitoAnnotations.openMocks(this);
        ReflectionTestUtils.setField(exportService, "facteurCaracterisationMapper", facteurCaracterisationMapper);
        ReflectionTestUtils.setField(exportService, "equipementMapper", equipementMapper);

    }

    private List<FacteurCaracterisationEntity> facteurCaracterisationEntities() {
        return List.of(
                TestDataFactory.FacteurCaracterisationFactory.entity("Laptop",
                        TestDataFactory.EtapeFactory.entity("UTILISATION", ""),
                        TestDataFactory.CritereFactory.entity("Changement climatique", "", ""),
                        "", Constants.EQUIPEMENT_NIVEAU, "", "Ecran", 0.1, "", 0.2, "", "NegaOctet", "ADEO"
                ),
                TestDataFactory.FacteurCaracterisationFactory.entity("Ecran",
                        TestDataFactory.EtapeFactory.entity("DISTRIBUTION", ""),
                        TestDataFactory.CritereFactory.entity("Changement climatique", "", ""),
                        "", Constants.EQUIPEMENT_NIVEAU, "", "Ecran", 0.01, "", 0.002, "", "", "ADEO"
                )
        );
    }

    @Test
    void getHeadersShouldReturnSameHeadersAsImport() {
        assertEquals(ImportImpactEquipementPortImpl.getHeaders(), exportService.getHeaders());
    }

    @Test
    void getObjectsToWriteShouldReturnRepositoryFindAll() {
        var entities = facteurCaracterisationEntities();

        when(repository.findByNiveau(Constants.EQUIPEMENT_NIVEAU)).thenReturn(entities);
        var actual = exportService.getObjectsToWrite();
        assertEquals(2, actual.size());
        assertEquals("DISTRIBUTION,UTILISATION", actual.stream().map(ImpactEquipement::getEtape).sorted().collect(Collectors.joining(",")));
    }

    @Test
    void printRecordShouldUseEntityAttributes() throws IOException {
        var entity = TestDataFactory.FacteurCaracterisationFactory.entity("Laptop",
                TestDataFactory.EtapeFactory.entity("UTILISATION", ""),
                TestDataFactory.CritereFactory.entity("Changement climatique", "", ""),
                "", Constants.EQUIPEMENT_NIVEAU, "", "Ecran", 0.1, "", 0.2, "", "NegaOctet", "ADEO"
        );

        DecimalFormat df = new DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale.ENGLISH));
        df.setMaximumFractionDigits(340);

        assertDoesNotThrow(() -> exportService.printRecord(csvPrinter, equipementMapper.toImpactEquipement(facteurCaracterisationMapper.toDomain(entity))));

        Mockito.verify(csvPrinter, times(1)).printRecord(entity.getNom(),
                entity.getEtape(), entity.getCritere(),
                df.format(entity.getConsoElecMoyenne()), df.format(entity.getValeur()),
                entity.getSource(), null);


    }

    @Test
    void logRecordErrorShouldLogSpecificErrorForRecord() {
        var entity = TestDataFactory.FacteurCaracterisationFactory.entity("Laptop",
                TestDataFactory.EtapeFactory.entity("UTILISATION", ""),
                TestDataFactory.CritereFactory.entity("Changement climatique", "", ""),
                "", Constants.EQUIPEMENT_NIVEAU, "", "Ecran", 0.1, "", 0.2, "", "NegaOctet", "ADEO"
        );
        assertDoesNotThrow(() -> exportService.logRecordError(equipementMapper.toImpactEquipement(facteurCaracterisationMapper.toDomain(entity)), new Exception("Test")));
    }

    @Test
    void logRecordErrorShouldLogGenericErrorForRecord() {
        assertDoesNotThrow(() -> exportService.logRecordError(null, new Exception("Test")));
    }

    @Test
    void logRecordErrorShouldLogGenericErrorForFile() {
        assertDoesNotThrow(() -> exportService.logWriterError(new Exception("Test")));
    }

    @Test
    void writeToCsvShouldReturnCSV() {
        // given
        var entities = facteurCaracterisationEntities();

        when(repository.findByNiveau(Constants.EQUIPEMENT_NIVEAU)).thenReturn(entities);
        StringWriter stringWriter = new StringWriter();

        // when
        exportService.writeToCsv(stringWriter);

        // Then
        String result = stringWriter.toString();
        assertEquals(
                "refEquipement;etapeacv;critere;consoElecMoyenne;valeur;source;type\r\n" +
                        "Laptop;UTILISATION;Changement climatique;0.1;0.2;NegaOctet;\r\n" +
                        "Ecran;DISTRIBUTION;Changement climatique;0.01;0.002;;\r\n",
                result
        );
    }
}
