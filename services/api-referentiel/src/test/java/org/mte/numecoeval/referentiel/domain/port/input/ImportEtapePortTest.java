package org.mte.numecoeval.referentiel.domain.port.input;

import org.junit.jupiter.api.Test;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.EtapeDTO;
import org.mte.numecoeval.referentiel.domain.ports.input.ImportCSVReferentielPort;
import org.mte.numecoeval.referentiel.domain.ports.input.impl.ImportEtapePortImpl;
import org.springframework.util.ResourceUtils;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

class ImportEtapePortTest {

    ImportCSVReferentielPort<EtapeDTO> importPortToTest = new ImportEtapePortImpl();

    @Test
    void importCSV_shouldImportAllDatas() throws Exception {
        File file = ResourceUtils.getFile("classpath:csv/unit/etapeACV.csv");
        var resultatImport = importPortToTest.importCSV(new FileInputStream(file));

        assertEquals(4, resultatImport.getNbrLignesImportees());
        assertEquals(0, resultatImport.getErreurs().size());
        assertEquals(4, resultatImport.getObjects().size());
        assertTrue(resultatImport.getObjects().stream().anyMatch(etapeDTO -> "UTILISATION".equals(etapeDTO.getCode()) && "Utilisation".equals(etapeDTO.getLibelle())));
        assertTrue(resultatImport.getObjects().stream().anyMatch(etapeDTO -> "FABRICATION".equals(etapeDTO.getCode()) && "Fabrication".equals(etapeDTO.getLibelle())));
        assertTrue(resultatImport.getObjects().stream().anyMatch(etapeDTO -> "DISTRIBUTION".equals(etapeDTO.getCode()) && "Distribution".equals(etapeDTO.getLibelle())));
        assertTrue(resultatImport.getObjects().stream().anyMatch(etapeDTO -> "FIN_DE_VIE".equals(etapeDTO.getCode()) && "Fin de vie".equals(etapeDTO.getLibelle())));
    }

    @Test
    void importCSV_whenErrorInMiddleOfFile_shouldImportDatasWithErrors() throws Exception {
        File file = ResourceUtils.getFile("classpath:csv/unit/etapeACV_errorInMiddle.csv");
        var resultatImport = importPortToTest.importCSV(new FileInputStream(file));

        assertEquals(3, resultatImport.getNbrLignesImportees());
        assertEquals(1, resultatImport.getErreurs().size());
        assertTrue(resultatImport.getErreurs().stream().anyMatch("La ligne n°4 est invalide : La colonne code ne peut être vide"::equals));
        assertEquals(3, resultatImport.getObjects().size());
        assertTrue(resultatImport.getObjects().stream().anyMatch(etapeDTO -> "FABRICATION".equals(etapeDTO.getCode()) && "Fabrication".equals(etapeDTO.getLibelle())));
        assertTrue(resultatImport.getObjects().stream().anyMatch(etapeDTO -> "DISTRIBUTION".equals(etapeDTO.getCode()) && "Distribution".equals(etapeDTO.getLibelle())));
        assertTrue(resultatImport.getObjects().stream().anyMatch(etapeDTO -> "FIN_DE_VIE".equals(etapeDTO.getCode()) && "Fin de vie".equals(etapeDTO.getLibelle())));
    }

    @Test
    void importCSV_whenWrongFile_shouldReturnOnlyErrors() throws Exception {
        File file = ResourceUtils.getFile("classpath:csv/unit/wrongCSVFile.csv");
        var resultatImport = importPortToTest.importCSV(new FileInputStream(file));

        assertEquals(0, resultatImport.getNbrLignesImportees());
        assertEquals(1, resultatImport.getErreurs().size());
        assertTrue(resultatImport.getErreurs().stream().anyMatch("La ligne n°2 est invalide : Entêtes incohérentes"::equals));
    }

    @Test
    void importCSV_whenStreamAlreadyClosedShouldReturnReportWithOneError() throws IOException {
        DataInputStream dataInputStream = mock(DataInputStream.class);
        doNothing().when(dataInputStream).close();

        var resultatImport = importPortToTest.importCSV(dataInputStream);

        verify(dataInputStream).close();
        assertEquals(0, resultatImport.getNbrLignesImportees());
        assertEquals(1, resultatImport.getErreurs().size());
        assertEquals("Le fichier CSV n'a pas pu être lu.", resultatImport.getErreurs().get(0));
    }

    @Test
    void importCSV_whenFileNotFoundShouldReturnReportWithOneError() throws IOException {
        DataInputStream dataInputStream = mock(DataInputStream.class);
        doThrow(new FileNotFoundException("start Read csv etape")).when(dataInputStream).close();

        var resultatImport = importPortToTest.importCSV(dataInputStream);

        verify(dataInputStream).close();
        assertEquals(0, resultatImport.getNbrLignesImportees());
        assertEquals(1, resultatImport.getErreurs().size());
        assertEquals("Le fichier CSV n'a pas pu être lu.", resultatImport.getErreurs().get(0));
    }
}
