package org.mte.numecoeval.referentiel.infrastructure.restapi.controller.correspondance;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.infrastructure.adapter.export.CorrespondanceRefEquipemenetCsvExportService;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.CorrespondanceRefEquipementDTO;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.id.CorrespondanceRefEquipementIdDTO;
import org.mte.numecoeval.referentiel.infrastructure.restapi.facade.CorrespondanceRefEquipementFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {ReferentielCorrespondanceRefEquipementRestApiImpl.class})
@ExtendWith(SpringExtension.class)
class ReferentielCorrespondanceRefEquipementRestApiImplTest {

    @Autowired
    private ReferentielCorrespondanceRefEquipementRestApiImpl referentielRestApi;

    @MockBean
    private CorrespondanceRefEquipementFacade referentielFacade;

    @MockBean
    private CorrespondanceRefEquipemenetCsvExportService csvExportService;

    @Test
    void get_shouldCallFacadeGetAndReturnCorrespondingDTO() throws ReferentielException {
        String modele = "modeleSource";
        String nomOrganisation = "nomOrga";
        CorrespondanceRefEquipementIdDTO idDTO = new CorrespondanceRefEquipementIdDTO().setModeleEquipementSource(modele).setNomOrganisation(nomOrganisation);
        var expectedDTO = CorrespondanceRefEquipementDTO.builder().modeleEquipementSource(modele).nomOrganisation(nomOrganisation).refEquipementCible("cible").build();

        when(referentielFacade.get(idDTO)).thenReturn(expectedDTO);
        var receivedDTO = referentielRestApi.get(modele, nomOrganisation);
        assertSame(expectedDTO, receivedDTO);
        verify(referentielFacade).get(idDTO);
    }

    @Test
    void importCSV_shouldCallPurgeByOrganisationAndAddAll() throws IOException, ReferentielException {
        doNothing().when(referentielFacade).purgeByOrganisationAndAddAll(any(), any());
        referentielRestApi.importCSV(new MockMultipartFile("Name", "AAAAAAAA".getBytes(StandardCharsets.UTF_8)), "nomOrga");
        verify(referentielFacade).purgeByOrganisationAndAddAll(any(), eq("nomOrga"));
    }

    @Test
    void importCSV_whenEmptyFileThenShouldThrowException() throws ReferentielException {
        doNothing().when(referentielFacade).purgeByOrganisationAndAddAll(any(), any());
        MockMultipartFile file = new MockMultipartFile("Name", (byte[]) null);
        ResponseStatusException responseStatusException = assertThrows(ResponseStatusException.class, () -> referentielRestApi.importCSV(file, "nomOrga"));
        assertEquals(HttpStatus.BAD_REQUEST, responseStatusException.getStatusCode());
        assertEquals("Le fichier n'existe pas ou alors il est vide", responseStatusException.getReason());
    }

    @Test
    void importCSV_whenNullFileThenShouldThrowException() throws ReferentielException {
        doNothing().when(referentielFacade).purgeByOrganisationAndAddAll(any(), any());
        ResponseStatusException responseStatusException = assertThrows(ResponseStatusException.class, () -> referentielRestApi.importCSV(null, "nomOrga"));
        assertEquals(HttpStatus.BAD_REQUEST, responseStatusException.getStatusCode());
        assertEquals("Le fichier n'existe pas ou alors il est vide", responseStatusException.getReason());
    }

    @Test
    void exportCSV_shouldReturnCSVFile() {
        var servletResponse = new MockHttpServletResponse();

        assertDoesNotThrow(() -> referentielRestApi.exportCSV(servletResponse));

        assertEquals("text/csv;charset=UTF-8", servletResponse.getContentType());
        String headerContentDisposition = servletResponse.getHeader("Content-Disposition");
        assertNotNull(headerContentDisposition);
        assertTrue(headerContentDisposition.contains("attachment; filename=correspondancesRefEquipement-"));
        assertEquals(200, servletResponse.getStatus());

    }
}
