package org.mte.numecoeval.referentiel.infrastructure.configuration.openapi;

import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.SpecVersion;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class ReferentielOpenApiConfigTest {
    @Test
    void testConfigOpenAPI() {
        OpenAPI actualConfigOpenAPIResult = (new ReferentielOpenApiConfig()).configOpenAPI();
        assertNull(actualConfigOpenAPIResult.getComponents());
        assertNull(actualConfigOpenAPIResult.getWebhooks());
        assertNull(actualConfigOpenAPIResult.getTags());
        assertEquals(SpecVersion.V30, actualConfigOpenAPIResult.getSpecVersion());
        assertNull(actualConfigOpenAPIResult.getServers());
        assertNull(actualConfigOpenAPIResult.getSecurity());
        assertNull(actualConfigOpenAPIResult.getPaths());
        assertEquals("3.0.1", actualConfigOpenAPIResult.getOpenapi());
        assertNull(actualConfigOpenAPIResult.getExtensions());
        ExternalDocumentation externalDocs = actualConfigOpenAPIResult.getExternalDocs();
        assertEquals("https://gitlab-forge.din.developpement-durable.gouv.fr/pub/numeco/m4g/docs", externalDocs.getUrl());
        assertNull(externalDocs.getExtensions());
        assertEquals("NumEcoEval Documentation", externalDocs.getDescription());
        Info info = actualConfigOpenAPIResult.getInfo();
        assertEquals("API des référentiels de NumEcoEval", info.getTitle());
        assertNull(info.getTermsOfService());
        assertNull(info.getSummary());
        assertNull(info.getExtensions());
        assertEquals("v0.0.1", info.getVersion());
        assertEquals("""
                                Endpoints permettant de manipuler les référentiels de NumEcoEval.
                                Les endpoints CRUD sont générés via Spring DataRest.
                                
                                Les endpoints d'export CSV permettent d'exporter l'intégralité d'un référentiel
                                sous forme de fichier CSV ré-importable via les endpoints d'imports.
                                
                                Les endpoints d'import fonctionnent en annule & remplace et supprimeront l'intégralité
                                du référentiel et utiliseront le contenu du CSV pour peupler le référentiel.
                                
                                Les endpoints internes sont utilisés par les différents modules de NumEcoEval.
                                """,
                info.getDescription());
        assertNull(info.getContact());
        License license = info.getLicense();
        assertEquals("https://gitlab-forge.din.developpement-durable.gouv.fr/pub/numeco/m4g/api-referentiel/-/blob/main/LICENSE.txt", license.getUrl());
        assertEquals("Apache 2.0", license.getName());
    }
}

