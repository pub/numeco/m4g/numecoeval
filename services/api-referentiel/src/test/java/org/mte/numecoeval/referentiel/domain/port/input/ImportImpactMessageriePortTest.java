package org.mte.numecoeval.referentiel.domain.port.input;

import org.junit.jupiter.api.Test;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.ImpactMessagerieDTO;
import org.mte.numecoeval.referentiel.domain.ports.input.ImportCSVReferentielPort;
import org.mte.numecoeval.referentiel.domain.ports.input.impl.ImportImpactMessageriePortImpl;
import org.springframework.util.ResourceUtils;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

class ImportImpactMessageriePortTest {

    ImportCSVReferentielPort<ImpactMessagerieDTO> importPortToTest = new ImportImpactMessageriePortImpl();

    @Test
    void importCSV_shouldImportAllDatas() throws Exception {
        File file = ResourceUtils.getFile("classpath:csv/unit/impactMessagerie.csv");
        var resultatImport = importPortToTest.importCSV(new FileInputStream(file));

        assertEquals(5, resultatImport.getNbrLignesImportees());
        assertEquals(0, resultatImport.getErreurs().size());
        assertEquals(5, resultatImport.getObjects().size());
        assertTrue(resultatImport.getObjects().stream().anyMatch(dto ->
                "Changement climatique".equals(dto.getCritere())
                        && Double.valueOf(0.5).equals(dto.getConstanteCoefficientDirecteur())
                        && Double.valueOf(0).equals(dto.getConstanteOrdonneeOrigine())
                        && "Tests".equals(dto.getSource())
                )
        );
        assertTrue(resultatImport.getObjects().stream().anyMatch(dto ->
                "Émissions de particules fines".equals(dto.getCritere())
                        && Double.valueOf(1.000111).equals(dto.getConstanteCoefficientDirecteur())
                        && Double.valueOf(1.451).equals(dto.getConstanteOrdonneeOrigine())
                        && "Tests".equals(dto.getSource())
                )
        );
        assertTrue(resultatImport.getObjects().stream().anyMatch(dto ->
                "Radiations ionisantes".equals(dto.getCritere())
                        && Double.valueOf(0.00005).equals(dto.getConstanteCoefficientDirecteur())
                        && Double.valueOf(0.02315412).equals(dto.getConstanteOrdonneeOrigine())
                        && "Tests".equals(dto.getSource())
                )
        );
        assertTrue(resultatImport.getObjects().stream().anyMatch(dto ->
                "Acidification".equals(dto.getCritere())
                        && Double.valueOf(1.224586).equals(dto.getConstanteCoefficientDirecteur())
                        && Double.valueOf(0.042).equals(dto.getConstanteOrdonneeOrigine())
                        && "Tests".equals(dto.getSource())
                )
        );
        assertTrue(resultatImport.getObjects().stream().anyMatch(dto ->
                "Épuisement des ressources naturelles (minérales et métaux)".equals(dto.getCritere())
                        && Double.valueOf(2.020).equals(dto.getConstanteCoefficientDirecteur())
                        && Double.valueOf(0.678).equals(dto.getConstanteOrdonneeOrigine())
                        && "Tests".equals(dto.getSource())
                )
        );
    }

    @Test
    void importCSV_whenErrorInMiddleOfFile_shouldImportDatasWithErrors() throws Exception {
        File file = ResourceUtils.getFile("classpath:csv/unit/impactMessagerie_errorInMiddle.csv");
        var resultatImport = importPortToTest.importCSV(new FileInputStream(file));

        assertEquals(2, resultatImport.getNbrLignesImportees());
        assertEquals(1, resultatImport.getErreurs().size());
        assertTrue(resultatImport.getErreurs().stream().anyMatch("La ligne n°3 est invalide : La colonne critere ne peut être vide"::equals));
        assertEquals(2, resultatImport.getObjects().size());
    }

    @Test
    void importCSV_whenWrongFile_shouldReturnOnlyErrors() throws Exception {
        File file = ResourceUtils.getFile("classpath:csv/unit/wrongCSVFile.csv");
        var resultatImport = importPortToTest.importCSV(new FileInputStream(file));

        assertEquals(0, resultatImport.getNbrLignesImportees());
        assertEquals(1, resultatImport.getErreurs().size());
        assertTrue(resultatImport.getErreurs().stream().anyMatch("La ligne n°2 est invalide : Entêtes incohérentes"::equals));
    }

    @Test
    void importCSV_whenStreamAlreadyClosedShouldReturnReportWithOneError() throws IOException {
        DataInputStream dataInputStream = mock(DataInputStream.class);
        doNothing().when(dataInputStream).close();

        var resultatImport = importPortToTest.importCSV(dataInputStream);

        verify(dataInputStream).close();
        assertEquals(0, resultatImport.getNbrLignesImportees());
        assertEquals(1, resultatImport.getErreurs().size());
        assertEquals("Le fichier CSV n'a pas pu être lu.", resultatImport.getErreurs().get(0));
    }

    @Test
    void importCSV_whenFileNotFoundShouldReturnReportWithOneError() throws IOException {
        DataInputStream dataInputStream = mock(DataInputStream.class);
        doThrow(new FileNotFoundException("start Read csv etape")).when(dataInputStream).close();

        var resultatImport = importPortToTest.importCSV(dataInputStream);

        verify(dataInputStream).close();
        assertEquals(0, resultatImport.getNbrLignesImportees());
        assertEquals(1, resultatImport.getErreurs().size());
        assertEquals("Le fichier CSV n'a pas pu être lu.", resultatImport.getErreurs().get(0));
    }
}
