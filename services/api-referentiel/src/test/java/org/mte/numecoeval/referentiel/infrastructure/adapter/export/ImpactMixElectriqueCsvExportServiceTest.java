package org.mte.numecoeval.referentiel.infrastructure.adapter.export;

import org.apache.commons.csv.CSVPrinter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mte.numecoeval.referentiel.domain.model.MixElectrique;
import org.mte.numecoeval.referentiel.domain.ports.input.impl.ImportMixElectriquePortImpl;
import org.mte.numecoeval.referentiel.factory.TestDataFactory;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.FacteurCaracterisationEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.repository.FacteurCaracterisationRepository;
import org.mte.numecoeval.referentiel.infrastructure.mapper.FacteurCaracterisationMapper;
import org.mte.numecoeval.referentiel.infrastructure.mapper.FacteurCaracterisationMapperImpl;
import org.mte.numecoeval.referentiel.infrastructure.mapper.MixElectriqueMapper;
import org.mte.numecoeval.referentiel.infrastructure.mapper.MixElectriqueMapperImpl;
import org.mte.numecoeval.referentiel.utils.Constants;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

class ImpactMixElectriqueCsvExportServiceTest {

    @InjectMocks
    MixElectriqueCsvExportService exportService;

    @Mock
    FacteurCaracterisationRepository repository;
    FacteurCaracterisationMapper facteurCaracterisationMapper = new FacteurCaracterisationMapperImpl();
    MixElectriqueMapper mixElectriqueMapper = new MixElectriqueMapperImpl();
    @Mock
    CSVPrinter csvPrinter;

    @BeforeEach
    void setup() {
        MockitoAnnotations.openMocks(this);
        ReflectionTestUtils.setField(exportService, "facteurCaracterisationMapper", facteurCaracterisationMapper);
        ReflectionTestUtils.setField(exportService, "mixElectriqueMapper", mixElectriqueMapper);
    }

    private List<FacteurCaracterisationEntity> facteurCaracterisationEntities() {
        return List.of(
                TestDataFactory.FacteurCaracterisationFactory.entity("Electricity mix FR",
                        TestDataFactory.EtapeFactory.entity("FABRICATION", ""),
                        TestDataFactory.CritereFactory.entity("Changement climatique", "", ""),
                        "", Constants.MIXELEC_NIVEAU, "", Constants.MIXELEC_CATEGORIE, 0.0, "France", 0.2, "", "NegaOctet", "ADEO"
                ),
                TestDataFactory.FacteurCaracterisationFactory.entity("Electricity mix ES",
                        TestDataFactory.EtapeFactory.entity("FABRICATION", ""),
                        TestDataFactory.CritereFactory.entity("Changement climatique", "", ""),
                        "", Constants.MIXELEC_NIVEAU, "", Constants.MIXELEC_CATEGORIE, 0.0, "Espagne", 0.1, "", "", "ADEO"
                )
        );
    }

    @Test
    void getHeadersShouldReturnSameHeadersAsImport() {
        Assertions.assertEquals(ImportMixElectriquePortImpl.getHeaders(), exportService.getHeaders());
    }

    @Test
    void getObjectsToWriteShouldReturnRepositoryFindAll() {
        var entities = facteurCaracterisationEntities();
        when(repository.findByNiveauAndCategorie(Constants.MIXELEC_NIVEAU, Constants.MIXELEC_CATEGORIE)).thenReturn(entities);
        var actual = exportService.getObjectsToWrite();
        assertEquals(2, actual.size());
        assertEquals("Espagne,France", actual.stream().map(MixElectrique::getPays).sorted().collect(Collectors.joining(",")));
    }

    @Test
    void printRecordShouldUseEntityAttributes() throws IOException {
        var entity = TestDataFactory.FacteurCaracterisationFactory.entity("Electricity mix FR",
                TestDataFactory.EtapeFactory.entity("FABRICATION", ""),
                TestDataFactory.CritereFactory.entity("Changement climatique", "", ""),
                "", Constants.MIXELEC_NIVEAU, "", Constants.MIXELEC_CATEGORIE, 0.0, "France", 0.2, "", "NegaOctet", "ADEO"
        );

        DecimalFormat df = new DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale.ENGLISH));
        df.setMaximumFractionDigits(340);

        assertDoesNotThrow(() -> exportService.printRecord(csvPrinter, mixElectriqueMapper.toMixElectrique(facteurCaracterisationMapper.toDomain(entity))));

        Mockito.verify(csvPrinter, times(1)).printRecord(
                entity.getLocalisation(),
                null,
                entity.getCritere(),
                df.format(entity.getValeur()),
                entity.getSource());
    }

    @Test
    void logRecordErrorShouldLogSpecificErrorForRecord() {
        var entity = TestDataFactory.FacteurCaracterisationFactory.entity("Electricity mix FR",
                TestDataFactory.EtapeFactory.entity("FABRICATION", ""),
                TestDataFactory.CritereFactory.entity("Changement climatique", "", ""),
                "", Constants.MIXELEC_NIVEAU, "", Constants.MIXELEC_CATEGORIE, 0.0, "France", 0.2, "", "NegaOctet", "ADEO"
        );

        assertDoesNotThrow(() -> exportService.logRecordError(mixElectriqueMapper.toMixElectrique(facteurCaracterisationMapper.toDomain(entity)), new Exception("Test")));
    }

    @Test
    void logRecordErrorShouldLogGenericErrorForRecord() {
        assertDoesNotThrow(() -> exportService.logRecordError(null, new Exception("Test")));
    }

    @Test
    void logRecordErrorShouldLogGenericErrorForFile() {
        assertDoesNotThrow(() -> exportService.logWriterError(new Exception("Test")));
    }

    @Test
    void writeToCsvShouldReturnCSV() {
        // given
        var entities = facteurCaracterisationEntities();
        when(repository.findByNiveauAndCategorie(Constants.MIXELEC_NIVEAU, Constants.MIXELEC_CATEGORIE)).thenReturn(entities);
        StringWriter stringWriter = new StringWriter();

        // when
        exportService.writeToCsv(stringWriter);

        // Then
        String result = stringWriter.toString();
        assertEquals(
                "pays;raccourcisAnglais;critere;valeur;source\r\n" +
                        "France;;Changement climatique;0.2;NegaOctet\r\n" +
                        "Espagne;;Changement climatique;0.1;\r\n",
                result
        );
    }
}
