package org.mte.numecoeval.referentiel.infrastructure.restapi.facade;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.domain.model.CorrespondanceRefEquipement;
import org.mte.numecoeval.referentiel.domain.model.id.CorrespondanceRefEquipementId;
import org.mte.numecoeval.referentiel.domain.ports.output.ReferentielPersistencePort;
import org.mte.numecoeval.referentiel.factory.TestDataFactory;
import org.mte.numecoeval.referentiel.infrastructure.mapper.CorrespondanceRefEquipementMapper;
import org.mte.numecoeval.referentiel.infrastructure.mapper.CorrespondanceRefEquipementMapperImpl;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

class CorrespondanceRefEquipementFacadeTest {

    @InjectMocks
    CorrespondanceRefEquipementFacade facadeToTest;

    @Mock
    private ReferentielPersistencePort<CorrespondanceRefEquipement, CorrespondanceRefEquipementId> persistencePort;

    private CorrespondanceRefEquipementMapper mapper = new CorrespondanceRefEquipementMapperImpl();

    @BeforeEach
    void setup() {
        MockitoAnnotations.openMocks(this);
        ReflectionTestUtils.setField(facadeToTest, "mapper", mapper);
    }

    @Test
    void get_shouldReturnMatchingDTO() throws ReferentielException {
        var wantedDTOId = TestDataFactory.CorrespondanceRefEquipementFactory.idDTO("modeleSource", "ADEO");
        var expectedDomainID = TestDataFactory.CorrespondanceRefEquipementFactory.idDomain("modeleSource", "ADEO");
        var expectedDomain = TestDataFactory.CorrespondanceRefEquipementFactory.domain("modeleSource", "refCible", "ADEO");

        Mockito.when(persistencePort.get(expectedDomainID)).thenReturn(expectedDomain);

        var result = Assertions.assertDoesNotThrow(() -> facadeToTest.get(wantedDTOId));

        assertEquals(expectedDomain.getModeleEquipementSource(), result.getModeleEquipementSource());
        assertEquals(expectedDomain.getRefEquipementCible(), result.getRefEquipementCible());
        assertEquals(expectedDomain.getNomOrganisation(), result.getNomOrganisation());
    }

    @Test
    void get_withNonMachingDTOShouldReturnNull() throws ReferentielException {

        var wantedDTOId = TestDataFactory.CorrespondanceRefEquipementFactory.idDTO("modeleSource", "ADEO");
        var expectedDomainID = TestDataFactory.CorrespondanceRefEquipementFactory.idDomain("modeleSource", "ADEO");
        Mockito.when(persistencePort.get(expectedDomainID)).thenReturn(null);

        var result = Assertions.assertDoesNotThrow(() -> facadeToTest.get(wantedDTOId));

        assertNull(result);
    }

    @Test
    void getAll_shouldReturnMatchingAllDTOs() throws ReferentielException {
        var expectedDomains = Arrays.asList(
                TestDataFactory.CorrespondanceRefEquipementFactory.domain("modele", "refCible", "ADEO"),
                TestDataFactory.CorrespondanceRefEquipementFactory.domain("modele2", "refCible", "ADEO")
        );

        Mockito.when(persistencePort.getAll()).thenReturn(expectedDomains);

        var result = Assertions.assertDoesNotThrow(() -> facadeToTest.getAll());

        assertEquals(2, result.size());
    }

    @Test
    void purgeByOrganisationAndAddAll_ShouldCallPurgeThenSaveAll() throws ReferentielException {
        var dtosToSave = Arrays.asList(
                TestDataFactory.CorrespondanceRefEquipementFactory.dto(
                        "modele01", "refCible", "ADEO"
                ),
                TestDataFactory.CorrespondanceRefEquipementFactory.dto(
                        "modele02", "refCible", "ADEO"
                )
        );
        ArgumentCaptor<Collection<CorrespondanceRefEquipement>> valueCapture = ArgumentCaptor.forClass(Collection.class);

        assertDoesNotThrow(() -> facadeToTest.purgeByOrganisationAndAddAll(dtosToSave, "ADEO"));

        Mockito.verify(persistencePort, Mockito.times(1)).purgeByOrganisation("ADEO");
        Mockito.verify(persistencePort, Mockito.times(1)).saveAll(valueCapture.capture());

        var expectedDomains = valueCapture.getValue();
        assertNotNull(expectedDomains);
        assertEquals(dtosToSave.size(), expectedDomains.size());
        dtosToSave.forEach(dto -> {
            var matchingDomain = expectedDomains.stream()
                    .filter(domain -> dto.getModeleEquipementSource().equals(domain.getModeleEquipementSource()))
                    .findAny();
            assertTrue(matchingDomain.isPresent());
            var domain = matchingDomain.get();

            assertEquals(dto.getModeleEquipementSource(), domain.getModeleEquipementSource());
            assertEquals(dto.getRefEquipementCible(), domain.getRefEquipementCible());
        });
    }
}
