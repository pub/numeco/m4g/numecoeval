package org.mte.numecoeval.referentiel.infrastructure.csv;

import org.apache.commons.csv.CSVRecord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.mockito.MockitoAnnotations;
import org.mte.numecoeval.referentiel.domain.data.ResultatImport;
import org.mte.numecoeval.referentiel.domain.ports.input.ImportCSVReferentielPort;

import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ImportCSVReferentielPortTest {

    CSVRecord csvRecord;
    ImportCSVReferentielPort csvReferentielPort = new ImportCSVReferentielPort() {

        @Override
        public ResultatImport importCSV(InputStream csvInputStream) {
            return new ResultatImport<>();
        }
    };

    @BeforeEach
    void setup() {
        MockitoAnnotations.openMocks(this);
        csvRecord = mock(CSVRecord.class);
    }

    @Test
    void getDoubleValue_shouldReturnRecordValue() {
        when(csvRecord.get("doubleValue")).thenReturn("1.6");

        Double value = csvReferentielPort.getDoubleValueFromRecord(csvRecord, "doubleValue", null);

        assertEquals(1.6, value);
    }

    @Test
    void getDoubleValue_givenUntrimmedValue_shouldReturnRecordValue() {
        when(csvRecord.get("doubleValue")).thenReturn("            97.58              ");

        Double value = csvReferentielPort.getDoubleValueFromRecord(csvRecord, "doubleValue", null);

        assertEquals(97.58, value);
    }

    @ParameterizedTest
    @NullAndEmptySource
    void getDoubleValue_shouldReturnDefaultValue(String nullOrEmptyValue) {
        when(csvRecord.get("doubleValue")).thenReturn(nullOrEmptyValue);

        Double value = csvReferentielPort.getDoubleValueFromRecord(csvRecord, "doubleValue", 0.0);

        assertEquals(0.0, value);
    }
}

