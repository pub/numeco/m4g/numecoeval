package org.mte.numecoeval.referentiel.factory;

import org.mte.numecoeval.referentiel.domain.model.*;
import org.mte.numecoeval.referentiel.domain.model.id.*;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.*;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.id.*;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.*;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.id.*;

public class TestDataFactory {

    public static final String DEFAULT_ETAPE = "UTILISATION";

    public static final String DEFAULT_CRITERE = "Changement Climatique";
    public static final String DEFAULT_UNITE = "kg CO² eq";

    public static class CritereFactory {
        public static CritereDTO dto(String nomCritere, String unite, String description) {
            return new CritereDTO(nomCritere, unite, description);
        }

        public static Critere domain(String nomCritere, String description, String unite) {
            return new Critere()
                    .setNomCritere(nomCritere)
                    .setDescription(description)
                    .setUnite(unite);
        }

        public static CritereEntity entity(String nomCritere, String description, String unite) {
            return new CritereEntity()
                    .setNomCritere(nomCritere)
                    .setDescription(description)
                    .setUnite(unite);
        }

        public static CritereIdDTO idDTO(String nomCritere) {
            return new CritereIdDTO(nomCritere);
        }

        public static CritereId idDomain(String nomCritere) {
            return new CritereId()
                    .setNomCritere(nomCritere);
        }

        public static CritereIdEntity idEntity(String nomCritere) {
            return new CritereIdEntity()
                    .setNomCritere(nomCritere);
        }
    }

    public static class EtapeFactory {

        public static EtapeDTO dto(String code, String libelle) {
            return new EtapeDTO(code, libelle);
        }

        public static Etape domain(String code, String libelle) {
            return new Etape()
                    .setCode(code)
                    .setLibelle(libelle);
        }

        public static EtapeEntity entity(String code, String libelle) {
            return new EtapeEntity()
                    .setCode(code)
                    .setLibelle(libelle);
        }

        public static EtapeIdDTO idDTO(String code) {
            return new EtapeIdDTO(code);
        }

        public static EtapeId idDomain(String code) {
            return new EtapeId()
                    .setCode(code);
        }

        public static EtapeIdEntity idEntity(String code) {
            return new EtapeIdEntity()
                    .setCode(code);
        }
    }

    public static class HypotheseFactory {
        public static HypotheseDTO dto(String code, String valeur, String source, String description, String nomOrganisation) {
            return new HypotheseDTO(code, valeur, source, description, nomOrganisation);
        }

        public static Hypothese domain(String code, String valeur, String source, String description, String nomOrganisation) {
            return new Hypothese()
                    .setCode(code)
                    .setValeur(valeur)
                    .setSource(source)
                    .setDescription(description)
                    .setNomOrganisation(nomOrganisation);
        }

        public static HypotheseEntity entity(String code, String valeur, String source, String description, String nomOrganisation) {
            return new HypotheseEntity()
                    .setCode(code)
                    .setValeur(valeur)
                    .setSource(source)
                    .setDescription(description)
                    .setNomOrganisation(nomOrganisation);
        }

        public static HypotheseIdDTO idDTO(String code, String nomOrganisation) {
            return new HypotheseIdDTO()
                    .setCode(code)
                    .setNomOrganisation(nomOrganisation);
        }

        public static HypotheseId idDomain(String code, String nomOrganisation) {
            return new HypotheseId()
                    .setCode(code)
                    .setNomOrganisation(nomOrganisation);
        }

        public static HypotheseIdEntity idEntity(String code, String nomOrganisation) {
            return new HypotheseIdEntity()
                    .setCode(code)
                    .setNomOrganisation(nomOrganisation);
        }
    }

    public static class TypeEquipementFactory {
        public static TypeEquipementDTO dto(String type, boolean estUnServeur, Double dureeVieDefaut, String commentaire, String source, String refEquipementParDefaut) {
            return TypeEquipementDTO.builder()
                    .type(type)
                    .serveur(estUnServeur)
                    .dureeVieDefaut(dureeVieDefaut)
                    .source(source)
                    .commentaire(commentaire)
                    .refEquipementParDefaut(refEquipementParDefaut)
                    .build();
        }

        public static TypeEquipement domain(String type, boolean estUnServeur, Double dureeVieDefaut, String commentaire, String source, String refEquipementParDefaut) {
            return TypeEquipement.builder()
                    .type(type)
                    .serveur(estUnServeur)
                    .dureeVieDefaut(dureeVieDefaut)
                    .source(source)
                    .commentaire(commentaire)
                    .refEquipementParDefaut(refEquipementParDefaut)
                    .build();
        }

    }

    public static class TypeItemFactory {
        public static TypeItemDTO dto(String type, String categorie, boolean estUnServeur, String commentaire, Double dureeVieDefaut, String refHypothese, String source, String refItemParDefaut, String nomOrganisation) {
            return TypeItemDTO.builder()
                    .type(type)
                    .categorie(categorie)
                    .serveur(estUnServeur)
                    .dureeVieDefaut(dureeVieDefaut)
                    .refHypothese(refHypothese)
                    .source(source)
                    .commentaire(commentaire)
                    .refItemParDefaut(refItemParDefaut)
                    .nomOrganisation(nomOrganisation)
                    .build();
        }

        public static TypeItem domain(String type, String categorie, boolean estUnServeur, String commentaire, Double dureeVieDefaut, String refHypothese, String source, String refItemParDefaut, String nomOrganisation) {
            return TypeItem.builder()
                    .type(type)
                    .categorie(categorie)
                    .serveur(estUnServeur)
                    .dureeVieDefaut(dureeVieDefaut)
                    .refHypothese(refHypothese)
                    .source(source)
                    .commentaire(commentaire)
                    .refItemParDefaut(refItemParDefaut)
                    .nomOrganisation(nomOrganisation)
                    .build();
        }

        public static TypeItemEntity entity(String type, String categorie, boolean estUnServeur, String commentaire, Double dureeVieDefaut, String refHypothese, String source, String refItemParDefaut, String nomOrganisation) {
            return new TypeItemEntity(type, nomOrganisation, categorie, estUnServeur, commentaire, dureeVieDefaut, refHypothese, source, refItemParDefaut);
        }

        public static TypeItemIdDTO idDTO(String type, String nomOrganisation) {
            return new TypeItemIdDTO()
                    .setType(type)
                    .setNomOrganisation(nomOrganisation);
        }

        public static TypeItemId idDomain(String type, String nomOrganisation) {
            return new TypeItemId()
                    .setType(type)
                    .setNomOrganisation(nomOrganisation);
        }

        public static TypeItemIdEntity idEntity(String type, String nomOrganisation) {
            return new TypeItemIdEntity()
                    .setType(type)
                    .setNomOrganisation(nomOrganisation);
        }
    }


    public static class MixElectriqueFactory {

        public static MixElectriqueDTO dto(CritereDTO critere, String pays, String raccourcisAnglais, Double valeur, String source) {
            return new MixElectriqueDTO(
                    pays,
                    raccourcisAnglais,
                    critere.getNomCritere(),
                    valeur,
                    source
            );
        }

        public static MixElectrique domain(Critere critere, String pays, String raccourcisAnglais, Double valeur, String source) {
            return new MixElectrique()
                    .setCritere(critere.getNomCritere())
                    .setPays(pays)
                    .setRaccourcisAnglais(raccourcisAnglais)
                    .setValeur(valeur)
                    .setSource(source);
        }

        public static MixElectriqueIdDTO idDTO(CritereIdDTO critereId, String pays) {
            return MixElectriqueIdDTO
                    .builder()
                    .critere(critereId.getNomCritere())
                    .pays(pays)
                    .build();
        }

        public static MixElectriqueId idDomain(CritereId critereId, String pays) {
            return new MixElectriqueId()
                    .setCritere(critereId.getNomCritere())
                    .setPays(pays);
        }
    }

    public static class ImpactEquipementFactory {

        public static ImpactEquipementDTO dto(String etape, String critere, String refEquipement, String source, String type, Double valeur, Double consoElecMoyenne, String description) {
            return new ImpactEquipementDTO(
                    refEquipement,
                    etape,
                    critere,
                    source,
                    type,
                    valeur,
                    consoElecMoyenne,
                    description
            );
        }

        public static ImpactEquipement domain(Etape etape, Critere critere, String refEquipement, String source, String type, Double valeur, Double consoElecMoyenne) {
            return new ImpactEquipement()
                    .setEtape(etape.getCode())
                    .setCritere(critere.getNomCritere())
                    .setRefEquipement(refEquipement)
                    .setSource(source)
                    .setType(type)
                    .setValeur(valeur)
                    .setConsoElecMoyenne(consoElecMoyenne);
        }

        public static ImpactEquipementIdDTO idDTO(String etapeIdDTO, String critereId, String refEquipement) {
            return ImpactEquipementIdDTO
                    .builder()
                    .etape(etapeIdDTO)
                    .critere(critereId)
                    .refEquipement(refEquipement)
                    .build();
        }

        public static ImpactEquipementId idDomain(String etapeId, String critereId, String refEquipement) {
            return new ImpactEquipementId()
                    .setEtape(etapeId)
                    .setCritere(critereId)
                    .setRefEquipement(refEquipement);
        }

    }

    public static class FacteurCaracterisationFactory {

        public static FacteurCaracterisationDTO dto(String nom, String etape, String critere, String description, String niveau, String tiers, String categorie, Double consoElecMoyenne, String localisation, Double valeur, String unite, String source, String nomOrganisation) {
            return new FacteurCaracterisationDTO(
                    nom,
                    etape,
                    critere,
                    description,
                    niveau,
                    tiers,
                    categorie,
                    consoElecMoyenne,
                    localisation,
                    valeur,
                    unite,
                    source,
                    nomOrganisation
            );
        }

        public static FacteurCaracterisation domain(String nom, Etape etape, Critere critere, String description, String niveau, String tiers, String categorie, Double consoElecMoyenne, String localisation, Double valeur, String unite, String source, String nomOrganisation) {
            return new FacteurCaracterisation()
                    .setNom(nom)
                    .setEtape(etape.getCode())
                    .setCritere(critere.getNomCritere())
                    .setDescription(description)
                    .setNiveau(niveau)
                    .setTiers(tiers)
                    .setCategorie(categorie)
                    .setConsoElecMoyenne(consoElecMoyenne)
                    .setLocalisation(localisation)
                    .setValeur(valeur)
                    .setUnite(unite)
                    .setSource(source)
                    .setNomOrganisation(nomOrganisation);
        }

        public static FacteurCaracterisationEntity entity(String nom, EtapeEntity etape, CritereEntity critere, String description, String niveau, String tiers, String categorie, Double consoElecMoyenne, String localisation, Double valeur, String unite, String source, String nomOrganisation) {
            return new FacteurCaracterisationEntity()
                    .setNom(nom)
                    .setEtape(etape.getCode())
                    .setCritere(critere.getNomCritere())
                    .setDescription(description)
                    .setNiveau(niveau)
                    .setTiers(tiers)
                    .setCategorie(categorie)
                    .setConsoElecMoyenne(consoElecMoyenne)
                    .setLocalisation(localisation)
                    .setValeur(valeur)
                    .setUnite(unite)
                    .setSource(source)
                    .setNomOrganisation(nomOrganisation);
        }

        public static FacteurCaracterisationIdDTO idDTO(String nom, String etapeIdDTO, String critereId, String nomOrganisation) {
            return FacteurCaracterisationIdDTO
                    .builder()
                    .nom(nom)
                    .etape(etapeIdDTO)
                    .critere(critereId)
                    .nomOrganisation(nomOrganisation)
                    .build();
        }

        public static FacteurCaracterisationId idDomain(String nom, String etapeId, String critereId, String nomOrganisation) {
            return new FacteurCaracterisationId()
                    .setNom(nom)
                    .setEtape(etapeId)
                    .setCritere(critereId)
                    .setNomOrganisation(nomOrganisation);
        }
    }

    public static class ImpactMessagerieFactory {

        public static ImpactMessagerieDTO dto(CritereDTO critere, Double constanteCoefficientDirecteur, Double constanteOrdonneeOrigine, String source) {
            return ImpactMessagerieDTO.builder()
                    .critere(critere.getNomCritere())
                    .source(source)
                    .constanteCoefficientDirecteur(constanteCoefficientDirecteur)
                    .constanteOrdonneeOrigine(constanteOrdonneeOrigine)
                    .build();
        }

        public static ImpactMessagerie domain(Critere critere, Double constanteCoefficientDirecteur, Double constanteOrdonneeOrigine, String source) {
            return new ImpactMessagerie()
                    .setCritere(critere.getNomCritere())
                    .setSource(source)
                    .setConstanteCoefficientDirecteur(constanteCoefficientDirecteur)
                    .setConstanteOrdonneeOrigine(constanteOrdonneeOrigine);
        }

        public static ImpactMessagerieEntity entity(CritereEntity critere, Double constanteCoefficientDirecteur, Double constanteOrdonneeOrigine, String source) {
            return new ImpactMessagerieEntity()
                    .setCritere(critere.getNomCritere())
                    .setSource(source)
                    .setConstanteCoefficientDirecteur(constanteCoefficientDirecteur)
                    .setConstanteOrdonneeOrigine(constanteOrdonneeOrigine);
        }
    }

    public static class CorrespondanceRefEquipementFactory {
        public static CorrespondanceRefEquipementDTO dto(String modeleSource, String refEquipementCible, String nomOrganisation) {
            return CorrespondanceRefEquipementDTO.builder()
                    .modeleEquipementSource(modeleSource)
                    .refEquipementCible(refEquipementCible)
                    .nomOrganisation(nomOrganisation)
                    .build();
        }

        public static CorrespondanceRefEquipement domain(String modeleSource, String refEquipementCible, String nomOrganisation) {
            return CorrespondanceRefEquipement.builder()
                    .modeleEquipementSource(modeleSource)
                    .refEquipementCible(refEquipementCible)
                    .nomOrganisation(nomOrganisation)
                    .build();
        }

        public static CorrespondanceRefEquipementEntity entity(String modeleSource, String refEquipementCible, String nomOrganisation) {
            return new CorrespondanceRefEquipementEntity()
                    .setModeleEquipementSource(modeleSource)
                    .setRefEquipementCible(refEquipementCible)
                    .setNomOrganisation(nomOrganisation);
        }

        public static CorrespondanceRefEquipementIdDTO idDTO(String modeleSource, String nomOrganisation) {
            return new CorrespondanceRefEquipementIdDTO()
                    .setModeleEquipementSource(modeleSource)
                    .setNomOrganisation(nomOrganisation);
        }

        public static CorrespondanceRefEquipementId idDomain(String modeleSource, String nomOrganisation) {
            return new CorrespondanceRefEquipementId()
                    .setModeleEquipementSource(modeleSource)
                    .setNomOrganisation(nomOrganisation);
        }

        public static CorrespondanceRefEquipementIdEntity idEntity(String modeleSource, String nomOrganisation) {
            return new CorrespondanceRefEquipementIdEntity()
                    .setModeleEquipementSource(modeleSource)
                    .setNomOrganisation(nomOrganisation);
        }
    }
}
