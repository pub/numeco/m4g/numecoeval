package org.mte.numecoeval.referentiel.infrastructure.jpa;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.domain.model.ImpactMessagerie;
import org.mte.numecoeval.referentiel.infrastructure.jpa.adapter.ImpactMessagerieJpaAdapter;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.ImpactMessagerieEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.repository.ImpactMessagerieRepository;
import org.mte.numecoeval.referentiel.infrastructure.mapper.ImpactMessagerieMapper;
import org.mte.numecoeval.referentiel.infrastructure.mapper.ImpactMessagerieMapperImpl;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Collections;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class ImpactMessagerieJpaAdapterTest {

    @InjectMocks
    private ImpactMessagerieJpaAdapter jpaAdapter;

    @Mock
    ImpactMessagerieRepository repository;

    ImpactMessagerieMapper mapper = new ImpactMessagerieMapperImpl();

    @BeforeEach
    void setup() {
        MockitoAnnotations.openMocks(this);
        ReflectionTestUtils.setField(jpaAdapter, "impactMessagerieMapper", mapper);
    }

    @Test
    void get_shouldReturnDomain() {
        var expectedCritere = "Changement climatique";
        var expectedEntity = new ImpactMessagerieEntity()
                .setCritere("Changement climatique")
                .setSource("Test")
                .setConstanteCoefficientDirecteur(0.05)
                .setConstanteOrdonneeOrigine(0.00120);

        Mockito.when(repository.findById(expectedCritere)).thenReturn(Optional.of(expectedEntity));

        var expectedDomain = assertDoesNotThrow( () -> jpaAdapter.get(expectedCritere) );

        assertNotNull(expectedDomain.getCritere());
        assertEquals(expectedEntity.getCritere(), expectedDomain.getCritere());
        assertEquals(expectedEntity.getConstanteCoefficientDirecteur(), expectedDomain.getConstanteCoefficientDirecteur());
        assertEquals(expectedEntity.getConstanteOrdonneeOrigine(), expectedDomain.getConstanteOrdonneeOrigine());
        assertEquals(expectedEntity.getSource(), expectedDomain.getSource());
    }

    @Test
    void get_shouldThrowException() {
        var expectedCritere = "Changement climatique";
        Mockito.when(repository.findById(expectedCritere)).thenReturn(Optional.empty());

        ReferentielException expectedException = assertThrows(ReferentielException.class, () -> jpaAdapter.get(expectedCritere) );

        assertEquals("ImpactMessagerie non trouvé",expectedException.getMessage());
    }

    @Test
    void get_whenNull_shouldThrowException() {
        ReferentielException expectedException = assertThrows(ReferentielException.class, () -> jpaAdapter.get(null) );

        assertEquals("ImpactMessagerie non trouvé",expectedException.getMessage());
    }

    @Test
    void purge_shouldCallDeleteAll() {
        jpaAdapter.purge();

        Mockito.verify(repository, Mockito.times(1)).deleteAll();
    }

    @Test
    void getAll_shouldCallfindAll() {
        var expectedEntity = new ImpactMessagerieEntity()
                .setCritere("Changement climatique")
                .setSource("Test")
                .setConstanteCoefficientDirecteur(0.05)
                .setConstanteOrdonneeOrigine(0.00120);
        Mockito.when(repository.findAll()).thenReturn(Collections.singletonList(expectedEntity));

        var result = jpaAdapter.getAll();

        Mockito.verify(repository, Mockito.times(1)).findAll();
        assertNotNull(result);
        assertEquals(1, result.size());

        var expectedDomain = result.get(0);
        assertNotNull(expectedDomain.getCritere());
        assertEquals(expectedEntity.getCritere(), expectedDomain.getCritere());
        assertEquals(expectedEntity.getConstanteCoefficientDirecteur(), expectedDomain.getConstanteCoefficientDirecteur());
        assertEquals(expectedEntity.getConstanteOrdonneeOrigine(), expectedDomain.getConstanteOrdonneeOrigine());
        assertEquals(expectedEntity.getSource(), expectedDomain.getSource());
    }

    @Test
    void saveAll_shouldCallsaveAll() {
        var domainToSave = new ImpactMessagerie()
                .setCritere("Changement climatique")
                .setSource("Test")
                .setConstanteCoefficientDirecteur(0.05)
                .setConstanteOrdonneeOrigine(0.00120);
        var entityToSave = mapper.toEntity(domainToSave);

        assertDoesNotThrow(() -> jpaAdapter.saveAll(Collections.singletonList(domainToSave)));

        Mockito.verify(repository, Mockito.times(1)).saveAll(Collections.singletonList(entityToSave));
    }

    @Test
    void save_shouldSaveAndReturnDomain() {
        var wantedDomain = new ImpactMessagerie()
                .setCritere("Changement climatique")
                .setSource("Test")
                .setConstanteCoefficientDirecteur(0.05)
                .setConstanteOrdonneeOrigine(0.00120);
        var expectedEntity = mapper.toEntities(Collections.singletonList(wantedDomain)).get(0);

        Mockito.when(repository.save(expectedEntity)).thenReturn(expectedEntity);

        var expectedDomain = assertDoesNotThrow( () -> jpaAdapter.save(wantedDomain) );

        assertNotNull(expectedDomain.getCritere());
        assertEquals(wantedDomain.getCritere(), expectedDomain.getCritere());
        assertEquals(expectedEntity.getConstanteCoefficientDirecteur(), expectedDomain.getConstanteCoefficientDirecteur());
        assertEquals(expectedEntity.getConstanteOrdonneeOrigine(), expectedDomain.getConstanteOrdonneeOrigine());
        assertEquals(wantedDomain.getSource(), expectedDomain.getSource());
    }

    @ParameterizedTest
    @NullSource
    void save_shouldSaveAndReturnNull(ImpactMessagerie nullValue) {
        var expectedDomain = assertDoesNotThrow( () -> jpaAdapter.save(nullValue) );

        assertNull(expectedDomain);
    }
}
