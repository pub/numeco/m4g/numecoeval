package org.mte.numecoeval.common.utils;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class PreparedStatementUtils {

    private PreparedStatementUtils() {
        // Private constructor
    }

    public static void setDoubleValue(PreparedStatement ps, int index, Double value) throws SQLException {
        if(Objects.isNull(value)) {
            ps.setNull(index, Types.DOUBLE );
        }
        else {
            ps.setDouble(index, value);
        }
    }

    public static void setIntValue(PreparedStatement ps, int index, Integer value) throws SQLException {
        if(Objects.isNull(value)) {
            ps.setNull(index, Types.INTEGER );
        }
        else {
            ps.setInt(index, value);
        }
    }

    public static Timestamp getTimestampFromLocalDateTime(LocalDateTime localDateTime) {
        if(Objects.isNull(localDateTime)) {
            return null;
        }
        return Timestamp.valueOf(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS").format(localDateTime));
    }

    public static Date getDateFromLocalDate(LocalDate localDate) {
        if(Objects.isNull(localDate)) {
            return null;
        }
        return Date.valueOf(DateTimeFormatter.ISO_LOCAL_DATE.format(localDate)) ;
    }
}
