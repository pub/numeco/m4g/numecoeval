package org.mte.numecoeval.common.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * RuntimeException spécifique à NumEcoEval.
 */
@Getter
@AllArgsConstructor
public class NumEcoEvalRuntimeException extends RuntimeException {

    /**
     * Description de l'erreur
     */
    private final String description;

}
