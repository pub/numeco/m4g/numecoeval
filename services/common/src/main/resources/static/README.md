# Swagger OpenAPI

Comment générer les fichiers openapi.

Mode opératoire générique :

- lancer le composant $COMPOSANT
- aller sur http://localhost:$PORT/swagger-ui/index.html
- Cliquer sur "/v3/api-docs/..."
- copier le contenu dans https://editor.swagger.io/ et récupérer le yaml

Fichiers openapi __générés__ par les composants:

- api-referentiels-openapi.yaml, COMPOSANT : api-referentiels, PORT : 18080
- api-expositiondonneesentrees-openapi.yaml, COMPOSANT : api-expositiondonneesentrees, PORT : 18081
- api-event-calculs-openapi.yaml, COMPOSANT : api-event-calculs, PORT : 18085

Fichiers pilotés __manuellement__ dans ce composant common:

- asyncapi_*.yaml
- api-event-calculs-async-openapi.yaml
- api-event-calculs-sync-openapi.yaml