package org.mte.numecoeval.donneesentrees.test.kafka;

import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.topic.data.OperationNonITDTO;
import org.springframework.kafka.annotation.KafkaListener;

@Slf4j
public class KafkaConsumerOperationNonIT extends KafkaConsumer<OperationNonITDTO> {

    @Override
    @KafkaListener(topics = "${numecoeval.topic.entree.operationNonIT}")
    public void consumeMessage(OperationNonITDTO message) {
        log.info("#############  Playload = {}", message);
        payloads.add(message);
        latch.countDown();
    }
}
