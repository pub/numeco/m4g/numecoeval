package org.mte.numecoeval.donneesentrees.test.kafka;

import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.topic.data.MessagerieDTO;
import org.springframework.kafka.annotation.KafkaListener;

@Slf4j
public class KafkaConsumerMessagerie extends KafkaConsumer<MessagerieDTO> {

    @Override
    @KafkaListener(topics = "${numecoeval.topic.entree.messagerie}")
    public void consumeMessage(MessagerieDTO message) {
        log.info("#############  Playload = {}", message);
        payloads.add(message);
        latch.countDown();

    }
}
