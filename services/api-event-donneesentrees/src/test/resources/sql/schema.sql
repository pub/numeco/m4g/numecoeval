CREATE TABLE IF NOT EXISTS en_donnees_entrees
(
    id                        int8         NOT NULL,
    date_update               timestamp    NULL,
    date_creation             timestamp    NULL,
    date_lot                  date         NULL,
    nom_organisation          varchar(255) NULL,
    nom_lot                   varchar(255) NULL,
    nbr_applications          int8         NULL,
    nbr_data_center           int8         NULL,
    nbr_equipements_physiques int8         NULL,
    nbr_equipements_virtuels  int8         NULL,
    nbr_messageries           int8         NULL,
    CONSTRAINT en_donnees_entrees_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS en_data_center
(
    id                   int8         NOT NULL,
    date_creation        timestamp    NULL,
    date_update          timestamp    NULL,
    date_lot             date         NULL,
    nom_lot              varchar(255) NULL,
    nom_organisation     varchar(255) NULL,
    nom_source_donnee    varchar(255) NULL,
    localisation         varchar(255) NULL,
    nom_court_datacenter varchar(255) NULL,
    nom_entite           varchar(255) NULL,
    nom_long_datacenter  varchar(255) NULL,
    pue                  float8       NULL,
    qualite              varchar(255) NULL,
    CONSTRAINT en_data_center_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS en_equipement_physique
(
    id                        int8         NOT NULL,
    date_creation             timestamp    NULL,
    date_update               timestamp    NULL,
    date_lot                  date         NULL,
    nom_lot                   varchar(255) NULL,
    nom_organisation          varchar(255) NULL,
    nom_source_donnee         varchar(255) NULL,
    conso_elec_annuelle       float8       NULL,
    date_achat                date         NULL,
    date_retrait              date         NULL,
    duree_vie_defaut          float8       NULL,
    duree_usage_interne       float8       NULL,
    duree_usage_amont         float8       NULL,
    duree_usage_aval          float8       NULL,
    modele                    varchar(255) NULL,
    nb_coeur                  varchar(255) NULL,
    nom_court_datacenter      varchar(255) NULL,
    nom_entite                varchar(255) NULL,
    nom_equipement_physique   varchar(255) NULL,
    pays_utilisation          varchar(255) NULL,
    quantite                  float8       NULL,
    serveur                   bool         NOT NULL,
    statut                    varchar(255) NULL,
    mode_utilisation          varchar(255) NULL,
    taux_utilisation          float8       NULL,
    "type"                    varchar(255) NULL,
    utilisateur               varchar(255) NULL,
    qualite                   varchar(255) NULL,
    CONSTRAINT en_equipement_physique_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS en_equipement_virtuel
(
    id                      int8         NOT NULL,
    date_creation           timestamp    NULL,
    date_lot                date         NULL,
    nom_lot                 varchar(255) NULL,
    nom_organisation        varchar(255) NULL,
    nom_source_donnee       varchar(255) NULL,
    "cluster"               varchar(255) NULL,
    nom_entite              varchar(255) NULL,
    nom_equipement_physique varchar(255) NULL,
    nom_equipement_virtuel  varchar(255) NULL,
    vcpu                    int4         NULL,
    qualite                 varchar(255) NULL,
    CONSTRAINT en_equipement_virtuel_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS en_application
(
    id                      int8                 NOT NULL,
    date_creation           timestamp            NULL,
    date_lot                date                 NULL,
    nom_lot                 varchar(255)         NULL,
    nom_organisation        varchar(255)         NULL,
    nom_source_donnee       varchar(255)         NULL,
    domaine                 varchar(255)         NULL,
    nom_application         varchar(255)         NULL,
    nom_entite              varchar(255)         NULL,
    nom_equipement_virtuel  varchar(255)         NULL,
    nom_equipement_physique varchar(255)         NULL,
    sous_domaine            varchar(255)         NULL,
    type_environnement      varchar(255)         NULL,
    qualite                 varchar(255)         NULL,
    CONSTRAINT en_application_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS en_operation_non_it
(
    id                            int8              NOT NULL,
    date_creation                 timestamp         NULL,
    date_update                   timestamp         NULL,
    date_lot                      date              NULL,
    nom_lot                       varchar(255)      NULL,
    nom_organisation              varchar(255)      NULL,
    nom_source_donnee             varchar(255)      NULL,
    nom_item_non_it               varchar(255)      NULL,
    quantite                      float8            NULL,
    type                          varchar(255)      NULL,
    duree_de_vie                  float8            NULL,
    localisation                  varchar(255)      NULL,
    nom_entite                    varchar(255)      NULL,
    nom_court_datacenter          varchar(255)      NULL,
    description                   varchar(255)      NULL,
    conso_elec_annuelle           float8            NULL,
    statut_traitement             varchar(255)      NULL,
    qualite                       varchar(255)      NULL,
    CONSTRAINT en_operation_non_it_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS en_entite
(
    id                                  int8         NOT NULL,
    date_creation                       timestamp    NULL,
    date_update                         timestamp    NULL,
    date_lot                            date         NULL,
    nom_lot                             varchar(255) NULL,
    nom_organisation                    varchar(255) NULL,
    nom_source_donnee                   varchar(255) NULL,
    nom_entite                          varchar(255) NULL,
    nb_collaborateurs                   int4         NULL,
    responsable_entite                  varchar(255) NULL,
    responsable_numerique_durable       varchar(255) NULL,
    CONSTRAINT en_entite_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS en_messagerie
(
    id                             int8         NOT NULL,
    date_creation                  timestamp    NULL,
    date_update                    timestamp    NULL,
    date_lot                       date         NULL,
    nom_lot                        varchar(255) NULL,
    nom_organisation               varchar(255) NULL,
    nom_source_donnee              varchar(255) NULL,
    mois_annee                     int4         NULL,
    nom_entite                     varchar(255) NULL,
    nombre_mail_emis               int4         NULL,
    nombre_mail_emisxdestinataires int4         NULL,
    volume_total_mail_emis         int4         NULL,
    CONSTRAINT en_messagerie_pkey PRIMARY KEY (id)
);

ALTER TABLE IF EXISTS en_donnees_entrees ADD COLUMN IF NOT EXISTS statut_traitement varchar(255);
ALTER TABLE IF EXISTS en_data_center ADD COLUMN IF NOT EXISTS statut_traitement varchar(255);
ALTER TABLE IF EXISTS en_equipement_physique ADD COLUMN IF NOT EXISTS statut_traitement varchar(255);
ALTER TABLE IF EXISTS en_equipement_virtuel ADD COLUMN IF NOT EXISTS statut_traitement varchar(255);
ALTER TABLE IF EXISTS en_application ADD COLUMN IF NOT EXISTS statut_traitement varchar(255);
ALTER TABLE IF EXISTS en_messagerie ADD COLUMN IF NOT EXISTS statut_traitement varchar(255);

ALTER TABLE IF EXISTS en_equipement_physique ADD COLUMN IF NOT EXISTS qualite varchar(255);
ALTER TABLE IF EXISTS en_application ADD COLUMN IF NOT EXISTS qualite varchar(255);
ALTER TABLE IF EXISTS en_operation_non_it ADD COLUMN IF NOT EXISTS qualite varchar(255);
ALTER TABLE IF EXISTS en_equipement_virtuel ADD COLUMN IF NOT EXISTS qualite varchar(255);
ALTER TABLE IF EXISTS en_data_center ADD COLUMN IF NOT EXISTS qualite varchar(255);

ALTER TABLE IF EXISTS en_equipement_physique DROP COLUMN IF EXISTS nb_jour_utilise_an;
ALTER TABLE IF EXISTS en_equipement_physique DROP COLUMN IF EXISTS go_telecharge;