INSERT INTO en_data_center (id, date_creation, nom_lot, date_lot, nom_organisation, localisation, nom_court_datacenter,
                            nom_entite, nom_long_datacenter, pue, statut_traitement, date_update, nom_source_donnee)
VALUES (6002, '2023-03-08 19:45:39.484539', 'ENTITE|2022-01-01', '2022-01-01', 'ENTITE', 'France', 'TestDataCenter', 'ENTITE', 'TEST', 1.44,
        'EN_ATTENTE', NULL, 'SOURCE_A');

INSERT INTO en_equipement_physique (id, date_creation, nom_lot, date_lot, nom_organisation, conso_elec_annuelle,
                                    date_achat, date_retrait, duree_vie_defaut, duree_usage_interne, duree_usage_amont, duree_usage_aval,
                                    modele, nb_coeur, mode_utilisation, taux_utilisation, nom_court_datacenter, nom_entite,
                                    nom_equipement_physique, pays_utilisation, quantite, serveur, statut, "type",
                                    utilisateur, statut_traitement, date_update, nom_source_donnee, qualite)
VALUES (376826, '2023-03-23 15:53:51.179031', 'ENTITE|2022-01-01', '2022-01-01', 'ENTITE', NULL, '2022-01-01', NULL, 8.0, 5.0, 1.0, 2.0,
        'computer monitor-01-55', '','BYOD',0.4, '', 'ENTITE', '2023-03-09-Ecran-105', 'France', 13.0, false, 'actif', 'Ecran',
        '', 'EN_ATTENTE', NULL, 'SOURCE_A','HAUTE');

