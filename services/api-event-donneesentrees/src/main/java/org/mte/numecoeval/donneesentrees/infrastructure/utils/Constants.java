package org.mte.numecoeval.donneesentrees.infrastructure.utils;

public class Constants {
    public static final String NOM_LOT = "nomLot";
    public static final String DATELOT = "dateLot";
    public static final String NOM_ORGANISATION = "nomOrganisation";
}
