package org.mte.numecoeval.donneesentrees;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.kafka.annotation.EnableKafka;

@SpringBootApplication
@EnableIntegration
@EnableKafka
public class ApiEventDonneesEntreesApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiEventDonneesEntreesApplication.class, args);
    }

}
