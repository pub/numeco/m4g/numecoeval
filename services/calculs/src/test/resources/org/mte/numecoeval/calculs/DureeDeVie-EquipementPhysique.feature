#language: fr
#encoding: utf-8
Fonctionnalité: : Durée de Vie d'un équipement physique / Durée de vie des équipements par défaut
  # JDDs - 1A à 1F
  # Les noms d'équipement physique ont été simplifié
  Scénario: Calcul de la Durée de Vie avec Date d'achat et Date retrait connues (période < 1 an)
    Soit Une demande de calcule de durée de vie tel que
      | nomEquipementPhysique             | dateAchat  | dateRetrait | refTypeEquipement.dureeVieDefaut | hypothese.dureeVieParDefaut |
      | 2023-04-OrdinateurPortable-test1A | 01/10/2022 | 01/03/2023  |                                  |                             |
    Alors la durée de vie de l'équipement est 1

  Scénario: Calcul de la Durée de Vie avec Date d'achat et Date retrait connues (période > 1 an)
    Soit Une demande de calcule de durée de vie tel que
      | nomEquipementPhysique             | dateAchat  | dateRetrait | refTypeEquipement.dureeVieDefaut | hypothese.dureeVieParDefaut |
      | 2023-04-OrdinateurPortable-test1B | 01/10/2021 | 01/03/2023  |                                  |                             |
    Alors la durée de vie de l'équipement est 1,4136986301369863

  Scénario: Calcul de la Durée de Vie avec Date d'achat connue mais Date retrait inconnue
    Soit Une demande de calcule de durée de vie tel que
      | nomEquipementPhysique             | dateAchat  | dateRetrait | refTypeEquipement.dureeVieDefaut | hypothese.dureeVieParDefaut |
      | 2023-04-OrdinateurPortable-test1C | 01/10/2022 |             |                                  |                             |
    Alors la durée de vie correspond à la différence entre la date d'achat et la date du jour

  Scénario: Calcul de la Durée de Vie avec Date d'achat et Date retrait inconnues - Equipement physique présent dans la table des références d'équipement
    Soit Une demande de calcule de durée de vie tel que
      | nomEquipementPhysique             | dateAchat | dateRetrait | refTypeEquipement.dureeVieDefaut | hypothese.dureeVieParDefaut |
      | 2023-04-OrdinateurPortable-test1D |           |             | 6,7                              |                             |
    Alors la durée de vie de l'équipement est 6,7

  Scénario: Calcul de la Durée de Vie avec Date d'achat et Date retrait inconnues - Equipement physique non présent dans la table des références d'équipement - Durée de vie par défaut des équipements présent dans la table des hypothèses
    Soit Une demande de calcule de durée de vie tel que
      | nomEquipementPhysique             | dateAchat | dateRetrait | refTypeEquipement.dureeVieDefaut | hypothese.dureeVieParDefaut |
      | 2023-04-OrdinateurPortable-test1E |           |             |                                  | 5,2                         |
    Alors la durée de vie de l'équipement est 5,2

  Scénario: Calcul de la Durée de Vie avec Date d'achat et Date retrait inconnues - Equipement physique non présent dans la table des références d'équipement - Durée de vie par défaut des équipements non présent dans la table des hypothèses
    Soit Une demande de calcule de durée de vie tel que
      | nomEquipementPhysique             | dateAchat | dateRetrait | refTypeEquipement.dureeVieDefaut | hypothese.dureeVieParDefaut |
      | 2023-04-OrdinateurPortable-test1F |           |             |                                  |                             |
    Alors le calcul de durée de vie lève une erreur de type "ErrCalcFonc" avec le message "La durée de vie par défaut de l'équipement n'a pas pu être déterminée"

  Scénario: Calcul de la Durée de Vie avec Date d'achat et Date retrait mal renseignées (date de retrait avant date d'achat)
    Soit Une demande de calcule de durée de vie tel que
      | nomEquipementPhysique             | dateAchat  | dateRetrait | refTypeEquipement.dureeVieDefaut | hypothese.dureeVieParDefaut |
      | 2023-04-OrdinateurPortable-test1F | 01/01/2023 | 01/02/2022  |                                  |                             |
    Alors le calcul de durée de vie lève une erreur de type "ErrCalcFonc" avec le message "La durée de vie de l'équipement n'a pas pu être déterminée"

