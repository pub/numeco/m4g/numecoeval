package org.mte.numecoeval.calculs;

import io.cucumber.spring.CucumberContextConfiguration;
import org.junit.jupiter.api.Test;
import org.junit.platform.suite.api.ConfigurationParameter;
import org.junit.platform.suite.api.IncludeEngines;
import org.junit.platform.suite.api.SelectClasspathResource;
import org.junit.platform.suite.api.Suite;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.context.ActiveProfiles;

import java.text.NumberFormat;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import static io.cucumber.junit.platform.engine.Constants.GLUE_PROPERTY_NAME;
import static io.cucumber.junit.platform.engine.Constants.PLUGIN_PROPERTY_NAME;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@CucumberContextConfiguration
@SpringBootTest(classes = CucumberIntegrationTest.TestApplication.class)
@ActiveProfiles(profiles = { "test" })
@Suite
@IncludeEngines("cucumber")
@SelectClasspathResource("org/mte/numecoeval/calculs")
@ConfigurationParameter(key = PLUGIN_PROPERTY_NAME, value = "pretty,html:target/cucumber-reports.html")
@ConfigurationParameter(key = GLUE_PROPERTY_NAME, value = "org.mte.numecoeval.calculs")
public class CucumberIntegrationTest {

    public static final DateTimeFormatter FORMATTER_FRENCH_FORMAT = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    public static final NumberFormat NUMBER_FRENCH_FORMAT = NumberFormat.getInstance(Locale.FRENCH);

    @Test
    void testContextLoadOK(@Autowired Environment environment) {
        assertNotNull(environment, "L'environnement est correctement chargé");
    }

    @SpringBootApplication
    public static class TestApplication {

        public static void main(String[] args) {
            SpringApplication.run(TestApplication.class, args);
        }

    }
}
