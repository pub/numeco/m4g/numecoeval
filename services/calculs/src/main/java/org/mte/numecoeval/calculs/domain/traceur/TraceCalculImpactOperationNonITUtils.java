package org.mte.numecoeval.calculs.domain.traceur;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.mte.numecoeval.calculs.domain.data.trace.*;
import org.mte.numecoeval.calculs.domain.exception.CalculImpactException;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TraceCalculImpactOperationNonITUtils {
    public static TraceCalculImpactOperationNonIT buildTraceErreur(CalculImpactException exception) {
        var message = "Erreur lors du calcul de l'impact de l'opération non IT";
        if (exception != null) {
            message = exception.getErrorType() + " : " + exception.getMessage();
        }
        return TraceCalculImpactOperationNonIT.builder()
                .erreur(message)
                .build();
    }

    public static TraceCalculImpactOperationNonIT buildTrace(ConsoElecAnMoyenne consoElecAnMoyenne, MixElectrique mixElectrique, Double valeurReferentielFacteurCaracterisation, String sourceReferentielFacteurCaracterisation, Hypothese hypothese, DureeDeVie dureeDeVie, String erreur) {
        return TraceCalculImpactOperationNonIT.builder()
                .consoElecAnMoyenne(consoElecAnMoyenne)
                .mixElectrique(mixElectrique)
                .valeurReferentielFacteurCaracterisation(valeurReferentielFacteurCaracterisation)
                .sourceReferentielFacteurCaracterisation(sourceReferentielFacteurCaracterisation)
                .hypothese(hypothese)
                .dureeDeVie(dureeDeVie)
                .erreur(erreur)
                .build();
    }

    public static String getFormuleUtilisation(Double quantite, Double consoElecAnMoyenne, Double mixElectriqueValeur) {
        return "ImpactOperationNonIT = (Quantité(%s) * ConsoElecAnMoyenne(%s) * MixElectrique(%s))".formatted(quantite, consoElecAnMoyenne, mixElectriqueValeur);
    }

    public static String getFormuleBase(Double quantite, Double valeurReferentiel) {
        return "ImpactOperationNonIT = (Quantité(%s) * ReferentielFacteurCaracterisation(%s))".formatted(quantite, valeurReferentiel);
    }

    public static String getFormuleDeplacement(Double consoMoyenne, Double quantite) {
        return "ImpactOperationNonIT = (ConsoMoyenne(%s) * NbKmParcourus(%s)".formatted(consoMoyenne, quantite);
    }


}
