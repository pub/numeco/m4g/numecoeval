package org.mte.numecoeval.calculs.domain.traceur;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TraceUtils {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    private TraceUtils() {
        // private constructor
    }

    public static String getTraceFromTraceur(Object traceur) {
        String trace = "";
        try {
            trace = objectMapper.writeValueAsString(traceur);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage());
        }
        return trace;
    }
}
