package org.mte.numecoeval.calculs.domain.data.entree;

import lombok.*;

import java.time.LocalDate;
import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class EquipementPhysique {
    private String nomEquipementPhysique;
    private String modele;
    private String type;
    private Double consoElecAnnuelle;
    private String statut;
    private String paysDUtilisation;
    private String utilisateur;
    private LocalDate dateAchat;
    private LocalDate dateRetrait;
    private Double dureeUsageInterne;
    private Double dureeUsageAmont;
    private Double dureeUsageAval;
    private Double quantite;
    private String nbCoeur;
    private String nomCourtDatacenter;
    private DataCenter dataCenter;
    private boolean serveur;
    private Double dureeVieDefaut;
    private List<EquipementVirtuel> equipementsVirtuels;
    private String nomLot;
    private LocalDate dateLot;
    private String nomOrganisation;
    private String nomEntite;
    private String nomSourceDonnee;
    private Integer nbEquipementsVirtuels;
    private Integer nbTotalVCPU;
    private String modeUtilisation;
    private Double tauxUtilisation;
    private String qualite;
}
