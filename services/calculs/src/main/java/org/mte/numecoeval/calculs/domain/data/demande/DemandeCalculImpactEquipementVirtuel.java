package org.mte.numecoeval.calculs.domain.data.demande;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.mte.numecoeval.calculs.domain.data.entree.EquipementVirtuel;
import org.mte.numecoeval.calculs.domain.data.indicateurs.ImpactEquipementPhysique;

import java.time.LocalDateTime;

@Builder
@AllArgsConstructor
@Getter
@Setter
public class DemandeCalculImpactEquipementVirtuel {

    LocalDateTime dateCalcul;
    EquipementVirtuel equipementVirtuel;
    Integer nbEquipementsVirtuels;
    Integer nbTotalVCPU;
    ImpactEquipementPhysique impactEquipement;
    Double stockageTotalVirtuel;
}
