package org.mte.numecoeval.calculs.domain.data.demande;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.mte.numecoeval.calculs.domain.data.entree.Application;
import org.mte.numecoeval.calculs.domain.data.indicateurs.ImpactEquipementVirtuel;

import java.time.LocalDateTime;

@Builder
@AllArgsConstructor
@Getter
@Setter
public class DemandeCalculImpactApplication {

    LocalDateTime dateCalcul;
    Application application;
    Integer nbApplications;
    ImpactEquipementVirtuel impactEquipementVirtuel;

    /**
     * Renvoie le nombre d'application pour un calcul.
     * @return si nbApplications est différent de null et supérieur à 0, renvoie {@link #nbApplications} sinon {@code 1}
     */
    public Integer getNbApplicationsForCalcul() {
        if(nbApplications != null && nbApplications > 0){
            return nbApplications;
        }

        return 1;
    }
}
