package org.mte.numecoeval.calculs.domain.traceur;

import org.mte.numecoeval.calculs.domain.data.demande.DemandeCalculImpactApplication;
import org.mte.numecoeval.calculs.domain.data.trace.TraceCalculImpactApplication;
import org.mte.numecoeval.calculs.domain.exception.CalculImpactException;

public class TraceCalculImpactApplicationUtils {

    private TraceCalculImpactApplicationUtils() {
        // private constructor
    }

    public static TraceCalculImpactApplication buildTrace(DemandeCalculImpactApplication demandeCalcul) {
        return TraceCalculImpactApplication.builder()
                .formule("ImpactApplication = ImpactEquipementVirtuel(%s) / nbApplications(%s)".formatted(demandeCalcul.getImpactEquipementVirtuel().getImpactUnitaire(), demandeCalcul.getNbApplicationsForCalcul()))
                .nbApplications(demandeCalcul.getNbApplications())
                .build();
    }

    public static TraceCalculImpactApplication buildTraceError(CalculImpactException exception) {
        var message = "Erreur lors du calcul de l'impact d'application";
        if(exception != null) {
            message = exception.getErrorType() + " : " + exception.getMessage();
        }
        return TraceCalculImpactApplication.builder()
                .erreur(message)
                .build();
    }
}
