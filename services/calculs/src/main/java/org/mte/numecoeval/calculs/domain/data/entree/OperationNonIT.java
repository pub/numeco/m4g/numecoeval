package org.mte.numecoeval.calculs.domain.data.entree;

import lombok.*;

import java.time.LocalDate;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class OperationNonIT {
    private String nomItemNonIT;
    private Double quantite;
    private String type;
    private Double dureeDeVie;
    private String localisation;
    private String nomEntite;
    private String nomCourtDatacenter;
    private String description;
    private Double consoElecAnnuelle;
    private String nomLot;
    private LocalDate dateLot;
    private String nomOrganisation;
    private String nomSourceDonnee;
    private String statut;
    private String qualite;
}
