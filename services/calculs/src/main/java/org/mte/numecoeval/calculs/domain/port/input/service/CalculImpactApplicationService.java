package org.mte.numecoeval.calculs.domain.port.input.service;

import org.mte.numecoeval.calculs.domain.data.demande.DemandeCalculImpactApplication;
import org.mte.numecoeval.calculs.domain.data.indicateurs.ImpactApplication;

public interface CalculImpactApplicationService {

    ImpactApplication calculImpactApplicatif(DemandeCalculImpactApplication demandeCalcul);

}
