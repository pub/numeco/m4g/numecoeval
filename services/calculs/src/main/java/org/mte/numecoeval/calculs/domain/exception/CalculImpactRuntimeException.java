package org.mte.numecoeval.calculs.domain.exception;

public class CalculImpactRuntimeException extends RuntimeException{

    private final String errorType;

    public CalculImpactRuntimeException(String message) {
        super(message);
        this.errorType = null;
    }

    public CalculImpactRuntimeException(String message, Throwable cause) {
        super(message, cause);
        this.errorType = null;
    }

    public CalculImpactRuntimeException(String errorType, String message ) {
        super(message);
        this.errorType = errorType;
    }

    public String getErrorType() {
        return errorType;
    }
}
