package org.mte.numecoeval.calculs.domain.data.indicateurs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Builder
@AllArgsConstructor
@Getter
@Setter
public class ImpactMessagerie {

    LocalDateTime dateCalcul;
    String versionCalcul;
    String critere;
    String source;
    String statutIndicateur;
    String trace;
    String nomLot;
    LocalDate dateLot;
    String unite;
    String nomOrganisation;
    String nomEntite;
    String nomSourceDonnee;
    Double impactMensuel;
    Integer moisAnnee;
    Double volumeTotalMailEmis;
    Double nombreMailEmis;

}
