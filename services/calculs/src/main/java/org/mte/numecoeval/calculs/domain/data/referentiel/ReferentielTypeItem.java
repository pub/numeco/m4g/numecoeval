package org.mte.numecoeval.calculs.domain.data.referentiel;

import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ReferentielTypeItem {
    String type;

    String categorie;

    Boolean serveur;

    String commentaire;

    Double dureeVieDefaut;

    String refHypothese;

    String refItemParDefaut;

    String source;
}
