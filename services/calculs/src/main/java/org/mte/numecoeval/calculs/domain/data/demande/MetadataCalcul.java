package org.mte.numecoeval.calculs.domain.data.demande;

import org.mte.numecoeval.calculs.domain.data.trace.TraceCalculImpactOperationNonIT;

public record MetadataCalcul(double valeurImpactUnitaire,
                             TraceCalculImpactOperationNonIT traceCalculImpactOperationNonIT) {
}