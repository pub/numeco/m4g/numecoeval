package org.mte.numecoeval.calculs.domain.data.erreur;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TypeErreurCalcul {

    ERREUR_FONCTIONNELLE("ErrCalcFonc"),
    ERREUR_TECHNIQUE("ErrCalcTech");

    private final String code;
}
