package org.mte.numecoeval.calculs.domain.data.referentiel;


import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ReferentielHypothese {

    private String code;
    private Double valeur;
    private String source;
}
