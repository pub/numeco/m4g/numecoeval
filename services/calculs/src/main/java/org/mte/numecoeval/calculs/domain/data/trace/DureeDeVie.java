package org.mte.numecoeval.calculs.domain.data.trace;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DureeDeVie {
    private Double valeurRetenue;
    private String methodeDureeUsage;
    private Double dureeUsageInterne;
    private Double dureeUsageAmont;
    private Double dureeUsageAval;
    private String dateAchat;
    private String dateRetrait;
    private DureeDeVieParDefaut dureeDeVieParDefaut;
}
