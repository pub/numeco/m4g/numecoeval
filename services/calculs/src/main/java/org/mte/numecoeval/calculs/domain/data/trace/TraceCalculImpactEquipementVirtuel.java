package org.mte.numecoeval.calculs.domain.data.trace;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TraceCalculImpactEquipementVirtuel {
    String formule;
    String erreur;
    Integer nbEquipementsVirtuels;
    Integer nbTotalVCPU;
    Double stockageTotalVirtuel;
}
