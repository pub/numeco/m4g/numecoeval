package org.mte.numecoeval.calculs.domain.data.demande;

import lombok.*;
import org.mte.numecoeval.calculs.domain.data.entree.EquipementPhysique;
import org.mte.numecoeval.calculs.domain.data.referentiel.*;
import org.mte.numecoeval.calculs.domain.utils.Constants;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class DemandeCalculImpactEquipementPhysique {

    LocalDateTime dateCalcul;

    EquipementPhysique equipementPhysique;

    ReferentielEtapeACV etape;

    ReferentielCritere critere;

    ReferentielTypeItem typeItem;

    ReferentielCorrespondanceRefEquipement correspondanceRefEquipement;

    List<ReferentielHypothese> hypotheses;

    List<ReferentielFacteurCaracterisation> facteurCaracterisations;

    OptionsCalcul optionsCalcul;

    public Optional<ReferentielHypothese> getHypotheseFromCode(String code) {
        if (code == null || hypotheses == null) {
            return Optional.empty();
        }

        return hypotheses.stream()
                .filter(hypothese -> code.equals(hypothese.getCode()))
                .findFirst();
    }

    public List<ReferentielHypothese> getHypothesesStartingWith(String startString) {
        return hypotheses == null ? List.of() : hypotheses.stream()
                .filter(hypothese -> hypothese.getCode().startsWith(startString))
                .toList();
    }

    public Optional<ReferentielFacteurCaracterisation> getFacteurCaracterisation(String refItem) {
        if (refItem == null || facteurCaracterisations == null) {
            return Optional.empty();
        }
        return facteurCaracterisations.stream()
                .filter(Objects::nonNull)
                .filter(facteurCaracterisation -> refItem.equals(facteurCaracterisation.getNom())
                        && critere.getNomCritere().equals(facteurCaracterisation.getCritere())
                        && etape.getCode().equals(facteurCaracterisation.getEtape())
                )
                .findFirst();
    }

    public Optional<ReferentielFacteurCaracterisation> getMixElectriqueFromFacteurCaracterisation(String localisation) {
        if (localisation == null || facteurCaracterisations == null) {
            return Optional.empty();
        }
        return facteurCaracterisations.stream()
                .filter(Objects::nonNull)
                .filter(fc -> Constants.ELECTRICITY_MIX_CATEGORY.equals(fc.getCategorie()) &&
                        localisation.equals(fc.getLocalisation()) &&
                        critere.getNomCritere().equals(fc.getCritere()))
                .findFirst();
    }

    public String getRefEquipementCible() {
        if (correspondanceRefEquipement != null) {
            return correspondanceRefEquipement.getRefEquipementCible();
        }
        return null;
    }

    public String getRefItemParDefaut() {
        if (typeItem != null) {
            return typeItem.getRefItemParDefaut();
        }
        return null;
    }
}
