package org.mte.numecoeval.calculs.domain.data.referentiel;

import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ReferentielCritere {
    String nomCritere;
    String unite;
    String description;

}
