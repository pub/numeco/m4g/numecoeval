package org.mte.numecoeval.calculs.domain.data.referentiel;

import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ReferentielFacteurCaracterisation {
    String nom;
    String etape;
    String critere;
    String description;
    String niveau;
    String tiers;
    String categorie;
    Double consoElecMoyenne;
    String localisation;
    Double valeur;
    String unite;
    String source;
}
