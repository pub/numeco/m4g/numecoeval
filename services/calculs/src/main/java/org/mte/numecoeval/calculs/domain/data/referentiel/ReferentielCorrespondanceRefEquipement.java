package org.mte.numecoeval.calculs.domain.data.referentiel;

import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ReferentielCorrespondanceRefEquipement {

    String modeleEquipementSource;

    String refEquipementCible;
}
