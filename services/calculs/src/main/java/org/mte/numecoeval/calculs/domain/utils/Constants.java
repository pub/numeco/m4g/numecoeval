package org.mte.numecoeval.calculs.domain.utils;

import java.util.List;

public class Constants {

    public static final String ELECTRICITY_MIX_CATEGORY = "electricity-mix";
    public static final String CATEGORIE_RESEAU_FIXE = "RESEAU_FIXE";
    public static final String CATEGORIE_RESEAU_MOBILE = "RESEAU_MOBILE";
    public static final String CATEGORIE_BATIMENT = "BATIMENT";
    public static final String CATEGORIE_MAINTENANCE = "MAINTENANCE";
    public static final String CATEGORIE_DEPLACEMENT_ELECTRIQUE = "DEPLACEMENT_ELECTRIQUE";
    public static final String CATEGORIE_DEPLACEMENT_HYBRIDE = "DEPLACEMENT_HYBRIDE";
    public static final String CATEGORIE_DEPLACEMENT_ESSENCE = "DEPLACEMENT_ESSENCE";
    public static final List<String> CATEGORIES_INDIRECT_FABRICATION = List.of(
            CATEGORIE_DEPLACEMENT_HYBRIDE,
            CATEGORIE_DEPLACEMENT_ESSENCE
    );

}
