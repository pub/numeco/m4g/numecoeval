package org.mte.numecoeval.calculs.domain.data.referentiel;

import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ReferentielEtapeACV {
    String code;
    String libelle;
}
