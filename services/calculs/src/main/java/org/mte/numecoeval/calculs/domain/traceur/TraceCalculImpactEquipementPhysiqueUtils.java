package org.mte.numecoeval.calculs.domain.traceur;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.mte.numecoeval.calculs.domain.data.trace.ConsoElecAnMoyenne;
import org.mte.numecoeval.calculs.domain.data.trace.DureeDeVie;
import org.mte.numecoeval.calculs.domain.data.trace.MixElectrique;
import org.mte.numecoeval.calculs.domain.data.trace.TraceCalculImpactEquipementPhysique;
import org.mte.numecoeval.calculs.domain.exception.CalculImpactException;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TraceCalculImpactEquipementPhysiqueUtils {

    public static TraceCalculImpactEquipementPhysique buildTraceErreur(CalculImpactException exception) {
        var message = "Erreur lors du calcul de l'impact d'équipement physique";
        if (exception != null) {
            message = exception.getErrorType() + " : " + exception.getMessage();
        }
        return TraceCalculImpactEquipementPhysique.builder()
                .erreur(message)
                .build();
    }

    public static TraceCalculImpactEquipementPhysique buildTracePremierScenario(Double quantite, ConsoElecAnMoyenne consoElecAnMoyenne, MixElectrique mixElectriqueValeur, Double taux_utilisation) {
        return TraceCalculImpactEquipementPhysique.builder()
                .formule(getFormulePremierScenario(quantite, consoElecAnMoyenne.getValeur(), mixElectriqueValeur.getValeur(), taux_utilisation))
                .consoElecAnMoyenne(consoElecAnMoyenne)
                .mixElectrique(mixElectriqueValeur)
                .build();
    }

    public static TraceCalculImpactEquipementPhysique buildTraceSecondScenario(Double quantite, Double valeurRefrentiel, String sourceReferentiel, DureeDeVie dureeDeVie, Double taux_utilisation) {
        return TraceCalculImpactEquipementPhysique.builder()
                .formule(getFormuleSecondScenario(quantite, valeurRefrentiel, dureeDeVie.getValeurRetenue(), taux_utilisation))
                .valeurReferentielFacteurCaracterisation(valeurRefrentiel)
                .sourceReferentielFacteurCaracterisation(sourceReferentiel)
                .dureeDeVie(dureeDeVie)
                .build();
    }

    public static String getFormulePremierScenario(Double quantite, Double consoElecAnMoyenne, Double mixElectriqueValeur, Double taux_utilisation) {
        return "ImpactEquipementPhysique = Quantité(%s) * ConsoElecAnMoyenne(%s) * MixElectrique(%s) * TauxUtilisation(%s)".formatted(quantite, consoElecAnMoyenne, mixElectriqueValeur, taux_utilisation);
    }

    public static String getFormuleSecondScenario(Double quantite, Double valeurRefrentiel, Double dureeVie, Double taux_utilisation) {
        return "ImpactEquipementPhysique = (Quantité(%s) * referentielFacteurCaracterisation(%s) * TauxUtilisation(%s)) / dureeVie(%s)".formatted(quantite, valeurRefrentiel, taux_utilisation, dureeVie);
    }

}
