# calculs

Module contenant l'intégralité des règles de calculs et du code métier lié aux calculs d'impact d'équipement.

**Ce module s'utilise comme une librairie et non comme une application.**

## Requirements

- Any JDK 21 Implementation
    - [Coretto](https://docs.aws.amazon.com/corretto/latest/corretto-17-ug/downloads-list.html)
    - [Open JDK](https://jdk.java.net/java-se-ri/21)
- [Maven 3](https://maven.apache.org/download.cgi)
- IDE compatible with Maven 3 and JDK 21 are highly recommended to develop

### Build

To build the app :

```bash
mvn clean install
```

## License

Le projet est sous licence Apache 2

## Project status

If you have run out of energy or time for your project, put a note at the top of the README saying that development has
slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or
owner, allowing your project to keep going. You can also make an explicit request for maintainers.
