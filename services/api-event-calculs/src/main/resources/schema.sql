-- Creation des tables indicateurs
CREATE TABLE IF NOT EXISTS ind_indicateur_impact_equipement_physique
(
    date_calcul                     timestamp       NULL,
    date_lot                        date            NULL,
    nom_lot                         varchar(255)    NULL,
    etapeacv                        varchar(255)    NULL,
    critere                         varchar(255)    NULL,
    "source"                        varchar(255)    NULL,
    statut_indicateur               varchar(255)    NULL,
    trace                           text            NULL,
    version_calcul                  varchar(255)    NULL,
    conso_elec_moyenne              float8          NULL,
    impact_unitaire                 float8          NULL,
    quantite                        int4            NULL,
    statut_equipement_physique      varchar(255)    NULL,
    type_equipement                 varchar(255)    NULL,
    unite                           varchar(255)    NULL,
    nom_entite                      varchar(255)    NULL,
    nom_organisation                varchar(255)    NULL,
    nom_source_donnee               varchar(255)    NULL,
    nom_equipement                  varchar(255)    NULL,
    qualite                         varchar(255)    NULL,
    date_lot_discriminator          date            NOT NULL DEFAULT('1970-01-01'),
    nom_organisation_discriminator  varchar(255)    NOT NULL DEFAULT(''),
    nom_entite_discriminator        varchar(255)    NOT NULL DEFAULT(''),
    nom_source_donnee_discriminator varchar(255)    NOT NULL DEFAULT('')
);

CREATE TABLE IF NOT EXISTS ind_indicateur_impact_equipement_virtuel
(
    date_calcul                     timestamp       NULL,
    date_lot                        date            NULL,
    nom_lot                         varchar(255)    NULL,
    etapeacv                        varchar(255)    NULL,
    critere                         varchar(255)    NULL,
    nom_organisation                varchar(255)    NULL,
    nom_source_donnee               varchar(255)    NULL,
    nom_equipement                  varchar(255)    NULL,
    nom_equipement_virtuel          varchar(255)    NULL,
    nom_entite                      varchar(255)    NULL,
    "source"                        varchar(255)    NULL,
    statut_indicateur               varchar(255)    NULL,
    trace                           text            NULL,
    version_calcul                  varchar(255)    NULL,
    impact_unitaire                 float8          NULL,
    unite                           varchar(255)    NULL,
    conso_elec_moyenne              float8          NULL,
    "cluster"                       varchar(255)    NULL,
    qualite                         varchar(255)    NULL,
    date_lot_discriminator          date            NOT NULL DEFAULT('1970-01-01'),
    nom_organisation_discriminator  varchar(255)    NOT NULL DEFAULT(''),
    nom_entite_discriminator        varchar(255)    NOT NULL DEFAULT(''),
    nom_source_donnee_discriminator varchar(255)    NOT NULL DEFAULT('')
);

CREATE TABLE IF NOT EXISTS ind_indicateur_impact_application
(
    date_calcul                     timestamp       NULL,
    date_lot                        date            NULL,
    nom_lot                         varchar(255)    NULL,
    etapeacv                        varchar(255)    NULL,
    critere                         varchar(255)    NULL,
    nom_organisation                varchar(255)    NULL,
    nom_source_donnee               varchar(255)    NULL,
    nom_application                 varchar(255)    NULL,
    type_environnement              varchar(255)    NULL,
    nom_equipement_physique         varchar(255)    NULL,
    nom_equipement_virtuel          varchar(255)    NULL,
    nom_entite                      varchar(255)    NULL,
    "source"                        varchar(255)    NULL,
    statut_indicateur               varchar(255)    NULL,
    trace                           text            NULL,
    version_calcul                  varchar(255)    NULL,
    domaine                         varchar(255)    NULL,
    sous_domaine                    varchar(255)    NULL,
    impact_unitaire                 float8          NULL,
    unite                           varchar(255)    NULL,
    conso_elec_moyenne              float8          NULL,
    qualite                         varchar(255)    NULL,
    date_lot_discriminator          date            NOT NULL DEFAULT('1970-01-01'),
    nom_organisation_discriminator  varchar(255)    NOT NULL DEFAULT(''),
    nom_entite_discriminator        varchar(255)    NOT NULL DEFAULT(''),
    nom_source_donnee_discriminator varchar(255)    NOT NULL DEFAULT('')
);

CREATE TABLE IF NOT EXISTS ind_indicateur_impact_operation_non_it
(
    date_calcul                     timestamp        NULL,
    date_lot                        date             NULL,
    nom_lot                         varchar(255)     NULL,
    etapeacv                        varchar(255)     NULL,
    critere                         varchar(255)     NULL,
    "source"                        varchar(255)     NULL,
    statut_indicateur               varchar(255)     NULL,
    trace                           text             NULL,
    version_calcul                  varchar(255)     NULL,
    conso_elec_moyenne              float8           NULL,
    impact_unitaire                 float8           NULL,
    quantite                        int4             NULL,
    type_item                       varchar(255)     NULL,
    unite                           varchar(255)     NULL,
    nom_entite                      varchar(255)     NULL,
    nom_organisation                varchar(255)     NULL,
    nom_source_donnee               varchar(255)     NULL,
    nom_item_non_it                 varchar(255)     NULL,
    qualite                         varchar(255)     NULL,
    date_lot_discriminator          date             NOT NULL DEFAULT('1970-01-01'),
    nom_organisation_discriminator  varchar(255)     NOT NULL DEFAULT(''),
    nom_entite_discriminator        varchar(255)     NOT NULL DEFAULT(''),
    nom_source_donnee_discriminator varchar(255)     NOT NULL DEFAULT('')
);

CREATE TABLE IF NOT EXISTS ind_indicateur_impact_messagerie
(
    date_calcul                         timestamp       NULL,
    date_lot                            date            NULL,
    nom_lot                             varchar(255)    NULL,
    nom_organisation                    varchar(255)    NULL,
    nom_source_donnee                   varchar(255)    NULL,
    critere                             varchar(255)    NULL,
    mois_annee                          date            NULL,
    nom_entite                          varchar(255)    NULL,
    "source"                            varchar(255)    NULL,
    statut_indicateur                   varchar(255)    NULL,
    trace                               text            NULL,
    version_calcul                      varchar(255)    NULL,
    impact_mensuel                      float8          NULL,
    unite                               varchar(255)    NULL,
    nombre_mail_emis                    float8          NULL,
    volume_total_mail_emis              float8          NULL,
    date_lot_discriminator              date            NOT NULL DEFAULT('1970-01-01'),
    nom_organisation_discriminator      varchar(255)    NOT NULL DEFAULT(''),
    nom_entite_discriminator            varchar(255)    NOT NULL DEFAULT(''),
    nom_source_donnee_discriminator     varchar(255)    NOT NULL DEFAULT('')
);

ALTER TABLE IF EXISTS ind_indicateur_impact_equipement_physique ADD COLUMN IF NOT EXISTS qualite varchar(255)  NULL DEFAULT '';
ALTER TABLE IF EXISTS ind_indicateur_impact_application ADD COLUMN IF NOT EXISTS qualite varchar(255)  NULL DEFAULT '';
ALTER TABLE IF EXISTS ind_indicateur_impact_equipement_virtuel ADD COLUMN IF NOT EXISTS qualite varchar(255)  NULL DEFAULT '';
ALTER TABLE IF EXISTS ind_indicateur_impact_operation_non_it ADD COLUMN IF NOT EXISTS qualite varchar(255)  NULL DEFAULT '';
ALTER TABLE IF EXISTS ind_indicateur_impact_operation_non_it ADD COLUMN IF NOT EXISTS date_lot_discriminator date NOT NULL DEFAULT('1970-01-01');
ALTER TABLE IF EXISTS ind_indicateur_impact_operation_non_it ADD COLUMN IF NOT EXISTS nom_organisation_discriminator varchar(255) NOT NULL DEFAULT('');
ALTER TABLE IF EXISTS ind_indicateur_impact_operation_non_it ADD COLUMN IF NOT EXISTS nom_entite_discriminator varchar(255) NOT NULL DEFAULT('');
ALTER TABLE IF EXISTS ind_indicateur_impact_operation_non_it ADD COLUMN IF NOT EXISTS nom_source_donnee_discriminator varchar(255) NOT NULL DEFAULT('');

CREATE INDEX IF NOT EXISTS idx_ind_eq_p__nom_lot_nom_equipement   ON ind_indicateur_impact_equipement_physique (nom_lot, nom_equipement);
CREATE INDEX IF NOT EXISTS idx_ind_eq_v__nom_lot_nom_equipement   ON ind_indicateur_impact_equipement_virtuel (nom_lot, nom_equipement);
CREATE INDEX IF NOT EXISTS idx_ind_app__nom_lot_nom_equipement    ON ind_indicateur_impact_application (nom_lot, nom_equipement_physique);
CREATE INDEX IF NOT EXISTS idx_ind_operation_non_it__nom_lot_nom_item_non_it ON ind_indicateur_impact_operation_non_it (nom_lot, nom_item_non_it);

-- Lignes a supprimer dans les futures versions
ALTER TABLE ind_indicateur_impact_equipement_virtuel ADD COLUMN IF NOT EXISTS nom_equipement_virtuel VARCHAR(255);
ALTER TABLE ind_indicateur_impact_application        ADD COLUMN IF NOT EXISTS nom_equipement_virtuel VARCHAR(255);
DROP TABLE IF EXISTS ind_indicateur_impact_reseau;
