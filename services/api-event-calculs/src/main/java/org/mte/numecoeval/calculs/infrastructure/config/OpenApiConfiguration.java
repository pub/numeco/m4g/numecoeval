package org.mte.numecoeval.calculs.infrastructure.config;

import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenApiConfiguration {

    @Bean
    GroupedOpenApi calculsApi() {
        return GroupedOpenApi.builder().group("calculs").pathsToMatch("/calculs/**").build();
    }

    @Bean
    GroupedOpenApi syncApi() {
        return GroupedOpenApi.builder().group("sync").pathsToMatch("/sync/**").build();
    }

    @Bean
    GroupedOpenApi indicateurApi() {
        return GroupedOpenApi.builder().group("indicateurs").pathsToMatch("/indicateur/**").build();
    }
}
