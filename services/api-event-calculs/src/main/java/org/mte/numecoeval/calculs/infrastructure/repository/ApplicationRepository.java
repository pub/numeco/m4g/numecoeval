package org.mte.numecoeval.calculs.infrastructure.repository;

import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.calculs.domain.data.entree.Application;
import org.mte.numecoeval.calculs.domain.exception.DatabaseException;
import org.mte.numecoeval.common.utils.ResultSetUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class ApplicationRepository {

    @Autowired
    private DataSource dataSource;

    public List<Application> findApplications(String nomOrganisation, String nomLot, String nomEquipementPhysique, String nomEquipementVirtuel) {
        List<Application> result = new ArrayList<>();

        try (Connection conn = dataSource.getConnection()) {
            try (var ps = conn.prepareStatement("""
                    SELECT *
                    FROM en_application
                    WHERE
                        nullif(nom_lot, ?) is null
                        AND nom_equipement_virtuel = ?
                        AND nom_equipement_physique = ?
                        AND nullif(nom_organisation, ?) is null
                    """)) {

                ps.setString(1, nomLot);
                ps.setString(2, nomEquipementVirtuel);
                ps.setString(3, nomEquipementPhysique);
                ps.setString(4, nomOrganisation);

                var rs = ps.executeQuery();
                while (rs.next()) {
                    result.add(Application.builder()
                            .nomLot(rs.getString("nom_lot"))
                            .dateLot(ResultSetUtils.getLocalDate(rs, "date_lot"))
                            .nomOrganisation(rs.getString("nom_organisation"))
                            .nomEntite(rs.getString("nom_entite"))
                            .nomEquipementPhysique(rs.getString("nom_equipement_physique"))
                            .nomEquipementVirtuel(rs.getString("nom_equipement_virtuel"))
                            .nomApplication(rs.getString("nom_application"))
                            .typeEnvironnement(rs.getString("type_environnement"))
                            .domaine(rs.getString("domaine"))
                            .sousDomaine(rs.getString("sous_domaine"))
                            .nomSourceDonnee(rs.getString("nom_source_donnee"))
                            .qualite(rs.getString("qualite"))
                            .build());
                }
            }
        } catch (SQLException e) {
            log.error("Une erreur s'est produite lors de la selection dans PostgreSQL. Exception: ", e);
            throw new DatabaseException(e);
        }

        return result;
    }
}
