package org.mte.numecoeval.calculs.infrastructure.controller.rest.calculs;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.calculs.domain.port.input.service.DureeDeVieEquipementPhysiqueService;
import org.mte.numecoeval.calculs.domain.port.input.service.impl.CalculImpactEquipementPhysiqueServiceImpl;
import org.mte.numecoeval.calculs.infrastructure.mapper.DomainMapper;
import org.mte.numecoeval.calculs.infrastructure.service.rest.calculs.CheckIndicateurService;
import org.mte.numecoeval.calculs.rest.generated.api.model.DemandeCalculImpactEquipementPhysiqueRest;
import org.mte.numecoeval.calculs.rest.generated.api.model.IndicateurImpactEquipementPhysiqueRest;
import org.mte.numecoeval.calculs.rest.generated.api.server.CalculsEquipementPhysiqueApi;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@AllArgsConstructor
public class CalculsEquipementPhysiqueController implements CalculsEquipementPhysiqueApi {

    DomainMapper domainMapper;

    ObjectMapper objectMapper;

    DureeDeVieEquipementPhysiqueService dureeDeVieEquipementPhysiqueService;

    CheckIndicateurService checkIndicateurService;

    /**
     * POST /calculs/equipementPhysique
     *
     * @param demandeCalculImpactEquipementPhysiqueRest (required)
     * @return la liste d'indicateurs calcules
     */
    @Override
    public ResponseEntity<IndicateurImpactEquipementPhysiqueRest> calculerImpactEquipementPhysique(DemandeCalculImpactEquipementPhysiqueRest demandeCalculImpactEquipementPhysiqueRest) {
        var demandeCalcul = domainMapper.toDomain(demandeCalculImpactEquipementPhysiqueRest);

        var indicateur = new CalculImpactEquipementPhysiqueServiceImpl(
                dureeDeVieEquipementPhysiqueService
        ).calculerImpactEquipementPhysique(demandeCalcul);

        return checkIndicateurService.isOk(indicateur.getStatutIndicateur()) ?
                ResponseEntity.ok(domainMapper.toRest(indicateur)) :
                ResponseEntity.badRequest().body(domainMapper.toRest(indicateur));
    }
}
