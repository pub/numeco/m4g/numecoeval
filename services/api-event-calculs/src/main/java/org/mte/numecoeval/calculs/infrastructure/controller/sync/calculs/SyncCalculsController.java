package org.mte.numecoeval.calculs.infrastructure.controller.sync.calculs;

import lombok.AllArgsConstructor;
import org.mte.numecoeval.calculs.infrastructure.service.sync.calculs.SyncCalculService;
import org.mte.numecoeval.calculs.sync.generated.api.model.ReponseCalculRest;
import org.mte.numecoeval.calculs.sync.generated.api.model.SyncCalculRest;
import org.mte.numecoeval.calculs.sync.generated.api.server.CalculsEquipementPhysiqueEtOperationNonItEtMessagerieApi;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class SyncCalculsController implements CalculsEquipementPhysiqueEtOperationNonItEtMessagerieApi {

    SyncCalculService syncCalculService;

    /**
     * POST /sync/calcul
     *
     * @param syncCalculRest (required)
     * @return la liste d'indicateurs calcules
     */
    @Override
    public ResponseEntity<ReponseCalculRest> syncCalculByIds(SyncCalculRest syncCalculRest) {
        return ResponseEntity.ok(syncCalculService.calcul(syncCalculRest));
    }
}
