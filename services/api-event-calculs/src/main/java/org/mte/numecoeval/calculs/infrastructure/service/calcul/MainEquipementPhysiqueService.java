package org.mte.numecoeval.calculs.infrastructure.service.calcul;

import lombok.AllArgsConstructor;
import org.mte.numecoeval.calculs.domain.model.CalculSizes;
import org.mte.numecoeval.calculs.infrastructure.service.enrichissement.EnrichissementEquipementPhysiqueService;
import org.mte.numecoeval.topic.data.EquipementPhysiqueDTO;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class MainEquipementPhysiqueService {

    private EnrichissementEquipementPhysiqueService enrichissementEquipementPhysiqueService;
    private IntegrationCalculEquipementsService integrationCalculEquipementsService;

    public CalculSizes calcul(EquipementPhysiqueDTO equipementPhysiqueDTO) {

        var eqPhEnrichi = enrichissementEquipementPhysiqueService.serviceEnrichissementEquipementPhysique(equipementPhysiqueDTO);
        return integrationCalculEquipementsService.calculImpactEquipementPhysique(eqPhEnrichi);

    }

}
