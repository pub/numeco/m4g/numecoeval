package org.mte.numecoeval.calculs.infrastructure.repository;

import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.calculs.domain.exception.DatabaseException;
import org.mte.numecoeval.common.utils.ResultSetUtils;
import org.mte.numecoeval.topic.data.MessagerieDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class MessagerieRepository {

    @Autowired
    private DataSource dataSource;

    public List<MessagerieDTO> findMessagerieDTOList(List<Long> ids) {
        List<MessagerieDTO> result = new ArrayList<>();

        try (Connection conn = dataSource.getConnection()) {
            try (var ps = conn.prepareStatement("""
                        SELECT *
                        FROM en_messagerie mes
                        WHERE id = ANY (?)
                    """)) {

                ps.setArray(1, conn.createArrayOf("long", ids.toArray(new Long[0])));

                var rs = ps.executeQuery();
                while (rs.next()) {
                    result.add(MessagerieDTO.builder()
                            .id(rs.getLong("id"))
                            .nomLot(rs.getString("nom_lot"))
                            .dateLot(ResultSetUtils.getLocalDate(rs, "date_lot"))
                            .nomOrganisation(rs.getString("nom_organisation"))
                            .nomEntite(rs.getString("nom_entite"))
                            .nomSourceDonnee(rs.getString("nom_source_donnee"))
                            .moisAnnee(rs.getInt("mois_annee"))
                            .nombreMailEmis(ResultSetUtils.getInteger(rs, "nombre_mail_emis"))
                            .nombreMailEmisXDestinataires(ResultSetUtils.getInteger(rs, "nombre_mail_emisxdestinataires"))
                            .volumeTotalMailEmis(ResultSetUtils.getInteger(rs, "volume_total_mail_emis"))
                            .build());
                }
            }
        } catch (SQLException e) {
            log.error("Une erreur s'est produite lors de la selection dans PostgreSQL. Exception: ", e);
            throw new DatabaseException(e);
        }

        return result;
    }

    public void setStatutToTraite(Long id) {
        try (Connection conn = dataSource.getConnection()) {
            try (var ps = conn.prepareStatement("""
                    UPDATE en_messagerie
                    SET statut_traitement = 'TRAITE', date_update = now()
                    WHERE id = ?
                    """)) {
                ps.setLong(1, id);
                ps.execute();
            }
        } catch (SQLException e) {
            log.error("Une erreur s'est produite lors de la mise a jour dans PostgreSQL. Exception: ", e);
            throw new DatabaseException(e);
        }
    }
}
