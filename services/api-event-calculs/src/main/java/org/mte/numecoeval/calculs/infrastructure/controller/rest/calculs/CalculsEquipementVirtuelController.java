package org.mte.numecoeval.calculs.infrastructure.controller.rest.calculs;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.mte.numecoeval.calculs.domain.port.input.service.impl.CalculImpactEquipementVirtuelServiceImpl;
import org.mte.numecoeval.calculs.infrastructure.mapper.DomainMapper;
import org.mte.numecoeval.calculs.infrastructure.service.rest.calculs.CheckIndicateurService;
import org.mte.numecoeval.calculs.rest.generated.api.model.DemandeCalculEquipementVirtuelRest;
import org.mte.numecoeval.calculs.rest.generated.api.model.IndicateurImpactEquipementVirtuelRest;
import org.mte.numecoeval.calculs.rest.generated.api.server.CalculsEquipementVirtuelApi;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class CalculsEquipementVirtuelController implements CalculsEquipementVirtuelApi {

    DomainMapper domainMapper;

    ObjectMapper objectMapper;

    CheckIndicateurService checkIndicateurService;

    /**
     * POST /calculs/equipementVirtuel
     *
     * @param demandeCalculEquipementVirtuelRest (required)
     * @return la liste d'indicateurs calcules
     */
    @Override
    public ResponseEntity<IndicateurImpactEquipementVirtuelRest> calculerImpactEquipementVirtuel(DemandeCalculEquipementVirtuelRest demandeCalculEquipementVirtuelRest) {
        var demandeCalcul = domainMapper.toDomain(demandeCalculEquipementVirtuelRest);

        var indicateur = new CalculImpactEquipementVirtuelServiceImpl().calculerImpactEquipementVirtuel(demandeCalcul);

        return checkIndicateurService.isOk(indicateur.getStatutIndicateur()) ?
                ResponseEntity.ok(domainMapper.toRest(indicateur)) :
                ResponseEntity.badRequest().body(domainMapper.toRest(indicateur));
    }
}
