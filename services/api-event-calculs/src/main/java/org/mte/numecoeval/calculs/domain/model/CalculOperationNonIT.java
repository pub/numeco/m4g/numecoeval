package org.mte.numecoeval.calculs.domain.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.mte.numecoeval.calculs.referentiels.generated.api.model.*;
import org.mte.numecoeval.topic.data.OperationNonITDTO;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class CalculOperationNonIT {
    private List<EtapeDTO> etapes;
    private List<CritereDTO> criteres;
    private List<HypotheseDTO> hypotheses;
    private OperationNonITDTO operationNonIT;
    private TypeItemDTO typeItem;
    private List<FacteurCaracterisationDTO> facteurCaracterisations;
}