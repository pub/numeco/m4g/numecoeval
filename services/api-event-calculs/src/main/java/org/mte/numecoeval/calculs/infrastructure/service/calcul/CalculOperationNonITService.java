package org.mte.numecoeval.calculs.infrastructure.service.calcul;

import lombok.AllArgsConstructor;
import org.mte.numecoeval.calculs.domain.data.demande.DemandeCalculImpactOperationNonIT;
import org.mte.numecoeval.calculs.domain.data.indicateurs.ImpactOperationNonIT;
import org.mte.numecoeval.calculs.domain.model.CalculOperationNonIT;
import org.mte.numecoeval.calculs.domain.port.input.service.CalculImpactOperationNonITService;
import org.mte.numecoeval.calculs.infrastructure.mapper.EntreesMapper;
import org.mte.numecoeval.calculs.infrastructure.mapper.ReferentielMapper;
import org.mte.numecoeval.calculs.referentiels.generated.api.model.CritereDTO;
import org.mte.numecoeval.calculs.referentiels.generated.api.model.EtapeDTO;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class CalculOperationNonITService {

    EntreesMapper entreesMapper;

    ReferentielMapper referentielMapper;

    CalculImpactOperationNonITService calculImpactOperationNonITService;

    /**
     * Calcule les impacts d'une opération non IT
     * Returne la liste d'impacts en iterant par (etape, critere)
     *
     * @param calculOperationNonIT l'opération non IT enrichi
     * @return la liste d'impacts
     */
    public List<ImpactOperationNonIT> calculImpactOperationNonIT(CalculOperationNonIT calculOperationNonIT) {

        LocalDateTime dateCalcul = LocalDateTime.now();

        List<ImpactOperationNonIT> impactOperationNonITList = new ArrayList<>();

        for (EtapeDTO etapeDTO : calculOperationNonIT.getEtapes()) {
            for (CritereDTO critereDTO : calculOperationNonIT.getCriteres()) {

                DemandeCalculImpactOperationNonIT demandeCalculImpactOperationNonIT = DemandeCalculImpactOperationNonIT.builder()
                        .dateCalcul(dateCalcul)
                        .operationNonIT(entreesMapper.toDomain(calculOperationNonIT.getOperationNonIT()))
                        .etape(referentielMapper.toEtape(etapeDTO))
                        .critere(referentielMapper.toCritere(critereDTO))
                        .typeItem(referentielMapper.toTypeItem(calculOperationNonIT.getTypeItem()))
                        .hypotheses(referentielMapper.toListHypothese(calculOperationNonIT.getHypotheses()))
                        .facteurCaracterisations(referentielMapper.toListFacteurCaracterisation(calculOperationNonIT.getFacteurCaracterisations()))
                        .build();
                var impact = calculImpactOperationNonITService.calculerImpactOperationNonIT(demandeCalculImpactOperationNonIT);
                if (impact != null) {
                    impactOperationNonITList.add(impact);
                }
            }
        }
        return impactOperationNonITList;

    }
}
