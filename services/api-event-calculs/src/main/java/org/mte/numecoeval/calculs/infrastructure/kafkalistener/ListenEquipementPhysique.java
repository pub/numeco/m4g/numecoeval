package org.mte.numecoeval.calculs.infrastructure.kafkalistener;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.calculs.infrastructure.service.calcul.MainEquipementPhysiqueService;
import org.mte.numecoeval.topic.data.EquipementPhysiqueDTO;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@AllArgsConstructor
public class ListenEquipementPhysique {

    private MainEquipementPhysiqueService mainEquipementPhysiqueService;

    @KafkaListener(topics = "${numecoeval.topic.equipementPhysique}", concurrency = "${numecoeval.topic.partition}")
    public void consume(EquipementPhysiqueDTO equipementPhysiqueDTO) {
        mainEquipementPhysiqueService.calcul(equipementPhysiqueDTO);
    }
}