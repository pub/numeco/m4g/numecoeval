package org.mte.numecoeval.calculs.infrastructure.controller.rest.calculs;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.calculs.domain.port.input.service.impl.CalculImpactOperationNonITServiceImpl;
import org.mte.numecoeval.calculs.infrastructure.mapper.DomainMapper;
import org.mte.numecoeval.calculs.infrastructure.service.rest.calculs.CheckIndicateurService;
import org.mte.numecoeval.calculs.rest.generated.api.model.DemandeCalculImpactOperationNonITRest;
import org.mte.numecoeval.calculs.rest.generated.api.model.IndicateurImpactOperationNonITRest;
import org.mte.numecoeval.calculs.rest.generated.api.server.CalculsOperationNonItApi;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@AllArgsConstructor
public class CalculsOperationNonITController implements CalculsOperationNonItApi {

    DomainMapper domainMapper;

    CheckIndicateurService checkIndicateurService;

    /**
     * POST /calculs/operationNonIT
     *
     * @param demandeCalculImpactOperationNonITRest (required)
     * @return la liste d'indicateurs calcules
     */
    @Override
    public ResponseEntity<IndicateurImpactOperationNonITRest> calculerImpactOperationNonIT(DemandeCalculImpactOperationNonITRest demandeCalculImpactOperationNonITRest) {
        var demandeCalcul = domainMapper.toDomain(demandeCalculImpactOperationNonITRest);

        var indicateur = new CalculImpactOperationNonITServiceImpl().calculerImpactOperationNonIT(demandeCalcul);

        return checkIndicateurService.isOk(indicateur.getStatutIndicateur()) ?
                ResponseEntity.ok(domainMapper.toRest(indicateur)) :
                ResponseEntity.badRequest().body(domainMapper.toRest(indicateur));
    }
}
