package org.mte.numecoeval.calculs.infrastructure.controller.rest.calculs;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.mte.numecoeval.calculs.domain.port.input.service.impl.CalculImpactMessagerieServiceImpl;
import org.mte.numecoeval.calculs.infrastructure.mapper.DomainMapper;
import org.mte.numecoeval.calculs.infrastructure.service.rest.calculs.CheckIndicateurService;
import org.mte.numecoeval.calculs.rest.generated.api.model.DemandeCalculMessagerieRest;
import org.mte.numecoeval.calculs.rest.generated.api.model.IndicateurImpactMessagerieRest;
import org.mte.numecoeval.calculs.rest.generated.api.server.CalculsMessagerieApi;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class CalculsMessagerieController implements CalculsMessagerieApi {

    DomainMapper domainMapper;

    ObjectMapper objectMapper;

    CheckIndicateurService checkIndicateurService;

    /**
     * POST /calculs/messagerie
     *
     * @param demandeCalculMessagerieRest (required)
     * @return la liste d'indicateurs calcules
     */
    @Override
    public ResponseEntity<IndicateurImpactMessagerieRest> calculerImpactMessagerie(DemandeCalculMessagerieRest demandeCalculMessagerieRest) {
        var demandeCalcul = domainMapper.toDomain(demandeCalculMessagerieRest);

        var indicateur = new CalculImpactMessagerieServiceImpl().calculerImpactMessagerie(demandeCalcul);

        return checkIndicateurService.isOk(indicateur.getStatutIndicateur()) ?
                ResponseEntity.ok(domainMapper.toRest(indicateur)) :
                ResponseEntity.badRequest().body(domainMapper.toRest(indicateur));
    }
}
