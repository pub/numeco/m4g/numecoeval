package org.mte.numecoeval.calculs.infrastructure.service.calcul;

import lombok.AllArgsConstructor;
import org.mte.numecoeval.calculs.domain.model.CalculSizes;
import org.mte.numecoeval.calculs.infrastructure.service.enrichissement.EnrichissementMessagerieService;
import org.mte.numecoeval.topic.data.MessagerieDTO;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class MainMessagerieService {

    private EnrichissementMessagerieService enrichissementMessagerieService;
    private IntegrationCalculMessagerieService integrationCalculMessagerieService;

    public CalculSizes calcul(MessagerieDTO messagerieDTO) {

        var messagerieEnrichie = enrichissementMessagerieService.serviceEnrichissementMessagerie(messagerieDTO);
        return integrationCalculMessagerieService.calculImpactMessagerie(messagerieEnrichie);

    }

}
