package org.mte.numecoeval.calculs.infrastructure.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.listener.DefaultErrorHandler;
import org.springframework.util.backoff.FixedBackOff;

@Configuration
@Slf4j
public class KafkaConfiguration {

    @Value("${spring.kafka.back-off-sec}")
    private String backOffSec;

    @Bean
    DefaultErrorHandler defaultErrorHandler() {
        return new DefaultErrorHandler(
                (rec, ex) -> log.error("Erreur kafka: {}", rec, ex),
                new FixedBackOff(Long.parseLong(backOffSec) * 1000, Integer.MAX_VALUE)
        );
    }

}
