package org.mte.numecoeval.calculs.infrastructure.service.calcul;

import lombok.AllArgsConstructor;
import org.mte.numecoeval.calculs.domain.data.demande.DemandeCalculImpactApplication;
import org.mte.numecoeval.calculs.domain.data.entree.Application;
import org.mte.numecoeval.calculs.domain.data.indicateurs.ImpactApplication;
import org.mte.numecoeval.calculs.domain.data.indicateurs.ImpactEquipementVirtuel;
import org.mte.numecoeval.calculs.domain.port.input.service.CalculImpactApplicationService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class CalculApplicationService {

    CalculImpactApplicationService calculImpactApplicationService;

    /**
     * Calcule les impacts d'une application
     * Returne la liste d'impacts à partir des impacts de son equipement virtuel
     *
     * @param impactEquipementVirtuelList la liste d'impact de l'equipement virtuel
     * @param application                 l'application
     * @param nbApplications              nombre d'application de l'équipement virtuel
     * @return la liste d'impacts
     */
    public List<ImpactApplication> calculImpactApplication(List<ImpactEquipementVirtuel> impactEquipementVirtuelList, Application application, Integer nbApplications) {

        if (impactEquipementVirtuelList.isEmpty()) return new ArrayList<>();

        LocalDateTime dateCalcul = LocalDateTime.now();
        return impactEquipementVirtuelList.stream()
                .map(impact -> calculImpactApplicationService.calculImpactApplicatif(
                        DemandeCalculImpactApplication.builder()
                                .dateCalcul(dateCalcul)
                                .application(application)
                                .nbApplications(nbApplications)
                                .impactEquipementVirtuel(impact)
                                .build()))
                .toList();
    }
}
