package org.mte.numecoeval.calculs.infrastructure.repository;

import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.calculs.domain.exception.DatabaseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class DonneesEntreeRepository {

    @Autowired
    private DataSource dataSource;

    @Cacheable("DureeUsage")
    public String findDureeUsage(String nomLot, String nomOrganisation) {
        List<String> result = new ArrayList<>();
        try (Connection conn = dataSource.getConnection()) {
            try (var ps = conn.prepareStatement("""
                     SELECT duree_usage
                     FROM en_donnees_entrees
                     WHERE nom_lot = ? AND nom_organisation= ?
                    """)) {

                ps.setString(1, nomLot);
                ps.setString(2, nomOrganisation);
                var rs = ps.executeQuery();
                while (rs.next()) {
                    result.add(rs.getString("duree_usage"));
                }
            }
        } catch (SQLException e) {
            log.error("Une erreur s'est produite lors de la selection dans PostgreSQL. Exception: ", e);
            throw new DatabaseException(e);
        }
        if (result.isEmpty()) {
            return null;
        }
        return result.getFirst();
    }

    @Cacheable("EtapesFiltrees")
    public String findEtapes(String nomLot) {
        List<String> result = new ArrayList<>();
        try (Connection conn = dataSource.getConnection()) {
            try (var ps = conn.prepareStatement("""
                     SELECT etapes
                     FROM en_donnees_entrees
                     WHERE nom_lot = ?
                    """)) {

                ps.setString(1, nomLot);
                var rs = ps.executeQuery();
                while (rs.next()) {
                    result.add(rs.getString("etapes"));
                }
            }
        } catch (SQLException e) {
            log.error("Une erreur s'est produite lors de la selection dans PostgreSQL. Exception: ", e);
            throw new DatabaseException(e);
        }
        if (result.isEmpty()) {
            return null;
        }
        return result.getFirst();
    }

    @Cacheable("CriteresFiltres")
    public String findCriteres(String nomLot) {
        List<String> result = new ArrayList<>();
        try (Connection conn = dataSource.getConnection()) {
            try (var ps = conn.prepareStatement("""
                     SELECT criteres
                     FROM en_donnees_entrees
                     WHERE nom_lot = ?
                    """)) {

                ps.setString(1, nomLot);
                var rs = ps.executeQuery();
                while (rs.next()) {
                    result.add(rs.getString("criteres"));
                }
            }
        } catch (SQLException e) {
            log.error("Une erreur s'est produite lors de la selection dans PostgreSQL. Exception: ", e);
            throw new DatabaseException(e);
        }
        if (result.isEmpty()) {
            return null;
        }
        return result.getFirst();
    }

    @Cacheable("NomOrganisation")
    public String findNomOrganisation(String nomLot) {
        List<String> result = new ArrayList<>();
        try (Connection conn = dataSource.getConnection()) {
            try (var ps = conn.prepareStatement("""
                     SELECT nom_organisation
                     FROM en_donnees_entrees
                     WHERE nom_lot = ?
                    """)) {

                ps.setString(1, nomLot);
                var rs = ps.executeQuery();
                while (rs.next()) {
                    result.add(rs.getString("nom_organisation"));
                }
            }
        } catch (SQLException e) {
            log.error("Une erreur s'est produite lors de la selection dans PostgreSQL. Exception: ", e);
            throw new DatabaseException(e);
        }
        if (result.isEmpty()) {
            return null;
        }
        return result.getFirst();
    }
}
