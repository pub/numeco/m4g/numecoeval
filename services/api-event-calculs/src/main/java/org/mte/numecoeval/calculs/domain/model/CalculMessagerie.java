package org.mte.numecoeval.calculs.domain.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.mte.numecoeval.calculs.referentiels.generated.api.model.CritereDTO;
import org.mte.numecoeval.calculs.referentiels.generated.api.model.ImpactMessagerieDTO;
import org.mte.numecoeval.topic.data.MessagerieDTO;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class CalculMessagerie {
    private MessagerieDTO messagerie;
    private List<CritereDTO> criteres;
    private List<ImpactMessagerieDTO> impactsMessagerie;
}

