package org.mte.numecoeval.calculs.domain.model;

import lombok.Data;

@Data
public class CalculSizes {
    private long nbEquipementPhysique;
    private long nbEquipementVirtuel;
    private long nbApplication;
    private long nbOperationNonIT;
    private long nbMessagerie;
    private long nbIndicateurEquipementPhysique;
    private long nbIndicateurEquipementVirtuel;
    private long nbIndicateurApplication;
    private long nbIndicateurOperationNonIT;
    private long nbIndicateurMessagerie;
}
