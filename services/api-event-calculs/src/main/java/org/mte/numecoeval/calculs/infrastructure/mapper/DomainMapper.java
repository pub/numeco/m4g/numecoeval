package org.mte.numecoeval.calculs.infrastructure.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mte.numecoeval.calculs.domain.data.demande.*;
import org.mte.numecoeval.calculs.domain.data.entree.*;
import org.mte.numecoeval.calculs.domain.data.indicateurs.*;
import org.mte.numecoeval.calculs.domain.data.referentiel.*;
import org.mte.numecoeval.calculs.rest.generated.api.model.*;

@Mapper(componentModel = "spring")
public interface DomainMapper {

    /*Entrées*/
    EquipementPhysique toDomain(EquipementPhysiqueRest rest);

    DataCenter toDomain(DataCenterRest rest);

    EquipementVirtuel toDomain(EquipementVirtuelRest rest);

    Application toDomain(ApplicationRest rest);

    Messagerie toDomain(MessagerieRest rest);

    OperationNonIT toDomain(OperationNonITRest rest);

    /* Référentiels */

    ReferentielCorrespondanceRefEquipement toDomain(CorrespondanceRefEquipementRest rest);

    ReferentielHypothese toDomain(HypotheseRest rest);

    ReferentielEtapeACV toDomain(EtapeRest rest);

    ReferentielCritere toDomain(CritereRest rest);

    ReferentielImpactMessagerie toDomain(ImpactMessagerieRest rest);

    /*Indicateurs*/
    ImpactEquipementPhysique toDomain(IndicateurImpactEquipementPhysiqueRest rest);

    ImpactEquipementVirtuel toDomain(IndicateurImpactEquipementVirtuelRest rest);

    ImpactApplication toDomain(IndicateurImpactApplicationRest rest);

    IndicateurImpactEquipementPhysiqueRest toRest(ImpactEquipementPhysique domain);

    IndicateurImpactEquipementVirtuelRest toRest(ImpactEquipementVirtuel domain);

    IndicateurImpactApplicationRest toRest(ImpactApplication domain);

    IndicateurImpactOperationNonITRest toRest(ImpactOperationNonIT domain);

    IndicateurImpactMessagerieRest toRest(ImpactMessagerie domain);

    /*Demandes*/
    @Mapping(target = "dateCalcul", expression = "java(java.time.LocalDateTime.now())")
    DemandeCalculImpactEquipementPhysique toDomain(DemandeCalculImpactEquipementPhysiqueRest rest);

    @Mapping(target = "dateCalcul", expression = "java(java.time.LocalDateTime.now())")
    DemandeCalculImpactEquipementVirtuel toDomain(DemandeCalculEquipementVirtuelRest rest);

    @Mapping(target = "dateCalcul", expression = "java(java.time.LocalDateTime.now())")
    DemandeCalculImpactApplication toDomain(DemandeCalculApplicationRest rest);

    @Mapping(target = "dateCalcul", expression = "java(java.time.LocalDateTime.now())")
    DemandeCalculImpactOperationNonIT toDomain(DemandeCalculImpactOperationNonITRest rest);

    @Mapping(target = "dateCalcul", expression = "java(java.time.LocalDateTime.now())")
    DemandeCalculImpactMessagerie toDomain(DemandeCalculMessagerieRest rest);
}
