package org.mte.numecoeval.calculs.infrastructure.config;

import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.calculs.referentiels.generated.api.client.InterneNumEcoEvalApi;
import org.mte.numecoeval.calculs.referentiels.generated.api.invoker.ApiClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
@Slf4j
public class CommonIntegrationConfig {

    @Value("${numecoeval.referentiels.url}")
    String referentielUrl;

    @Bean
    public InterneNumEcoEvalApi clientAPIReferentiel() {
        InterneNumEcoEvalApi interneNumEcoEvalApi = new InterneNumEcoEvalApi();
        ApiClient apiClient = new ApiClient(WebClient.builder()
                .baseUrl(referentielUrl)
                .build());
        apiClient.setBasePath(referentielUrl);
        interneNumEcoEvalApi.setApiClient(apiClient);

        log.info("Création du client d'API Référentiel sur l'URL {}", referentielUrl);

        return interneNumEcoEvalApi;
    }
}
