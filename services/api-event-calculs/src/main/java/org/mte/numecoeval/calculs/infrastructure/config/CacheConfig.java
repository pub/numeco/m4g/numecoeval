package org.mte.numecoeval.calculs.infrastructure.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.concurrent.TimeUnit;

@Slf4j
@EnableCaching
@EnableScheduling
@Configuration
@Profile("!test")
public class CacheConfig {

    @CacheEvict(value = {
            "Etapes",
            "Criteres",
            "Hypothese",
            "TypeItem",
            "CorrespondanceRefEquipement",
            "FacteurCaracterisationMixElec",
            "FacteurCaracterisation",
            "ImpactOperationNonIT",
            "ImpactMessagerie",
            "DureeUsage",
            "NomOrganisation",
    }, allEntries = true)
    @Scheduled(fixedRateString = "${numecoeval.cache.ttl}", timeUnit = TimeUnit.MINUTES)
    public void emptyAllCaches() {
        log.info("Nettoyage de tous les caches internes");
    }

    @CacheEvict(value = {
            "EtapesFiltrees",
            "CriteresFiltres",
    }, allEntries = true)
    @Scheduled(fixedRateString = "${numecoeval.cache.donnesfiltrees}", timeUnit = TimeUnit.MINUTES)
    public void emptyDonneesFiltreesCaches() {
        log.info("Nettoyage des caches d'EtapesFiltrees et CriteresFiltres");
    }
}