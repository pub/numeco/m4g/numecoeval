package org.mte.numecoeval.calculs.infrastructure.service.calcul;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.calculs.domain.model.CalculMessagerie;
import org.mte.numecoeval.calculs.domain.model.CalculSizes;
import org.mte.numecoeval.calculs.infrastructure.repository.IndicateurRepository;
import org.mte.numecoeval.calculs.infrastructure.repository.MessagerieRepository;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@AllArgsConstructor
public class IntegrationCalculMessagerieService {

    IndicateurRepository indicateurRepository;

    CalculMessagerieService calculMessagerieService;

    MessagerieRepository messagerieRepository;

    public CalculSizes calculImpactMessagerie(CalculMessagerie calculMessagerie) {
        if (calculMessagerie == null) return null;

        var messagerie = calculMessagerie.getMessagerie();

        log.debug("{} - {} - {} : Calcul impact messagerie : {}, Nombre de Critère : {}",
                messagerie.getNomOrganisation(), messagerie.getNomLot(), messagerie.getDateLot(),
                messagerie.getId(),
                calculMessagerie.getCriteres().size()
        );

        var impactsMessagerie = calculMessagerieService.calculImpactEquipementVirtuel(calculMessagerie);
        indicateurRepository.saveIndicateursMessagerie(impactsMessagerie);
        messagerieRepository.setStatutToTraite(messagerie.getId());

        var result = new CalculSizes();
        result.setNbMessagerie(1);
        result.setNbIndicateurMessagerie(impactsMessagerie.size());
        return result;
    }
}
