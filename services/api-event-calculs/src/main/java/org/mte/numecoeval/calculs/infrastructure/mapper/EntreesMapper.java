package org.mte.numecoeval.calculs.infrastructure.mapper;

import org.mapstruct.Mapper;
import org.mte.numecoeval.calculs.domain.data.entree.DataCenter;
import org.mte.numecoeval.calculs.domain.data.entree.EquipementPhysique;
import org.mte.numecoeval.calculs.domain.data.entree.Messagerie;
import org.mte.numecoeval.calculs.domain.data.entree.OperationNonIT;
import org.mte.numecoeval.topic.data.DataCenterDTO;
import org.mte.numecoeval.topic.data.EquipementPhysiqueDTO;
import org.mte.numecoeval.topic.data.MessagerieDTO;
import org.mte.numecoeval.topic.data.OperationNonITDTO;

@Mapper(componentModel = "spring")
public interface EntreesMapper {

    EquipementPhysique toDomain(EquipementPhysiqueDTO dto);

    DataCenter toDomain(DataCenterDTO dto);

    OperationNonIT toDomain(OperationNonITDTO dto);

    Messagerie toDomain(MessagerieDTO messagerieDTO);

}
