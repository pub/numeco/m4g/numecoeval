package org.mte.numecoeval.calculs.infrastructure.kafkalistener;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.calculs.infrastructure.service.calcul.MainOperationNonITService;
import org.mte.numecoeval.topic.data.OperationNonITDTO;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@AllArgsConstructor
public class ListenOperationNonIT {

    private MainOperationNonITService mainOperationNonITService;

    @KafkaListener(topics = "${numecoeval.topic.operationNonIT}", concurrency = "${numecoeval.topic.partition}")
    public void consume(OperationNonITDTO OperationNonITDTO) {
        mainOperationNonITService.calcul(OperationNonITDTO);
    }
}