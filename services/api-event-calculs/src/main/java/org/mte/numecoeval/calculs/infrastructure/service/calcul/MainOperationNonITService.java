package org.mte.numecoeval.calculs.infrastructure.service.calcul;

import lombok.AllArgsConstructor;
import org.mte.numecoeval.calculs.domain.model.CalculSizes;
import org.mte.numecoeval.calculs.infrastructure.service.enrichissement.EnrichissementOperationNonITService;
import org.mte.numecoeval.topic.data.OperationNonITDTO;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class MainOperationNonITService {

    private EnrichissementOperationNonITService enrichissementOperationNonITService;
    private IntegrationCalculOperationNonITService integrationCalculOperationNonITService;

    public CalculSizes calcul(OperationNonITDTO operationNonITDTO) {

        var eqPhEnrichi = enrichissementOperationNonITService.serviceEnrichissementOperationNonIT(operationNonITDTO);
        return integrationCalculOperationNonITService.calculImpactOperationNonIT(eqPhEnrichi);

    }

}
