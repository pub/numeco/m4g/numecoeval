package org.mte.numecoeval.calculs.domain.exception;

public class DatabaseException extends RuntimeException {

    /**
     * Constructor with no arg
     */
    public DatabaseException() {
        super();
    }

    /**
     * Constructor with cause
     */
    public DatabaseException(Throwable cause) {
        super(cause);
    }
}
