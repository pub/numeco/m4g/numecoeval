package org.mte.numecoeval.calculs.infrastructure.service.calcul;

import lombok.AllArgsConstructor;
import org.mte.numecoeval.calculs.domain.data.demande.DemandeCalculImpactMessagerie;
import org.mte.numecoeval.calculs.domain.data.indicateurs.ImpactMessagerie;
import org.mte.numecoeval.calculs.domain.model.CalculMessagerie;
import org.mte.numecoeval.calculs.domain.port.input.service.CalculImpactMessagerieService;
import org.mte.numecoeval.calculs.infrastructure.mapper.EntreesMapper;
import org.mte.numecoeval.calculs.infrastructure.mapper.ReferentielMapper;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class CalculMessagerieService {

    EntreesMapper entreesMapper;
    ReferentielMapper referentielMapper;

    CalculImpactMessagerieService calculImpactMessagerieService;

    /**
     * Calcule les impacts d'une messagerie
     * Returne la liste d'impacts
     *
     * @param calculMessagerie la messagerie enrichie
     * @return la liste d'impacts
     */
    public List<ImpactMessagerie> calculImpactEquipementVirtuel(CalculMessagerie calculMessagerie) {

        if (calculMessagerie.getCriteres() == null) return new ArrayList<>();

        LocalDateTime dateCalcul = LocalDateTime.now();

        return calculMessagerie.getCriteres().stream()
                .map(critereDTO -> calculImpactMessagerieService.calculerImpactMessagerie(DemandeCalculImpactMessagerie.builder()
                        .dateCalcul(dateCalcul)
                        .messagerie(entreesMapper.toDomain(calculMessagerie.getMessagerie()))
                        .critere(referentielMapper.toCritere(critereDTO))
                        .impactsMessagerie(referentielMapper.toListImpactMessagerie(calculMessagerie.getImpactsMessagerie()))
                        .build())
                ).toList();

    }
}
