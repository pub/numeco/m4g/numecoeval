package org.mte.numecoeval.calculs.infrastructure.service.calcul;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.calculs.domain.data.indicateurs.ImpactOperationNonIT;
import org.mte.numecoeval.calculs.domain.model.CalculOperationNonIT;
import org.mte.numecoeval.calculs.domain.model.CalculSizes;
import org.mte.numecoeval.calculs.infrastructure.repository.IndicateurRepository;
import org.mte.numecoeval.calculs.infrastructure.repository.OperationNonITRepository;
import org.mte.numecoeval.topic.data.OperationNonITDTO;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@AllArgsConstructor
public class IntegrationCalculOperationNonITService {

    IndicateurRepository indicateurRepository;
    OperationNonITRepository operationNonITRepository;
    CalculOperationNonITService calculOperationNonITService;

    /**
     * Calcul les impacts d'une opération non IT
     *
     * @param calculOperationNonIT l'opération Non IT
     * @return le nombre d'elements traites sous forme CalculSizes
     */
    public CalculSizes calculImpactOperationNonIT(CalculOperationNonIT calculOperationNonIT) {

        if (calculOperationNonIT == null) return null;

        OperationNonITDTO operationNonITDTO = calculOperationNonIT.getOperationNonIT();
        String nomLot = operationNonITDTO.getNomLot();
        String nomOrganisation = operationNonITDTO.getNomOrganisation();
        String nomOperationNonIT = operationNonITDTO.getNomItemNonIT();

        // delete existing indicateurs
        indicateurRepository.clearIndicateursOperation(nomOperationNonIT, nomLot);

        List<ImpactOperationNonIT> impactOperationNonITList = calculOperationNonITService.calculImpactOperationNonIT(calculOperationNonIT);

        // save impacts into db
        indicateurRepository.saveIndicateursOperationNonIT(impactOperationNonITList);

        // set statut of the operation to TRAITE
        operationNonITRepository.setStatutToTraite(operationNonITDTO.getId());

        log.info("{} - {} - {} : Calcul impacts opérations non IT: {}",
                nomOrganisation, nomLot, operationNonITDTO.getDateLot(),
                nomOperationNonIT
        );

        CalculSizes result = new CalculSizes();
        result.setNbOperationNonIT(1);
        result.setNbIndicateurOperationNonIT(impactOperationNonITList.size());
        return result;
    }
}