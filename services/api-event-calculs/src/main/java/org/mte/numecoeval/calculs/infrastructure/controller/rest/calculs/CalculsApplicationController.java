package org.mte.numecoeval.calculs.infrastructure.controller.rest.calculs;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.mte.numecoeval.calculs.domain.port.input.service.impl.CalculImpactApplicationServiceImpl;
import org.mte.numecoeval.calculs.infrastructure.mapper.DomainMapper;
import org.mte.numecoeval.calculs.infrastructure.service.rest.calculs.CheckIndicateurService;
import org.mte.numecoeval.calculs.rest.generated.api.model.DemandeCalculApplicationRest;
import org.mte.numecoeval.calculs.rest.generated.api.model.IndicateurImpactApplicationRest;
import org.mte.numecoeval.calculs.rest.generated.api.server.CalculsApplicationApi;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class CalculsApplicationController implements CalculsApplicationApi {

    DomainMapper domainMapper;

    ObjectMapper objectMapper;

    CheckIndicateurService checkIndicateurService;

    /**
     * POST /calculs/application
     *
     * @param demandeCalculApplicationRest (required)
     * @return la liste d'indicateurs calcules
     */
    @Override
    public ResponseEntity<IndicateurImpactApplicationRest> calculerImpactApplication(DemandeCalculApplicationRest demandeCalculApplicationRest) {
        var demandeCalcul = domainMapper.toDomain(demandeCalculApplicationRest);

        var indicateur = new CalculImpactApplicationServiceImpl().calculImpactApplicatif(demandeCalcul);

        return checkIndicateurService.isOk(indicateur.getStatutIndicateur()) ?
                ResponseEntity.ok(domainMapper.toRest(indicateur)) :
                ResponseEntity.badRequest().body(domainMapper.toRest(indicateur));
    }
}
