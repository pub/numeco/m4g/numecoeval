package org.mte.numecoeval.calculs.infrastructure.service.enrichissement;

import lombok.AllArgsConstructor;
import org.mte.numecoeval.calculs.domain.model.CalculMessagerie;
import org.mte.numecoeval.calculs.infrastructure.client.ReferentielClient;
import org.mte.numecoeval.topic.data.MessagerieDTO;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Objects;

@Service
@AllArgsConstructor
public class EnrichissementMessagerieService {

    private ReferentielClient referentielClient;

    public CalculMessagerie serviceEnrichissementMessagerie(MessagerieDTO messagerieDTO) {

        if (Objects.isNull(messagerieDTO)) return null;

        CalculMessagerie calculMessagerie = new CalculMessagerie();
        calculMessagerie.setMessagerie(messagerieDTO);

        calculMessagerie.setCriteres(referentielClient.getCriteres());
        calculMessagerie.setImpactsMessagerie(new ArrayList<>());

        calculMessagerie.setImpactsMessagerie(calculMessagerie.getCriteres().stream()
                .map(critereDTO -> referentielClient.getMessagerie(critereDTO.getNomCritere()))
                .filter(Objects::nonNull)
                .toList());

        return calculMessagerie;
    }
}
