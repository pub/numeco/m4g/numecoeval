package org.mte.numecoeval.calculs.infrastructure.kafkalistener;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.calculs.infrastructure.service.calcul.MainMessagerieService;
import org.mte.numecoeval.topic.data.MessagerieDTO;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@AllArgsConstructor
public class ListenMessagerie {

    private MainMessagerieService mainMessagerieService;

    @KafkaListener(topics = "${numecoeval.topic.messagerie}")
    public void consume(MessagerieDTO messagerieDTO) {
        mainMessagerieService.calcul(messagerieDTO);
    }
}


