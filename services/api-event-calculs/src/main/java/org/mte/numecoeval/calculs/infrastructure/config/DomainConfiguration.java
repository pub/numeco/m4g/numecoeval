package org.mte.numecoeval.calculs.infrastructure.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.mte.numecoeval.calculs.domain.port.input.service.*;
import org.mte.numecoeval.calculs.domain.port.input.service.impl.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DomainConfiguration {

    @Bean
    public DureeDeVieEquipementPhysiqueService dureeDeVieEquipementPhysiqueService() {
        return new DureeDeVieEquipementPhysiqueServiceImpl();
    }

    @Bean
    public CalculImpactEquipementPhysiqueService calculImpactEquipementPhysiqueService(DureeDeVieEquipementPhysiqueService dureeDeVieEquipementPhysiqueService) {
        return new CalculImpactEquipementPhysiqueServiceImpl(
                dureeDeVieEquipementPhysiqueService);
    }

    @Bean
    CalculImpactEquipementVirtuelService calculImpactEquipementVirtuelService() {
        return new CalculImpactEquipementVirtuelServiceImpl();
    }

    @Bean
    CalculImpactApplicationService calculImpactApplicationService() {
        return new CalculImpactApplicationServiceImpl();
    }

    @Bean
    public CalculImpactOperationNonITService calculImpactOperationNonITService() {
        return new CalculImpactOperationNonITServiceImpl();
    }

    @Bean
    CalculImpactMessagerieService calculImpactMessagerieService(ObjectMapper objectMapper) {
        return new CalculImpactMessagerieServiceImpl();
    }

}
