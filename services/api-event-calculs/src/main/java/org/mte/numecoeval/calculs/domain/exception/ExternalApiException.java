package org.mte.numecoeval.calculs.domain.exception;

public class ExternalApiException extends RuntimeException {

    /**
     * Constructor with no arg
     */
    public ExternalApiException() {
        super();
    }

    /**
     * Constructor with cause
     */
    public ExternalApiException(Throwable cause) {
        super(cause);
    }
}
