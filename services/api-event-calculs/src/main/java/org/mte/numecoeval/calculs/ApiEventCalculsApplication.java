package org.mte.numecoeval.calculs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiEventCalculsApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiEventCalculsApplication.class, args);
    }

}
