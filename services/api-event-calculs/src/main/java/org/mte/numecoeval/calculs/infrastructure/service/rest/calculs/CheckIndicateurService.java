package org.mte.numecoeval.calculs.infrastructure.service.rest.calculs;

import org.springframework.stereotype.Service;

@Service
public class CheckIndicateurService {

    public boolean isOk(String statutIndicateur) {
        return "OK".equals(statutIndicateur);
    }
}
