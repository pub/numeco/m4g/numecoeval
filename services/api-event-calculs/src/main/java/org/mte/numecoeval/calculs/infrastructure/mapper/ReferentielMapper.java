package org.mte.numecoeval.calculs.infrastructure.mapper;

import org.mapstruct.Mapper;
import org.mte.numecoeval.calculs.domain.data.referentiel.*;
import org.mte.numecoeval.calculs.referentiels.generated.api.model.*;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ReferentielMapper {

    ReferentielEtapeACV toEtape(EtapeDTO etapeDTO);

    ReferentielCritere toCritere(CritereDTO critereDTO);

    ReferentielHypothese toHypothese(HypotheseDTO hypotheseDTO);

    List<ReferentielHypothese> toListHypothese(List<HypotheseDTO> hypotheseDTO);

    ReferentielFacteurCaracterisation toFacteurCaracterisation(FacteurCaracterisationDTO facteurCaracterisationDTO);

    List<ReferentielFacteurCaracterisation> toListFacteurCaracterisation(List<FacteurCaracterisationDTO> facteurCaracterisationDTO);

    ReferentielImpactMessagerie toImpactMessagerie(ImpactMessagerieDTO impactMessagerieDTO);

    List<ReferentielImpactMessagerie> toListImpactMessagerie(List<ImpactMessagerieDTO> impactMessagerieDTO);

    ReferentielCorrespondanceRefEquipement toCorrespondanceRefEquipement(CorrespondanceRefEquipementDTO dto);

    ReferentielTypeItem toTypeItem(TypeItemDTO dto);
}
