package org.mte.numecoeval.calculs.infrastructure.service.calcul;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mte.numecoeval.calculs.domain.data.entree.EquipementVirtuel;

import java.util.List;

@ExtendWith(MockitoExtension.class)
class CalculEquipementVirtuelServiceTest {

    @InjectMocks
    CalculEquipementVirtuelService calculEquipementVirtuelService;

    @Test
    void testCalculEquipementVirtuelService_getTotalVCPU_nominal() {
        List<EquipementVirtuel> equipementVirtuelList = List.of(
                EquipementVirtuel.builder().vCPU(2).build(),
                EquipementVirtuel.builder().vCPU(6).build()
        );

        Assertions.assertEquals(8, calculEquipementVirtuelService.getTotalVCPU(equipementVirtuelList));
    }

    @Test
    void testCalculEquipementVirtuelService_getTotalVCPU_null() {
        List<EquipementVirtuel> equipementVirtuelList = List.of(
                EquipementVirtuel.builder().vCPU(2).build(),
                EquipementVirtuel.builder().vCPU(0).build()
        );
        Assertions.assertNull(calculEquipementVirtuelService.getTotalVCPU(equipementVirtuelList));

        equipementVirtuelList = List.of(
                EquipementVirtuel.builder().vCPU(2).build(),
                EquipementVirtuel.builder().build()
        );
        Assertions.assertNull(calculEquipementVirtuelService.getTotalVCPU(equipementVirtuelList));
    }


    @Test
    void testCalculEquipementVirtuelService_getTotalStockage_nominal() {
        List<EquipementVirtuel> equipementVirtuelList = List.of(
                EquipementVirtuel.builder().capaciteStockage(2.1).build(),
                EquipementVirtuel.builder().capaciteStockage(6.2).build()
        );

        Assertions.assertEquals(8.3, calculEquipementVirtuelService.getTotalStockage(equipementVirtuelList));
    }

    @Test
    void testCalculEquipementVirtuelService_getTotalStockage_null() {
        List<EquipementVirtuel> equipementVirtuelList = List.of(
                EquipementVirtuel.builder().capaciteStockage(2.1).build(),
                EquipementVirtuel.builder().capaciteStockage(0.0).build()
        );
        Assertions.assertNull(calculEquipementVirtuelService.getTotalVCPU(equipementVirtuelList));

        equipementVirtuelList = List.of(
                EquipementVirtuel.builder().capaciteStockage(2.0).build(),
                EquipementVirtuel.builder().build()
        );
        Assertions.assertNull(calculEquipementVirtuelService.getTotalVCPU(equipementVirtuelList));
    }
}
