package org.mte.numecoeval.calculs.infrastructure.service.rest.calculs;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class CheckIndicateurServiceTest {

    @InjectMocks
    CheckIndicateurService checkIndicateurService;

    @Test
    void testCheckIndicateurService_isOk() {
        Assertions.assertTrue(checkIndicateurService.isOk("OK"));
        Assertions.assertFalse(checkIndicateurService.isOk("ERREUR"));
    }

}
