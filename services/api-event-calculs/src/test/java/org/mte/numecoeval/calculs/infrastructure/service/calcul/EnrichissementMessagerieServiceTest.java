package org.mte.numecoeval.calculs.infrastructure.service.calcul;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mte.numecoeval.calculs.infrastructure.client.ReferentielClient;
import org.mte.numecoeval.calculs.infrastructure.service.enrichissement.EnrichissementMessagerieService;
import org.mte.numecoeval.calculs.referentiels.generated.api.model.CritereDTO;
import org.mte.numecoeval.calculs.referentiels.generated.api.model.ImpactMessagerieDTO;
import org.mte.numecoeval.topic.data.MessagerieDTO;

import java.util.List;

@ExtendWith(MockitoExtension.class)
class EnrichissementMessagerieServiceTest {

    @InjectMocks
    EnrichissementMessagerieService enrichissementMessagerieService;

    @Mock
    ReferentielClient referentielClient;

    @Test
    void testServiceEnrichissementMessagerie_null() {
        Assertions.assertNull(enrichissementMessagerieService.serviceEnrichissementMessagerie(null));
    }

    @Test
    void testServiceEnrichissementMessagerie() {

        /* INPUT DATA */
        var messagerieDTO = new MessagerieDTO();
        messagerieDTO.setId(1L);

        var critereDTO = new CritereDTO();
        critereDTO.setNomCritere("critere");

        Mockito.when(referentielClient.getCriteres()).thenReturn(List.of(critereDTO));

        var impactMessagerieDTO = new ImpactMessagerieDTO();
        impactMessagerieDTO.setCritere("critere");
        impactMessagerieDTO.setConstanteCoefficientDirecteur(1.1);

        Mockito.when(referentielClient.getMessagerie("critere")).thenReturn(impactMessagerieDTO);

        /* EXECUTE */
        var actual = enrichissementMessagerieService.serviceEnrichissementMessagerie(messagerieDTO);
        Assertions.assertEquals(1, actual.getCriteres().size());
        Assertions.assertEquals(1, actual.getImpactsMessagerie().size());
        Assertions.assertEquals(1L, actual.getMessagerie().getId());

        Assertions.assertEquals(1.1, actual.getImpactsMessagerie().get(0).getConstanteCoefficientDirecteur());
        Assertions.assertEquals("critere", actual.getImpactsMessagerie().get(0).getCritere());
    }

}
